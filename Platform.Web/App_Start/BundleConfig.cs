﻿using System.Web;
using System.Web.Optimization;

namespace Platform.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Index/css").Include(

            "~/Assets/apphome/css/bootstrap.min.css",
            "~/Assets/apphome/css/style.css",
             "~/Assets/apphome/css/responsive.css",
             "~/Assets/apphome/lib/toastr/toastr.min.css",
             "~/Assets/apphome/css/kendo.common.min.css",
            "~/Assets/apphome/css/kendo.default.min.css",
            "~/Assets/apphome/js/angularjs-datetime-picker-master/angularjs-datetime-picker-master/angularjs-datetime-picker.css",
            "~/Assets/apphome/css/video-js.css",
           "~/Assets/apphome/css/select2.min.css",
           "~/Assets/apphome/css/swiper-bundle.min.css",
            "~/Assets/apphome/css/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css",
            "~/Assets/apphome/css/geosearch.css",
           
            "~/Assets/apphome/css/bootstrap-select.css",
            "~/Assets/apphome/css/bootsnav.css",
            "~/Assets/apphome/css/font1.css",
            "~/Assets/apphome/css/font2.css",
            "~/Assets/apphome/css/jquery.fancybox1.min.css",
            "~/Assets/apphome/css/sweetalert2.min.css",
             "~/Assets/apphome/css/video-js.css"));

            bundles.Add(new StyleBundle("~/style/css").Include(
           "~/Assets/apphome/css/style.css"));


            bundles.Add(new ScriptBundle("~/Index/js").Include(
             "~/Assets/apphome/js/jquery-3.5.1.min.js" ,
             "~/Assets/apphome/js/jquery-ui.js" ,
             "~/Assets/apphome/jslocal/slick/slick.js" ,
             "~/Assets/apphome/js/select2.min.js" ,
             "~/Assets/apphome/jslocal/angular-1.8.0/angular.js" ,
             "~/Assets/apphome/js/angular-loading-bar-master/src/loading-bar.js",

             "~/Assets/apphome/jslocal/angular-1.8.0/angular-animate.js" ,
             "~/Assets/apphome/js/angular-filter.js" ,
             "~/Assets/apphome/js/kendo.all.min.js" ,
             "~/Assets/apphome/js/popper.min.js" ,
             "~/Assets/apphome/js/bootstrap.min.js" ,
             "~/Assets/apphome/js/bootstrap-select.js" ,
             "~/Assets/apphome/js/bootsnav.js" ,
             "~/Assets/apphome/js/custom.js" ,
             "~/Assets/apphome/css/OwlCarousel2-2.3.4/dist/owl.carousel.min.js" ,
             "~/Assets/apphome/js/angular-recaptcha-master/angular-recaptcha-master/release/angular-recaptcha.js" ,
             "~/Assets/apphome/js/geosearch.umd.js" ,
             "~/Assets/apphome/jslocal/angular-ui-router.js" ,
             "~/Assets/apphome/lib/bootbox/bootbox.js" ,
             "~/Assets/apphome/lib/ngBootbox/ngBootbox.js" ,
             "~/Assets/apphome/lib/toastr/toastr.js" ,
             "~/apphome/shared/modules/platformTH.common.js" ,
             "~/apphome/shared/services/apiService.js" ,
             "~/apphome/shared/services/authData.js" ,
             "~/apphome/shared/services/authenticationService.js" ,
             "~/apphome/shared/services/loginService.js" ,
             "~/apphome/shared/app.js" ,
             "~/Assets/apphome/lib/angular-local-storage/dist/angular-local-storage.js" ,
             "~/Assets/apphome/lib/angular-cookies/angular-cookies.min.js" ,
             "~/Assets/apphome/lib/angular-ui-router/release/stateEvents.js" ,
             "~/apphome/shared/services/notificationService.js" ,
             "~/apphome/shared/directives/pagerDirective.js" ,
             "~/apphome/conponents/trangchu/Cart.js",
             "~/apphome/shared/filters/totalFilter.js" ,
             "~/Assets/apphome/js/angularjs-datetime-picker-master/angularjs-datetime-picker-master/angularjs-datetime-picker.js" ,
             "~/Assets/apphome/js/angular-locale_vi-vn.js" ,
             "~/apphome/conponents/trangchu/rootController.js" ,
             "~/apphome/conponents/trangchu/topSearchController.js" ,
             "~/apphome/conponents/trangchu/trangChu.module.js" ,
             "~/apphome/conponents/trangchu/trangChuController.js" ,
             "~/apphome/conponents/dathang/datHang.module.js" ,
             "~/apphome/conponents/dathang/datHangController.js" ,
             "~/apphome/conponents/menu/meNu.module.js" ,
             "~/apphome/conponents/menu/meNuController.js" ,
             "~/apphome/conponents/gioithieu/gioiThieu.module.js" ,
             "~/apphome/conponents/gioithieu/gioiThieuController.js" ,
             "~/apphome/conponents/vitri/viTri.module.js" ,
             "~/apphome/conponents/vitri/viTriController.js" ,
             "~/apphome/conponents/chitiethanghoa/chiTietHangHoa.module.js" ,
             "~/apphome/conponents/chitiethanghoa/chiTietHangHoaController.js" ,
             "~/apphome/conponents/chinhanh/chiNhanh.module.js" ,
             "~/apphome/conponents/chinhanh/chiNhanhController.js" ,
             "~/apphome/conponents/xemgiohang/xemGioHang.module.js" ,
             "~/apphome/conponents/xemgiohang/xemGioHangController.js" ,
             "~/apphome/conponents/userprofile/UserProfile.module.js" ,
             "~/apphome/conponents/userprofile/UserProfileController.js" ,
             "~/apphome/conponents/tuyendung/tuyenDungModule.js" ,
             "~/apphome/conponents/tuyendung/tuyenDungController.js" ,
             "~/apphome/conponents/hinhanh/hinhAnhModule.js" ,
             "~/apphome/conponents/hinhanh/hinhAnhController.js" ,
             "~/apphome/conponents/khuyenmai/khuyenMaiModule.js" ,
             "~/apphome/conponents/khuyenmai/khuyenMaiController.js" ,
             "~/apphome/conponents/video/VideoModule.js" ,
             "~/apphome/conponents/video/viDeoController.js" ,
             "~/apphome/conponents/huongdanthanhtoan/huongDanThanhToanModule.js" ,
             "~/apphome/conponents/huongdanthanhtoan/huongDanThanhToanController.js" ,
             "~/apphome/conponents/gioithieuchung/gioiThieuChungModule.js" ,
             "~/apphome/conponents/gioithieuchung/gioiThieuChungController.js" ,
             "~/apphome/conponents/thongtincuahang/thongTinCuaHangModule.js" ,
             "~/apphome/conponents/thongtincuahang/thongTinCuaHangController.js" ,
             "~/apphome/conponents/huongdanquetqr/huongDanQuetMaModule.js" ,
             "~/apphome/conponents/huongdanquetqr/huongDanQuetMaController.js" ,
             "~/apphome/shared/services/commonService.js" ,
             "~/Assets/apphome/js/moment.js" ,
             "~/Assets/apphome/js/swiper-bundle.min.js" ,
             "~/Assets/apphome/js/sweetalert2.min.js"
            
           ));






















           
        }
    }
}
