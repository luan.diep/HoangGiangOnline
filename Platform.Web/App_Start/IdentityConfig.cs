﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.OAuth;
using Platform.Data;
using Platform.Model;
using Platform.Model.Models;
using System;
using System.Security.Claims;
using System.Threading.Tasks;


namespace Platform.Web.App_Start
{
    public class ApplicationUserStore : UserStore<ApplicationUser_KH>
    {
        public ApplicationUserStore(QLTHDbContext context)
            : base(context)
        {
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    //public class ApplicationUserManager : UserManager<ApplicationUser>
    //{
    //    public ApplicationUserManager(IUserStore<ApplicationUser> store)
    //        : base(store)
    //    {
    //    }

    //    public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
    //    {
    //        var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<QLTHDbContext>()));
    //        // Configure validation logic for usernames
    //        manager.UserValidator = new UserValidator<ApplicationUser>(manager)
    //        {
    //            AllowOnlyAlphanumericUserNames = false,
    //            RequireUniqueEmail = true
    //        };

    //        // Configure validation logic for passwords
    //        manager.PasswordValidator = new PasswordValidator
    //        {
    //            RequiredLength = 6,
    //            RequireNonLetterOrDigit = true,
    //            RequireDigit = true,
    //            RequireLowercase = true,
    //            RequireUppercase = true,
    //        };

    //        // Configure user lockout defaults
    //        //manager.UserLockoutEnabledByDefault = true;
    //        //manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);

    //        //manager.MaxFailedAccessAttemptsBeforeLockout = 5;
    //        //manager.UserLockoutEnabledByDefault = false;

    //       // var dataProtectionProvider = options.DataProtectionProvider;
    //        //if (dataProtectionProvider != null)
    //        //{
    //        //    manager.UserTokenProvider =
    //        //        new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));

    //        //}
    //        //return manager;

    //        var dataProtectionProvider = Startup.DataProtectionProvider;
    //        if (dataProtectionProvider != null)
    //        {

    //            IDataProtector dataProtector = dataProtectionProvider.Create("ASP.NET Identity");

    //            UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser, string>(dataProtector)
    //            {
    //                TokenLifespan = TimeSpan.FromHours(24),
    //            };
    //        }
    //    }
    //}

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationUserManager : UserManager<ApplicationUser_KH>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser_KH> store)
            : base(store)
        {
            // Configure validation logic for usernames
            UserValidator = new UserValidator<ApplicationUser_KH>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            UserLockoutEnabledByDefault = false;
            //DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(30);
            //MaxFailedAccessAttemptsBeforeLockout = 1;

            //RegisterTwoFactorProvider("Mã bảo mật qua Email", new EmailTokenProvider<ApplicationUser>
            //{
            //    Subject = "Mã bảo mật",
            //    BodyFormat = "Mã bảo mật của bạn là {0}"
            //});

            var dataProtectionProvider = Startup.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                IDataProtector dataProtector = dataProtectionProvider.Create("ASP.NET Identity");

                UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser_KH, string>(dataProtector)
                {
                    TokenLifespan = TimeSpan.FromHours(24),
                };
            }
        }
    }
    public class ApplicationSignInManager : SignInManager<ApplicationUser_KH, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser_KH user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }

        
    }
}