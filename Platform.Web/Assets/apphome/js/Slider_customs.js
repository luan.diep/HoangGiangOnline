﻿

    (function (app) {
        app.factory('Slider', Slider);

        Slider.$inject = [];

        function Slider() {

            $(document).ready(function () {
                $(".hg-slider-next").on("click", function () {
                    var count = $(".hg-slider-wapper").find(".hg-slider-item").length - 1;
                    var index = $(".hg-slider-wapper").find(".active").index() + 1
                    var indexwidth = ($(".hg-slider-wapper .hg-slider-item").eq(index).width() * (index))/* + (20 * index)*/

                    if ((count - 1) == ($(".hg-slider-wapper").find(".active").index() + 1)) {
                        $(".hg-slider-wapper .hg-slider-item").css({ "transform": "translate3d(-" + 0 + "px, 0px, 0px)", 'transition': '0s' })
                        $(".hg-slider-wapper").find(".active").removeClass("active")
                        $(".hg-slider-wapper .hg-slider-item").eq(0).addClass("active")
                    } else {
                        $(".hg-slider-wapper").find(".active").removeClass("active")
                        $(".hg-slider-wapper .hg-slider-item").eq(index).addClass("active")
                        $(".hg-slider-wapper .hg-slider-item").css({ "transform": "translate3d(-" + indexwidth + "px, 0px, 0px)", 'transition': '0.5s' })



                    }

                })



                $(".hg-slider-pre").on('click', function () {

                    var count = $(".hg-slider-wapper").find(".hg-slider-item").length - 1;
                    var index = $(".hg-slider-wapper").find(".active").index() - 1
                    if (index == -1) {
                        index = count - 2
                    }
                    var indexwidth = ($(".hg-slider-wapper .hg-slider-item").eq(index).width() * (index)) /*+ (20 * index)*/

                    if (($(".hg-slider-wapper").find(".active").index()) == 0) {
                        $(".hg-slider-wapper .hg-slider-item").css({ "transform": "translate3d(-" + indexwidth + "px, 0px, 0px)", 'transition': '0s' })
                        $(".hg-slider-wapper").find(".active").removeClass("active")
                        $(".hg-slider-wapper .hg-slider-item").eq(index).addClass("active")
                    } else {
                        $(".hg-slider-wapper").find(".active").removeClass("active")
                        $(".hg-slider-wapper .hg-slider-item").eq(index).addClass("active")
                        $(".hg-slider-wapper .hg-slider-item").css({ "transform": "translate3d(-" + indexwidth + "px, 0px, 0px)", 'transition': '0.5s' })



                    }




                })


                
            })

            function autoplay() {
                var count = $(".hg-slider-wapper").find(".hg-slider-item").length - 1;
                var index = $(".hg-slider-wapper").find(".active").index() + 1
                if ($(document).width() <= 480) {
                    var indexwidth = ($(".hg-slider-wapper .hg-slider-item").eq(index).width() * (index)) /*+ (10 * index)*/
                } else {
                    var indexwidth = ($(".hg-slider-wapper .hg-slider-item").eq(index).width() * (index)) /*+ (20 * index)*/
                }


                if ((count - 1) == ($(".hg-slider-wapper").find(".active").index() + 1)) {
                    $(".hg-slider-wapper .hg-slider-item").css({ "transform": "translate3d(-" + 0 + "px, 0px, 0px)", 'transition': '0s' })
                    $(".hg-slider-wapper").find(".active").removeClass("active")
                    $(".hg-slider-wapper .hg-slider-item").eq(0).addClass("active")
                } else {
                    $(".hg-slider-wapper").find(".active").removeClass("active")
                    $(".hg-slider-wapper .hg-slider-item").eq(index).addClass("active")
                    $(".hg-slider-wapper .hg-slider-item").css({ "transform": "translate3d(-" + indexwidth + "px, 0px, 0px)", 'transition': '0.5s' })
                }
            }

            function autorun(i) { 
                if (i.auto == 'true') {
                    setInterval(autoplay, i.time);
                } else {
                    clearInterval(autoplay);

                }
               


            }

            
            return {
                Slider: Slider,
                autorun: autorun,
            }
               
             
        }

    })(angular.module('apphome.common'));