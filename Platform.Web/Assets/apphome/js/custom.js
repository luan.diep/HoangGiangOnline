(function($) {
    "use strict";
	
	/* ..............................................
	   Loader 
	   ................................................. */
	$(window).on('load', function() {
		$('.preloader').fadeOut();
		$('#preloader').delay(550).fadeOut('slow');
		$('body').delay(450).css({
			'overflow': 'visible'
		});
	});

	/* ..............................................
	   Fixed Menu
	   ................................................. */

	//$(window).on('scroll', function() {
	//	if ($(window).scrollTop() > 100) {
	//		$('.main-header').addClass('fixed-menu', { duration: 500 });
	//	} else {
	//		$('.main-header').removeClass('fixed-menu', { duration: 500 });
	//	}
	//});

	

	
	/* ..............................................
	   Map Full
	   ................................................. */

	$(document).ready(function() {
		$(window).on('scroll', function() {
			if ($(this).scrollTop() > 100) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});
		$('#back-to-top').click(function() {
			$("html, body").animate({
				scrollTop: 0
			}, 600);
			return false;
		});
	});

	/* ..............................................
	   Special Menu
	   ................................................. */

	
	/* ..............................................
	   BaguetteBox
	   ................................................. */

	
	

	/* ..............................................
	   Scroll
	   ................................................. */

	$(document).ready(function() {
		$(window).on('scroll', function() {
			if ($(this).scrollTop() > 100) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});
		$('#back-to-top').click(function() {
			$("html, body").animate({
				scrollTop: 0
			}, 600);
			return false;
		});
	});


	$(window).resize(function () {
		console.log('resize called');
		var width = $(window).width();
		if (width <= 1199) {
			$('#container').removeClass('container');
			$('#container').addClass('container-fluid');
			
		}
		else {
			$('.container-fluid').removeClass('container-fluid');
			$('#container').addClass('container');
		}
	})
		.resize();//trigger the resize event on page load.



	


}(jQuery));