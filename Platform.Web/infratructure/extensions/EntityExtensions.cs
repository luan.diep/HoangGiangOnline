﻿using Platform.Model;
using Platform.Model.Models;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.infratructure.extensions
{
    public static class EntityExtensions
    {
       

       

        public static void UpdateApplicationGroup(this ApplicationGroup_KH appGroup, ApplicationGroupViewModel appGroupViewModel)
        {
            appGroup.ID = appGroupViewModel.ID;
            appGroup.Name = appGroupViewModel.Name;
        }

        public static void UpdateApplicationRole(this ApplicationRole_KH appRole, ApplicationRoleViewModel appRoleViewModel, string action = "add")
        {
            if (action == "update")
                appRole.Id = appRoleViewModel.Id;
            else
                appRole.Id = Guid.NewGuid().ToString();
            appRole.Name = appRoleViewModel.Name;
            appRole.Description = appRoleViewModel.Description;
        }
        public static void UpdateUser(this ApplicationUser_KH appUser, ApplicationUserViewModel appUserViewModel, string action = "add")
        {

            appUser.Id = appUserViewModel.Id;
            appUser.FullName = appUserViewModel.FullName;
            appUser.BirthDay = appUserViewModel.BirthDay;
            appUser.Email = appUserViewModel.Email;
            appUser.UserName = appUserViewModel.UserName;
            appUser.Address = appUserViewModel.Address;
            appUser.PhoneNumber = appUserViewModel.PhoneNumber;
        }
        public static void UpdateTuyenDung(this TuyenDung tuyenDung, TuyenDungViewModel tuyenDungViewModel)
        {

            tuyenDung.MaTuyenDung = tuyenDungViewModel.MaTuyenDung;
            tuyenDung.MaCoSoTuyenDung = tuyenDungViewModel.MaCoSoTuyenDung;
            tuyenDung.DiaChi = tuyenDungViewModel.DiaChi;
            tuyenDung.SoDienThoai = tuyenDungViewModel.SoDienThoai;
            tuyenDung.Email = tuyenDungViewModel.Email;
            tuyenDung.TieuDe = tuyenDungViewModel.TieuDe;
            tuyenDung.NoiDung = tuyenDungViewModel.NoiDung;
            tuyenDung.NgayTao = tuyenDungViewModel.NgayTao;
            tuyenDung.MaNhomTuyenDung = tuyenDungViewModel.MaNhomTuyenDung;
        }

        public static void UpdateNhomTuyenDung(this NhomTuyenDung nhomTuyenDung, NhomTuyenDungViewModel nhomTuyenDungViewModel)
        {

            nhomTuyenDung.MaNhomTuyenDung = nhomTuyenDungViewModel.MaNhomTuyenDung;
            nhomTuyenDung.TenNhomTuyenDung = nhomTuyenDungViewModel.TenNhomTuyenDung;

        }
        public static void UpdateHinhAnh(this HinhAnhs hinhAnhs, HinhAnhViewModel hinhAnhViewModel)
        {

            hinhAnhs.MaHinhAnh = hinhAnhViewModel.MaHinhAnh;
            hinhAnhs.MaCoSo = hinhAnhViewModel.MaCoSo;
            hinhAnhs.HinhAnh = hinhAnhViewModel.HinhAnh;
            hinhAnhs.NhieuHinhAnh = hinhAnhViewModel.NhieuHinhAnh;
            hinhAnhs.NgayTao = hinhAnhViewModel.NgayTao;

            
        }
          public static void UpdateVideo(this Video video, VideoViewModel videoViewModel)
        {

            video.MaViDeo = videoViewModel.MaViDeo;
            video.DuongDan = videoViewModel.DuongDan;
            video.GhiChu = videoViewModel.GhiChu;
            video.TieuDe = videoViewModel.TieuDe;
            video.NoiDung = videoViewModel.NoiDung;
            video.NgayTao = videoViewModel.NgayTao;
          

            
        }







        
     

     
      
       

      
       
      
       
        public static void UpdateCoSo(this CoSo coSo, CoSoViewModel coSoVM)
        {

            coSo.MaCoSo = coSoVM.MaCoSo;
            coSo.DiaChi = coSoVM.DiaChi;
            coSo.TenCoSo = coSoVM.TenCoSo;
            coSo.CapToChuc = coSoVM.CapToChuc;
            coSo.KinhTuyen = coSoVM.KinhTuyen;
            coSo.ViTuyen = coSoVM.ViTuyen;
            coSo.GhiChu = coSoVM.GhiChu;
            coSo.HinhAnh = coSoVM.HinhAnh;



        }
        
      
        public static void UpdateChucVu(this ChucVu chucVu, ChucVuViewModel chucVuVm)
        {

            chucVu.MaChucVu = chucVuVm.MaChucVu;
            chucVu.TenChucVu = chucVuVm.TenChucVu;
          

        }
        public static void UpdateKhachHang(this KhachHang khachHang, KhachHangViewModel khachHangVm)
        {
            khachHang.MaKhachHang = khachHangVm.MaKhachHang;
            khachHang.NhomKH_NCC = khachHangVm.NhomKH_NCC;
            khachHang.PhanLoai = khachHangVm.PhanLoai;
            khachHang.TenKhachHang = khachHangVm.TenKhachHang;
            khachHang.ChiNhanh = khachHangVm.ChiNhanh;
            khachHang.DiaChi = khachHangVm.DiaChi;
            khachHang.SoDienThoai = khachHangVm.SoDienThoai;
            khachHang.Fax = khachHangVm.Fax;
            khachHang.Email = khachHangVm.Email;
            khachHang.Website = khachHangVm.Website;
            khachHang.MaSoThue = khachHangVm.MaSoThue;
            khachHang.SoTaiKhoanNganHang = khachHangVm.SoTaiKhoanNganHang;
            khachHang.TenNganHang = khachHangVm.TenNganHang;
            khachHang.NguoiLienHe = khachHangVm.NguoiLienHe;
            khachHang.DaXoa = khachHangVm.DaXoa;
            khachHang.NguoiXoa = khachHangVm.NguoiXoa;
            khachHang.NgayXoa = khachHangVm.NgayXoa;
            khachHang.XungHo = khachHangVm.XungHo;
            khachHang.NVBanHang = khachHangVm.NVBanHang;
            khachHang.DienGiai = khachHangVm.DienGiai;
            khachHang.SoNgayDuocNo = khachHangVm.SoNgayDuocNo;
            khachHang.SoNoToiDa = khachHangVm.SoNoToiDa;
            khachHang.QuocGia = khachHangVm.QuocGia;
            khachHang.MaQuan_Huyen = khachHangVm.MaQuan_Huyen;
            khachHang.Tinh_TP = khachHangVm.Tinh_TP;
            khachHang.Xa_Phuong = khachHangVm.Xa_Phuong;
            khachHang.DienThoaiCoDinh_LH = khachHangVm.DienThoaiCoDinh_LH;
            khachHang.HoVaTen_LH = khachHangVm.HoVaTen_LH;
            khachHang.Email_LH = khachHangVm.Email_LH;
            khachHang.ChucDanh_LH = khachHangVm.ChucDanh_LH;
            khachHang.DiaChi_LH = khachHangVm.DiaChi_LH;
            khachHang.DTdidong_LH = khachHangVm.DTdidong_LH;
            khachHang.DTKhac_LH = khachHangVm.DTKhac_LH;
            khachHang.TenNguoiNhan = khachHangVm.TenNguoiNhan;
            khachHang.DienThoaiNguoiNhan = khachHangVm.DienThoaiNguoiNhan;
            khachHang.DiaChiNguoiNhan = khachHangVm.DiaChiNguoiNhan;
            khachHang.EmailNguoiNhan = khachHangVm.EmailNguoiNhan;
            khachHang.DaiDienTheoPL_LH = khachHangVm.DaiDienTheoPL_LH;
            khachHang.DiaDiemGiaoHang = khachHangVm.DiaDiemGiaoHang;
            khachHang.NoiCap = khachHangVm.NoiCap;
            khachHang.CMND = khachHangVm.CMND;
            khachHang.DieuKhoanTT = khachHangVm.DieuKhoanTT;
            khachHang.NgayCap = khachHangVm.NgayCap;
            khachHang.NgaySinh = khachHangVm.NgaySinh;
            khachHang.DangHoatDong = khachHangVm.DangHoatDong;
            khachHang.NgayTao = khachHangVm.NgayTao;
            khachHang.SoLuongGiaoDich = khachHangVm.SoLuongGiaoDich;
            khachHang.TongGiaoDich = khachHangVm.TongGiaoDich;
            khachHang.GiaTriGiaoDichLonNhat = khachHangVm.GiaTriGiaoDichLonNhat;
        }
         public static void UpdateKhachHang_KhongTaiKhoan(this KhachHang_KhongTaiKhoan khachHang, KhachHang_KhongTaiKhoanViewModel khachHangVm)
        {
            khachHang.MaKhachHang = khachHangVm.MaKhachHang;
            khachHang.NhomKH_NCC = khachHangVm.NhomKH_NCC;
            khachHang.PhanLoai = khachHangVm.PhanLoai;
            khachHang.TenKhachHang = khachHangVm.TenKhachHang;
            khachHang.ChiNhanh = khachHangVm.ChiNhanh;
            khachHang.DiaChi = khachHangVm.DiaChi;
            khachHang.SoDienThoai = khachHangVm.SoDienThoai;
            khachHang.Fax = khachHangVm.Fax;
            khachHang.Email = khachHangVm.Email;
            khachHang.Website = khachHangVm.Website;
            khachHang.MaSoThue = khachHangVm.MaSoThue;
            khachHang.SoTaiKhoanNganHang = khachHangVm.SoTaiKhoanNganHang;
            khachHang.TenNganHang = khachHangVm.TenNganHang;
            khachHang.NguoiLienHe = khachHangVm.NguoiLienHe;
            khachHang.DaXoa = khachHangVm.DaXoa;
            khachHang.NguoiXoa = khachHangVm.NguoiXoa;
            khachHang.NgayXoa = khachHangVm.NgayXoa;
            khachHang.XungHo = khachHangVm.XungHo;
            khachHang.NVBanHang = khachHangVm.NVBanHang;
            khachHang.DienGiai = khachHangVm.DienGiai;
            khachHang.SoNgayDuocNo = khachHangVm.SoNgayDuocNo;
            khachHang.SoNoToiDa = khachHangVm.SoNoToiDa;
            khachHang.QuocGia = khachHangVm.QuocGia;
            khachHang.MaQuan_Huyen = khachHangVm.MaQuan_Huyen;
            khachHang.Tinh_TP = khachHangVm.Tinh_TP;
            khachHang.Xa_Phuong = khachHangVm.Xa_Phuong;
            khachHang.DienThoaiCoDinh_LH = khachHangVm.DienThoaiCoDinh_LH;
            khachHang.HoVaTen_LH = khachHangVm.HoVaTen_LH;
            khachHang.Email_LH = khachHangVm.Email_LH;
            khachHang.ChucDanh_LH = khachHangVm.ChucDanh_LH;
            khachHang.DiaChi_LH = khachHangVm.DiaChi_LH;
            khachHang.DTdidong_LH = khachHangVm.DTdidong_LH;
            khachHang.DTKhac_LH = khachHangVm.DTKhac_LH;
            khachHang.TenNguoiNhan = khachHangVm.TenNguoiNhan;
            khachHang.DienThoaiNguoiNhan = khachHangVm.DienThoaiNguoiNhan;
            khachHang.DiaChiNguoiNhan = khachHangVm.DiaChiNguoiNhan;
            khachHang.EmailNguoiNhan = khachHangVm.EmailNguoiNhan;
            khachHang.DaiDienTheoPL_LH = khachHangVm.DaiDienTheoPL_LH;
            khachHang.DiaDiemGiaoHang = khachHangVm.DiaDiemGiaoHang;
            khachHang.NoiCap = khachHangVm.NoiCap;
            khachHang.CMND = khachHangVm.CMND;
            khachHang.DieuKhoanTT = khachHangVm.DieuKhoanTT;
            khachHang.NgayCap = khachHangVm.NgayCap;
            khachHang.NgaySinh = khachHangVm.NgaySinh;
            khachHang.DangHoatDong = khachHangVm.DangHoatDong;
            khachHang.NgayTao = khachHangVm.NgayTao;
            khachHang.SoLuongGiaoDich = khachHangVm.SoLuongGiaoDich;
            khachHang.TongGiaoDich = khachHangVm.TongGiaoDich;
            khachHang.GiaTriGiaoDichLonNhat = khachHangVm.GiaTriGiaoDichLonNhat;
        }

      
       

        public static void UpdateHangHhoa(this HangHoa hangHoa, HangHoaViewModel hangHoaVm)
        {
            hangHoa.MaHang = hangHoaVm.MaHang;
            hangHoa.TenHang = hangHoaVm.TenHang;
            hangHoa.MaNhomHH = hangHoaVm.MaNhomHH;
            hangHoa.MaDonViTinh = hangHoaVm.MaDonViTinh;
            hangHoa.MaTinhChat = hangHoaVm.MaTinhChat;
            hangHoa.DonViTinh = hangHoaVm.DonViTinh;
            hangHoa.BaoHanh = hangHoaVm.BaoHanh;
            hangHoa.NguonGoc = hangHoaVm.NguonGoc;
            hangHoa.MoTa = hangHoaVm.MoTa;
            hangHoa.GiaNhap = hangHoaVm.GiaNhap;
            hangHoa.GiaBan = hangHoaVm.GiaBan;
            hangHoa.GiaKhuyenMai = hangHoaVm.GiaKhuyenMai;
            hangHoa.KhuyenMai2 = hangHoaVm.KhuyenMai2;
            hangHoa.GiaNhapCuoi = hangHoaVm.GiaNhapCuoi;
            hangHoa.VAT = hangHoaVm.VAT;
            hangHoa.ChietKhau = hangHoaVm.ChietKhau;
            hangHoa.HanSuDung = hangHoaVm.HanSuDung;
            hangHoa.ThanhPham = hangHoaVm.ThanhPham;
            hangHoa.SoLo = hangHoaVm.SoLo;
            hangHoa.HinhAnh = hangHoaVm.HinhAnh;
            hangHoa.NguoiSua = hangHoaVm.NguoiSua;
            hangHoa.NgaySua = hangHoaVm.NgaySua;
            hangHoa.NgayNhap = hangHoaVm.NgayNhap;
            hangHoa.NguoiNhap = hangHoaVm.NguoiNhap;
            hangHoa.MaVach = hangHoaVm.MaVach;
            hangHoa.MaNhaCungCap = hangHoaVm.MaNhaCungCap;
            hangHoa.MaThuongHieu = hangHoaVm.MaThuongHieu;
            hangHoa.DinhMucTonItNhat = hangHoaVm.DinhMucTonItNhat;
            hangHoa.DinhMucTonNhieuNhat = hangHoaVm.DinhMucTonNhieuNhat;
            hangHoa.NgungKinhDoanh = hangHoaVm.NgungKinhDoanh;
            hangHoa.DaXoa = hangHoaVm.DaXoa;
            hangHoa.GhiChu = hangHoaVm.GhiChu;
            hangHoa.TrongLuong = hangHoaVm.TrongLuong;
            hangHoa.ViTri = hangHoaVm.ViTri;
            hangHoa.NhieuHinhAnh = hangHoaVm.NhieuHinhAnh;
            hangHoa.TonKho = hangHoaVm.TonKho;
            hangHoa.KhachHangDat = hangHoaVm.KhachHangDat;
            hangHoa.DuKienHetHang = hangHoaVm.DuKienHetHang;
        
    }
      
       
       
       
      
        public static void UpdateChiTietDonMuaHang(this ChiTietDonMuaHang donMuaHang, ChiTietDonMuaHangViewModel donMuaHangVm)
        {
            donMuaHang.MaDonMuaHang = donMuaHangVm.MaDonMuaHang;
            donMuaHang.MaCTDMH = donMuaHangVm.MaCTDMH;
            donMuaHang.MaHang = donMuaHangVm.MaHang;
            donMuaHang.SoLuong = donMuaHangVm.SoLuong;
            donMuaHang.SoLuongNhan = donMuaHangVm.SoLuongNhan;
            donMuaHang.DonGia = donMuaHangVm.DonGia;
            donMuaHang.ThanhTien = donMuaHangVm.ThanhTien;
            donMuaHang.ThueGTGT = donMuaHangVm.ThueGTGT;
            donMuaHang.TienThueGTGT = donMuaHangVm.TienThueGTGT;
            donMuaHang.LenhSanXuat = donMuaHangVm.LenhSanXuat;
            donMuaHang.ThanhPham = donMuaHangVm.ThanhPham;
             
    }
      
      
        public static void UpdateChiTietChungTuMuaHang(this ChiTietChungTuMuaHang chungTuMuaHang, ChiTietChungTuMuaHangViewModel chungTuMuaHangVm)
        {
            chungTuMuaHang.MaChiTietChungTuMuaHang = chungTuMuaHangVm.MaChiTietChungTuMuaHang;
            chungTuMuaHang.MaHang = chungTuMuaHangVm.MaHang;
            chungTuMuaHang.Kho = chungTuMuaHangVm.Kho;
            chungTuMuaHang.TKKho = chungTuMuaHangVm.TKKho;
            chungTuMuaHang.TKCongNo = chungTuMuaHangVm.TKCongNo;
            chungTuMuaHang.SoLuong = chungTuMuaHangVm.SoLuong;
            chungTuMuaHang.ThanhTien = chungTuMuaHangVm.ThanhTien;
            chungTuMuaHang.TienChietKhau = chungTuMuaHangVm.TienChietKhau;
            chungTuMuaHang.ChiPhiMuaHang = chungTuMuaHangVm.ChiPhiMuaHang;
            chungTuMuaHang.GiaTriNhapKho = chungTuMuaHangVm.GiaTriNhapKho;
            chungTuMuaHang.SoLo = chungTuMuaHangVm.SoLo;
            chungTuMuaHang.DoiTuongDHCP = chungTuMuaHangVm.DoiTuongDHCP;
            chungTuMuaHang.CongTrinh = chungTuMuaHangVm.CongTrinh;
            chungTuMuaHang.MaHopDongMua = chungTuMuaHangVm.MaHopDongMua;
            chungTuMuaHang.MaDonMuaHang = chungTuMuaHangVm.MaDonMuaHang;
            chungTuMuaHang.MaThongKe = chungTuMuaHangVm.MaThongKe;
            chungTuMuaHang.MaChungTuMuaHang = chungTuMuaHangVm.MaChungTuMuaHang;



        }
      
      
        public static void UpdateChiTietDonDatHang(this ChiTietDonDatHang chiTietDonDatHang, ChiTietDonDatHangViewModel chiTietDonDatHangVm)
        {
            chiTietDonDatHang.MaChiTietDonDatHang = chiTietDonDatHangVm.MaChiTietDonDatHang;
            chiTietDonDatHang.MaDonDatHang = chiTietDonDatHangVm.MaDonDatHang;
            chiTietDonDatHang.MaHang = chiTietDonDatHangVm.MaHang;
            chiTietDonDatHang.SoLuong = chiTietDonDatHangVm.SoLuong;
            chiTietDonDatHang.SoLuongDaGiao = chiTietDonDatHangVm.SoLuongDaGiao;
            chiTietDonDatHang.ThanhTien = chiTietDonDatHangVm.ThanhTien;
            chiTietDonDatHang.TienThueGTGT = chiTietDonDatHangVm.TienThueGTGT;
        }
        public static void UpdateDonDatHang(this DonDatHang donDatHang, DonDatHangViewModel donDatHangVm)
        {
            donDatHang.MaDonDatHang = donDatHangVm.MaDonDatHang;
            donDatHang.MaKhachHang = donDatHangVm.MaKhachHang;
            donDatHang.NguoiNhanHang = donDatHangVm.NguoiNhanHang;
            donDatHang.DienGiai = donDatHangVm.DienGiai;
            donDatHang.MaSoNhanVien = donDatHangVm.MaSoNhanVien;
            donDatHang.MaDieuKhoan = donDatHangVm.MaDieuKhoan;
            donDatHang.SoNgayDuocNo = donDatHangVm.SoNgayDuocNo;
            donDatHang.NgayDonHang = donDatHangVm.NgayDonHang;
            donDatHang.MaTinhTrang = donDatHangVm.MaTinhTrang;
            donDatHang.NgayGiaoHang = donDatHangVm.NgayGiaoHang;
            donDatHang.MaLoaiTien = donDatHangVm.MaLoaiTien;
            donDatHang.TyGia = donDatHangVm.TyGia;
            donDatHang.TongTienHang = donDatHangVm.TongTienHang;
            donDatHang.TienThueGTGT = donDatHangVm.TienThueGTGT;
            donDatHang.TongChietKhau = donDatHangVm.TongChietKhau;
            donDatHang.TongTienThanhToan = donDatHangVm.TongTienThanhToan;
        }

      
      
      
      
        public static void UpdateHoaDon_BanHang(this HoaDon_BanHang hoaDon_BanHang, HoaDon_BanHangViewModel hoaDon_BanHangVm)
        {
            hoaDon_BanHang.MaHoaDon = hoaDon_BanHangVm.MaHoaDon;
            hoaDon_BanHang.MaKhachHang = hoaDon_BanHangVm.MaKhachHang;
            hoaDon_BanHang.TKNganHang = hoaDon_BanHangVm.TKNganHang;
            hoaDon_BanHang.NguoiMuaHang = hoaDon_BanHangVm.NguoiMuaHang;
            hoaDon_BanHang.HinhThucTT = hoaDon_BanHangVm.HinhThucTT;
            hoaDon_BanHang.MauSoHD = hoaDon_BanHangVm.MauSoHD;
            hoaDon_BanHang.KyHieuHD = hoaDon_BanHangVm.KyHieuHD;
            hoaDon_BanHang.NgayHoaDon = hoaDon_BanHangVm.NgayHoaDon;
            hoaDon_BanHang.MaDieuKhoan = hoaDon_BanHangVm.MaDieuKhoan;
            hoaDon_BanHang.SoNgayDuocNo = hoaDon_BanHangVm.SoNgayDuocNo;
            hoaDon_BanHang.HanThanhToan = hoaDon_BanHangVm.HanThanhToan;
            hoaDon_BanHang.TyGia = hoaDon_BanHangVm.TyGia;
            hoaDon_BanHang.TongTienHang = hoaDon_BanHangVm.TongTienHang;
            hoaDon_BanHang.TienThueGTGT = hoaDon_BanHangVm.TienThueGTGT;
            hoaDon_BanHang.TienChietKhau = hoaDon_BanHangVm.TienChietKhau;
            hoaDon_BanHang.TongTienThanhToan = hoaDon_BanHangVm.TongTienThanhToan;
            hoaDon_BanHang.MaLoaiTien = hoaDon_BanHangVm.MaLoaiTien;
            hoaDon_BanHang.MaChungTuBanHang = hoaDon_BanHangVm.MaChungTuBanHang;
            hoaDon_BanHang.MaSoNhanVien = hoaDon_BanHangVm.MaSoNhanVien;
        }
        public static void UpdateChiTietHoaDon_BanHang(this ChiTietHoaDon_BanHang chiTietHoaDon_BanHang, ChiTietHoaDon_BanHangViewModel chiTietHoaDon_BanHangVm)
        {
            chiTietHoaDon_BanHang.MaChiTietHoaDonBanHang = chiTietHoaDon_BanHangVm.MaChiTietHoaDonBanHang;
            chiTietHoaDon_BanHang.MaHoaDon = chiTietHoaDon_BanHangVm.MaHoaDon;
            chiTietHoaDon_BanHang.MaHang = chiTietHoaDon_BanHangVm.MaHang;
            chiTietHoaDon_BanHang.TKCongNo_ChiPhi = chiTietHoaDon_BanHangVm.TKCongNo_ChiPhi;
            chiTietHoaDon_BanHang.TKDoanhThu = chiTietHoaDon_BanHangVm.TKDoanhThu;
            chiTietHoaDon_BanHang.SoLuong = chiTietHoaDon_BanHangVm.SoLuong;
            chiTietHoaDon_BanHang.ThanhTien = chiTietHoaDon_BanHangVm.ThanhTien;
            chiTietHoaDon_BanHang.TienChietKhau = chiTietHoaDon_BanHangVm.TienChietKhau;
            chiTietHoaDon_BanHang.TienThueGTGT = chiTietHoaDon_BanHangVm.TienThueGTGT;
         
    }
      
     
       
       
       
       
        
     
      
        
     

        public static void UpdateChungTuGiaoHang(this ChungTuGiaoHang chungTuGiaoHang, ChungTuGiaoHangViewModel chungTuGiaoHangVm)
        {
            chungTuGiaoHang.MaChungTuGiaoHang = chungTuGiaoHangVm.MaChungTuGiaoHang;
            chungTuGiaoHang.MaChungTuBanHang = chungTuGiaoHangVm.MaChungTuBanHang;
            chungTuGiaoHang.MaNguoiGiaoHang = chungTuGiaoHangVm.MaNguoiGiaoHang;
            chungTuGiaoHang.MaSoNhanVien = chungTuGiaoHangVm.MaSoNhanVien;
            chungTuGiaoHang.TrongLuong = chungTuGiaoHangVm.TrongLuong;
            chungTuGiaoHang.DiaChiGui = chungTuGiaoHangVm.DiaChiGui;
            chungTuGiaoHang.SdtNguoiNhan = chungTuGiaoHangVm.SdtNguoiNhan;
            chungTuGiaoHang.TongTienHang = chungTuGiaoHangVm.TongTienHang;
            chungTuGiaoHang.GioGiaoDuKien = chungTuGiaoHangVm.GioGiaoDuKien;

        }
        public static void UpdateDoiTacGiaoHang(this DoiTacGiaoHang doiTacGiaoHang, DoiTacGiaoHangViewModel doiTacGiaoHangVM)
        {
            doiTacGiaoHang.MaNguoiGiaoHang = doiTacGiaoHangVM.MaNguoiGiaoHang;
            doiTacGiaoHang.TenNguoiGiaoHang = doiTacGiaoHangVM.TenNguoiGiaoHang;
            doiTacGiaoHang.MaNhaCungCap = doiTacGiaoHangVM.MaNhaCungCap;
            doiTacGiaoHang.PhanLoai = doiTacGiaoHangVM.PhanLoai;
            doiTacGiaoHang.XungHo = doiTacGiaoHangVM.XungHo;
            doiTacGiaoHang.TenNguoiGiaoHang = doiTacGiaoHangVM.TenNguoiGiaoHang;
            doiTacGiaoHang.ChiNhanh = doiTacGiaoHangVM.ChiNhanh;
            doiTacGiaoHang.DiaChi = doiTacGiaoHangVM.DiaChi;
            doiTacGiaoHang.SoDTDD_1 = doiTacGiaoHangVM.SoDTDD_1;
            doiTacGiaoHang.SoDTDD_2 = doiTacGiaoHangVM.SoDTDD_2;
            doiTacGiaoHang.DienThoaiCoDinh = doiTacGiaoHangVM.DienThoaiCoDinh;
            doiTacGiaoHang.Fax = doiTacGiaoHangVM.Fax;
            doiTacGiaoHang.Email = doiTacGiaoHangVM.Email;
            doiTacGiaoHang.NgaySinh = doiTacGiaoHangVM.NgaySinh;
            doiTacGiaoHang.MaSoThue = doiTacGiaoHangVM.MaSoThue;
            doiTacGiaoHang.SoTaiKhoanNganHang = doiTacGiaoHangVM.SoTaiKhoanNganHang;
            doiTacGiaoHang.TenNganHang = doiTacGiaoHangVM.TenNganHang;
            doiTacGiaoHang.TongDonDaGiao = doiTacGiaoHangVM.TongDonDaGiao;
            doiTacGiaoHang.TongTienDaGiao = doiTacGiaoHangVM.TongTienDaGiao;
            doiTacGiaoHang.TongTrongLuongDaGiao = doiTacGiaoHangVM.TongTrongLuongDaGiao;
            doiTacGiaoHang.CMND = doiTacGiaoHangVM.CMND;
            doiTacGiaoHang.NgayCap = doiTacGiaoHangVM.NgayCap;
            doiTacGiaoHang.NoiCap = doiTacGiaoHangVM.NoiCap;
            doiTacGiaoHang.DangHoatDong = doiTacGiaoHangVM.DangHoatDong;
            doiTacGiaoHang.ChungTuGiaoHangs = doiTacGiaoHangVM.ChungTuGiaoHangs;
            doiTacGiaoHang.NgayTao = doiTacGiaoHangVM.NgayTao;
            doiTacGiaoHang.MaNguoiTao = doiTacGiaoHangVM.MaNguoiTao;
            doiTacGiaoHang.DaXoa = doiTacGiaoHangVM.DaXoa;
            doiTacGiaoHang.MaNguoiXoa = doiTacGiaoHangVM.MaNguoiXoa;
            doiTacGiaoHang.NgayXoa = doiTacGiaoHangVM.NgayXoa;
        }
        public static void UpdateTrangThaiGiaoHang(this TrangThaiGiaoHang chiTietTrangThai, TrangThaiGiaoHangViewModel chiTietTrangThaiVm)
        {
            chiTietTrangThai.MaChiTietTrangThai = chiTietTrangThaiVm.MaChiTietTrangThai;
            chiTietTrangThai.MaChungTuDatHang = chiTietTrangThaiVm.MaChungTuDatHang;
            chiTietTrangThai.MaNhanVienTiepNhan = chiTietTrangThaiVm.MaNhanVienTiepNhan;
            chiTietTrangThai.MaTrangThai = chiTietTrangThaiVm.MaTrangThai;
            chiTietTrangThai.NgayGio = chiTietTrangThaiVm.NgayGio;
            chiTietTrangThai.LyDo = chiTietTrangThaiVm.LyDo;
            chiTietTrangThai.DaLayHang = chiTietTrangThaiVm.DaLayHang;
            chiTietTrangThai.ChoHienThi = chiTietTrangThaiVm.ChoHienThi;
        }
       
        public static void UpdateNhomHangHoa(this NhomHangHoa nhomHangHoa, NhomHangHoaViewModel nhomHangHoaVm)
        {
            nhomHangHoa.MaNhomHH = nhomHangHoaVm.MaNhomHH;
            nhomHangHoa.TenNhom = nhomHangHoaVm.TenNhom;
            nhomHangHoa.GhiChu = nhomHangHoaVm.GhiChu;
          
        }
       
        public static void UpdateDanhSachTrangThaiDonMuaHang(this DanhSachTrangThaiDonMuaHang danhSachTrangThaiDonMuaHang, DanhSachTrangThaiDonMuaHangViewModel danhSachTrangThaiDonMuaHangVm)
        {
            danhSachTrangThaiDonMuaHang.MaTrangThai = danhSachTrangThaiDonMuaHangVm.MaTrangThai;
            danhSachTrangThaiDonMuaHang.TenTrangThai = danhSachTrangThaiDonMuaHangVm.TenTrangThai;
            danhSachTrangThaiDonMuaHang.GhiChu = danhSachTrangThaiDonMuaHangVm.GhiChu;
        }
     
      
      
        public static void UpdateLichSuDatHang(this LichSuDatHang lichSuDatHang, LichSuDatHangViewModel lichSuDatHangVm)
        {
            lichSuDatHang.MaLichSuDatHang = lichSuDatHangVm.MaLichSuDatHang;
            lichSuDatHang.Id = lichSuDatHangVm.Id;
            lichSuDatHang.MaChungTuDatHang = lichSuDatHangVm.MaChungTuDatHang;
            lichSuDatHang.DangXuLy = lichSuDatHangVm.DangXuLy;
            lichSuDatHang.NgayGioDangXuLy = lichSuDatHangVm.NgayGioDangXuLy;
            lichSuDatHang.DaNhanDon = lichSuDatHangVm.DaNhanDon;
            lichSuDatHang.NgayGioDaNhanDon = lichSuDatHangVm.NgayGioDaNhanDon;
            lichSuDatHang.DangGiaoHang = lichSuDatHangVm.DangGiaoHang;
            lichSuDatHang.NgayGioDangGiaoHang = lichSuDatHangVm.NgayGioDangGiaoHang;
            lichSuDatHang.DaNhanHang = lichSuDatHangVm.DaNhanHang;
            lichSuDatHang.NgayGioDaNhanHang = lichSuDatHangVm.NgayGioDaNhanHang;
            lichSuDatHang.DaHuy = lichSuDatHangVm.DaHuy;
            lichSuDatHang.NgayGioDaHuy = lichSuDatHangVm.NgayGioDaHuy;
            lichSuDatHang.LyDo = lichSuDatHangVm.LyDo;
        }
        public static void UpdateChungTuDatHang(this ChungTuDatHang chungTuDatHang, ChungTuDatHangViewModel chungTuDatHangVm)
        {
            chungTuDatHang.MaChungTuDatHang = chungTuDatHangVm.MaChungTuDatHang;
            chungTuDatHang.MaKhachHang = chungTuDatHangVm.MaKhachHang;
            chungTuDatHang.DienGiai = chungTuDatHangVm.DienGiai;
            chungTuDatHang.MaSoNhanVien = chungTuDatHangVm.MaSoNhanVien;
            chungTuDatHang.NgayHoachToan = chungTuDatHangVm.NgayHoachToan;
            chungTuDatHang.NgayChungTu = chungTuDatHangVm.NgayChungTu;
            chungTuDatHang.MaDieuKhoan = chungTuDatHangVm.MaDieuKhoan;
            chungTuDatHang.SoNgayDuocNo = chungTuDatHangVm.SoNgayDuocNo;
            chungTuDatHang.HanThanhToan = chungTuDatHangVm.HanThanhToan;
            chungTuDatHang.MaLoaiTien = chungTuDatHangVm.MaLoaiTien;
            chungTuDatHang.TyGia = chungTuDatHangVm.TyGia;
            chungTuDatHang.TongTienHang = chungTuDatHangVm.TongTienHang;
            chungTuDatHang.TienThueGTGT = chungTuDatHangVm.TienThueGTGT;
            chungTuDatHang.TienChietKhau = chungTuDatHangVm.TienChietKhau;
            chungTuDatHang.TongTienThanhToan = chungTuDatHangVm.TongTienThanhToan;
            chungTuDatHang.DaGhiSo = chungTuDatHangVm.DaGhiSo;
            chungTuDatHang.MaSoBaoGia = chungTuDatHangVm.MaSoBaoGia;
            chungTuDatHang.MaPhieuGoc = chungTuDatHangVm.MaPhieuGoc;
            chungTuDatHang.MaPhieuMoi = chungTuDatHangVm.MaPhieuMoi;
            chungTuDatHang.DaThayDoi = chungTuDatHangVm.DaThayDoi;
            chungTuDatHang.NgayThayDoi = chungTuDatHangVm.NgayThayDoi;
            chungTuDatHang.NhanVienThayDoi = chungTuDatHangVm.NhanVienThayDoi;
            chungTuDatHang.CoSoThayDoi = chungTuDatHangVm.CoSoThayDoi;
            chungTuDatHang.MaKenhBanHang = chungTuDatHangVm.MaKenhBanHang;
            chungTuDatHang.DaThanhToan = chungTuDatHangVm.DaThanhToan;
            chungTuDatHang.MaHinhThucThanhToan = chungTuDatHangVm.MaHinhThucThanhToan;
            chungTuDatHang.MaTinhTrang = chungTuDatHangVm.MaTinhTrang;
            chungTuDatHang.ThoiGianMuonNhanHang = chungTuDatHangVm.ThoiGianMuonNhanHang;
            chungTuDatHang.IsViewed = chungTuDatHangVm.IsViewed;
            chungTuDatHang.SoTienDaTra = chungTuDatHangVm.SoTienDaTra;
            chungTuDatHang.TienThuHienTai = chungTuDatHangVm.TienThuHienTai;
            chungTuDatHang.TyLeChietKhauHoaDon = chungTuDatHangVm.TyLeChietKhauHoaDon;
            chungTuDatHang.TongTienKhuyenMai = chungTuDatHangVm.TongTienKhuyenMai;
            chungTuDatHang.ChungTuThamChieu = chungTuDatHangVm.ChungTuThamChieu;
        }
        public static void UpdateChiTietChungTuDatHang(this ChiTietChungTuDatHang chiTietChungTuDatHang, ChiTietChungTuDatHangViewModel chiTietChungTuDatHangVm)
        {
            chiTietChungTuDatHang.MaChiTietChungTuDatHang = chiTietChungTuDatHangVm.MaChiTietChungTuDatHang;
            chiTietChungTuDatHang.MaChungTuDatHang = chiTietChungTuDatHangVm.MaChungTuDatHang;
            chiTietChungTuDatHang.MaHang = chiTietChungTuDatHangVm.MaHang;
            chiTietChungTuDatHang.TKCongNo_ChiPhi = chiTietChungTuDatHangVm.TKCongNo_ChiPhi;
            chiTietChungTuDatHang.TKDoanhThu = chiTietChungTuDatHangVm.TKDoanhThu;
            chiTietChungTuDatHang.SoLuong = chiTietChungTuDatHangVm.SoLuong;
            chiTietChungTuDatHang.DonGia = chiTietChungTuDatHangVm.DonGia;
            chiTietChungTuDatHang.KhuyenMai = chiTietChungTuDatHangVm.KhuyenMai;
            chiTietChungTuDatHang.ThanhTien = chiTietChungTuDatHangVm.ThanhTien;
            chiTietChungTuDatHang.TienChietKhau = chiTietChungTuDatHangVm.TienChietKhau;
            chiTietChungTuDatHang.TienThueGTGT = chiTietChungTuDatHangVm.TienThueGTGT;
            chiTietChungTuDatHang.MaDonViTinh = chiTietChungTuDatHangVm.MaDonViTinh;
            chiTietChungTuDatHang.GiaKhuyenMai = chiTietChungTuDatHangVm.GiaKhuyenMai;
            chiTietChungTuDatHang.KhuyenMai2 = chiTietChungTuDatHangVm.KhuyenMai2;
            //chiTietChungTuDatHang.SoLuongKhuyenMai2 = chiTietChungTuDatHangVm.SoLuongKhuyenMai2;

        }
        public static void UpdatePhanHoi(this PhanHoi phanHoi, PhanHoiViewModel phanHoiVm)
        {
            phanHoi.ID = phanHoiVm.ID;
            phanHoi.Ten = phanHoiVm.Ten;
            phanHoi.Email = phanHoiVm.Email;
            phanHoi.Website = phanHoiVm.Website;
            phanHoi.SoDienThoai = phanHoiVm.SoDienThoai;
            phanHoi.DiaChi = phanHoiVm.DiaChi;
           
            phanHoi.GhiChu = phanHoiVm.GhiChu;
            phanHoi.NgayTao = phanHoiVm.NgayTao;
            phanHoi.TrangThai = phanHoiVm.TrangThai;
            phanHoi.MaTinh_TP = phanHoiVm.MaTinh_TP;
            phanHoi.MaQuan_Huyen = phanHoiVm.MaQuan_Huyen;

            ;
        }
       
       
      
      
      
       
    }
}
