﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;
using Platform.Web.App_Start;
using Microsoft.AspNet.Identity;
using System.Web.Script.Serialization;

namespace Platform.Web.Api
{
    [RoutePrefix("api/chungtudathang")]

    public class ChungTuDatHangController : ApiControllerBase
    {
        IChungTuDatHangService _chungTuDatHangService;
        IChiTietChungTuDatHangService _chiTietChungTuDatHangService;
        IKhachHangService _khachHangService;
        ITrangThaiGiaoHangService _trangThaiGiaoHangService;
        

        private ApplicationUserManager _userManager;

        public ChungTuDatHangController(ILoiService loiService, ITrangThaiGiaoHangService trangThaiGiaoHangService, IChungTuDatHangService chungTuDatHangService, ApplicationUserManager userManager, IChiTietChungTuDatHangService chiTietChungTuDatHangService, IKhachHangService khachHangService) : base(loiService)
        {
            this._chungTuDatHangService = chungTuDatHangService;
            this._userManager = userManager;
            this._chiTietChungTuDatHangService = chiTietChungTuDatHangService;
            this._khachHangService = khachHangService;
            this._trangThaiGiaoHangService = trangThaiGiaoHangService;
        }
        

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, ChungTuDatHangViewModel ctdhVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var KH=_khachHangService.GetByEmail(ctdhVm.Email);
                    var newCTDH = new ChungTuDatHang();
                    newCTDH.UpdateChungTuDatHang(ctdhVm);
                    newCTDH.MaKhachHang = KH.MaKhachHang;
                    newCTDH.MaSoNhanVien = "0000";
                    newCTDH.SoTienDaTra = 0;
                   
                    newCTDH.NgayChungTu = DateTime.Now;
                    newCTDH.NgayHoachToan = DateTime.Now;
                    newCTDH.ThoiGianMuonNhanHang = DateTime.Now;
                    newCTDH.MaHinhThucThanhToan = 4;
                    newCTDH.TienThuHienTai = 0;
                    newCTDH.TyLeChietKhauHoaDon = 0;
                    newCTDH.DaGhiSo = true;
                    //var chitiet = _chiTietChungTuDatHangService.GetByMaCTDH(newCTDH.MaChungTuDatHang);
                    //newCTDH.TongTienKhuyenMai = chitiet.Sum(x => x.GiaKhuyenMai);
                    _chungTuDatHangService.Add(newCTDH);
                    _chungTuDatHangService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK, newCTDH.MaChungTuDatHang);


                }
                return response;
            });

        }
      

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listChungTuDatHangtegory = _chungTuDatHangService.GetAll();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listChungTuDatHangtegory);
                return response;
            });
        }
        [Route("getByMaCT")]
        public HttpResponseMessage getByMaCT(HttpRequestMessage request,string mact)
        {
            return CreateHttpResponse(request, () =>
            {
                var ChungTuDatHang = _chungTuDatHangService.getByMaCT(mact);
                var map = Mapper.Map<ChungTuDatHang, ChungTuDatHangViewModel>(ChungTuDatHang);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, map);
                return response;
            });
        }
        [Route("DanhSachChiTietTrangThaiDonGiaoHang")]
        [HttpGet]
        public HttpResponseMessage DanhSachChiTietTrangThaiDonGioaHang(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var KH = _khachHangService.GetAll();
                var MAKH = "";
                foreach (var item in KH)
                {
                    if (item.Email != null)
                    {
                       // var email = _userManager.FindByEmail(item.Email);
                        if (item.Email == User.Identity.GetUserName())
                        {
                            MAKH = item.MaKhachHang;
                        }
                    }

                }
                var listChungTuDatHangtegory = _chungTuDatHangService.GetByKH(MAKH);

                var map = Mapper.Map<IEnumerable<ChungTuDatHang> ,IEnumerable<DanhSachChiTietTrangThaiDonGiaoHang>>(listChungTuDatHangtegory);
                foreach (var item in map)
                {
                    item.ChiTiet = _chiTietChungTuDatHangService.GetAll().Where(x => x.MaChungTuDatHang == item.MaChungTuDatHang);
                    item.TrangThaiGiaoHang = _trangThaiGiaoHangService.LayTenTrangThai(item.MaChungTuDatHang);

                }
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, map);
                return response;
            });
        }
        [Route("getbyuser")]
        [HttpGet]
        public HttpResponseMessage getbyuser(HttpRequestMessage request, string Email)
        {
            return CreateHttpResponse(request, () =>
            {

                var laygiohang = _chiTietChungTuDatHangService.getchitietchungtudathangbyemail(Email);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, laygiohang);
                return response;
            });
        }


        [Route("getMaCT")]
        public HttpResponseMessage getMaCT(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
               
                var KH = _khachHangService.GetAll();
                var MAKH = "";
                foreach (var item in KH)
                {
                    if (item.Email != null)
                    {
                        var email = _userManager.FindByEmail(item.Email);
                        if (email != null)
                        {
                            MAKH = item.MaKhachHang;
                        }
                    }

                }
                var listChungTuDatHangtegory = _chungTuDatHangService.GetAll().Where( x=>x.MaTinhTrang==1&&x.DaThanhToan==false&&x.MaKhachHang== MAKH).FirstOrDefault();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listChungTuDatHangtegory);
                return response;
            });
        }


        [Route("getid")]
        public HttpResponseMessage getid(HttpRequestMessage request, string id )
        {
            return CreateHttpResponse(request, () =>
            {
                var listChungTuDatHangtegory = _chungTuDatHangService.GetByID(id);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listChungTuDatHangtegory);
                return response;
            });
        }
        
        [Route("checklength_giohang")]
        public HttpResponseMessage checklength_giohang(HttpRequestMessage request )
        {
            return CreateHttpResponse(request, () =>
            {
                var iduser = User.Identity.GetUserName();
                var listCategory = _chiTietChungTuDatHangService.getchitietchungtudathangbyemail(iduser).Count();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }



        [Route("delete")]
        [HttpDelete]
        public HttpResponseMessage Delete(HttpRequestMessage request, string id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                  var  trangthaiID= _trangThaiGiaoHangService.GetAll().Where(x => x.MaChungTuDatHang == id).FirstOrDefault();
                    if(trangthaiID == null)
                    {
                        return response = request.CreateResponse(HttpStatusCode.OK);
                    }
                    _trangThaiGiaoHangService.delete(trangthaiID.MaChiTietTrangThai);
                    _trangThaiGiaoHangService.Commit();
                    _chungTuDatHangService.delete(id);
                    _chungTuDatHangService.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });
        }
        [Route("update")]
        [HttpPut]
        public HttpResponseMessage update(HttpRequestMessage request, ChungTuDatHangViewModel giohangVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var giohang = _chungTuDatHangService.GetByID(giohangVm.MaChungTuDatHang);
                    giohang.UpdateChungTuDatHang(giohangVm);
                    _chungTuDatHangService.Update(giohang);
                    _chungTuDatHangService.Commit();
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("MaChungTuMoi", giohangVm.MaChungTuDatHang);
                    dict.Add("TongTienThanhToan", Convert.ToString(giohangVm.TongTienThanhToan));
                    dict.Add("ThoiGianMuonNhanHang", Convert.ToDateTime(giohangVm.ThoiGianMuonNhanHang).ToString());
                    dict.Add("HinhThucThanhToan", _chungTuDatHangService.getbyid(giohangVm.MaHinhThucThanhToan.Value).TenHinhThucThanhToan);
                    dict.Add("DiaChi",
                        _khachHangService.getDiaChiKH(
                            _khachHangService.GetKhachHangTheoMa(giohangVm.MaKhachHang).FirstOrDefault().Tinh_TP, 
                            _khachHangService.GetKhachHangTheoMa(giohangVm.MaKhachHang).FirstOrDefault().MaQuan_Huyen));

                    Hashtable ht = new Hashtable(dict);
                    response = request.CreateResponse(HttpStatusCode.OK, ht);
                }
                return response;
            });
        }
        [Route("laymamoinhat")]
        [HttpGet]
        public HttpResponseMessage laymamoinhat(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var ID = _chungTuDatHangService.GetNewID();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, ID);
                return response;
            });
        }
        [Route("updatestatus")]
        [HttpGet]
        public HttpResponseMessage updatestatus(HttpRequestMessage request,string id)
        {
            return CreateHttpResponse(request, () =>
            {
                var CTDH = _chungTuDatHangService.GetByID(id);
                CTDH.IsViewed = true;
                _chungTuDatHangService.Update(CTDH);
                _chungTuDatHangService.Commit();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK);
                return response;
            });
        }
      
        
      
        [Route("capnhatsaukhixacnhan")]
        [HttpPut]
        public HttpResponseMessage capnhatsaukhixacnhan(HttpRequestMessage request, ChungTuDatHangViewModel giohangVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var giohang = _chungTuDatHangService.GetByID(giohangVm.MaChungTuDatHang);
                    giohang.TongTienThanhToan = giohangVm.TongTienThanhToan;
                    giohang.ThoiGianMuonNhanHang = giohangVm.ThoiGianMuonNhanHang;
                    giohang.DaThanhToan = giohangVm.DaThanhToan;
                    giohang.MaHinhThucThanhToan = giohangVm.MaHinhThucThanhToan;
                    _chungTuDatHangService.Update(giohang);
                    _chungTuDatHangService.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });
        }
    }
}
