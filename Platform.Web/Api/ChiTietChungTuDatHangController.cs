﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;
using Platform.Web.App_Start;
using Microsoft.AspNet.Identity;

namespace Platform.Web.Api
{
    [RoutePrefix("api/chitietchungtudathang")]

    public class ChiTietChungTuDatHangController : ApiControllerBase
    {
        IChiTietChungTuDatHangService _chiTietChungTuDatHangService;
        IChungTuDatHangService _chungTuDatHangService;
        IKhachHangService _khachHangService;
        private ApplicationUserManager _userManager;
        public ChiTietChungTuDatHangController(ILoiService loiService, IChiTietChungTuDatHangService chiTietChungTuDatHangService, ApplicationUserManager userManager, IChungTuDatHangService chungTuDatHangService, IKhachHangService khachHangService) : base(loiService)
        {
            this._chiTietChungTuDatHangService = chiTietChungTuDatHangService;
            this._chungTuDatHangService = chungTuDatHangService;
            this._khachHangService = khachHangService;
            this._userManager = userManager;
        }
        

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage update(HttpRequestMessage request, ChiTietChungTuDatHangViewModel chiTietChungTuDatHangViewModel)

        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   
                    if (chiTietChungTuDatHangViewModel.KhuyenMai == null)
                    {
                        var id = _chiTietChungTuDatHangService.getbyIdKhongCoMaGoc(chiTietChungTuDatHangViewModel.MaHang, chiTietChungTuDatHangViewModel.MaChungTuDatHang);
                        chiTietChungTuDatHangViewModel.MaChiTietChungTuDatHang = id.MaChiTietChungTuDatHang;
                        var vienchucDb = _chiTietChungTuDatHangService.GetByID(chiTietChungTuDatHangViewModel.MaChiTietChungTuDatHang);

                        vienchucDb.UpdateChiTietChungTuDatHang(chiTietChungTuDatHangViewModel);
                        _chiTietChungTuDatHangService.Update(vienchucDb);
                        _chiTietChungTuDatHangService.Commit();
                    }
                    else if (chiTietChungTuDatHangViewModel.KhuyenMai != null) {
                        var id = _chiTietChungTuDatHangService.getbyId(chiTietChungTuDatHangViewModel.MaHang, chiTietChungTuDatHangViewModel.MaChungTuDatHang);
                        chiTietChungTuDatHangViewModel.MaChiTietChungTuDatHang = id.MaChiTietChungTuDatHang;
                        var vienchucDb = _chiTietChungTuDatHangService.GetByID(chiTietChungTuDatHangViewModel.MaChiTietChungTuDatHang);

                        vienchucDb.UpdateChiTietChungTuDatHang(chiTietChungTuDatHangViewModel);
                        _chiTietChungTuDatHangService.Update(vienchucDb);
                        _chiTietChungTuDatHangService.Commit();

                    }
                   

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
        [Route("updateList")]
        [HttpPut]
        public HttpResponseMessage updateList(HttpRequestMessage request, IEnumerable<GioHangTam> gioHangTam)

        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var ChungTuDatHang = _chungTuDatHangService.getByMaCT(gioHangTam.FirstOrDefault().MaChungTuDatHang);
                    var iduser = User.Identity.GetUserName();
                    var listCategory = _chiTietChungTuDatHangService.getchitietchungtudathangbyemail(iduser).ToList();
                    double Sum = 0;
                    foreach (var item in gioHangTam)
                    {
                        var map = Mapper.Map<GioHangTam, ChiTietChungTuDatHang>(item);
                        var index = listCategory.FindIndex(x => x.MaHang == item.MaHang);
                        if (index != -1)
                        {
                            listCategory[index].SoLuong += item.SoLuong;
                            listCategory[index].ThanhTien += item.ThanhTien;
                            Sum += listCategory[index].ThanhTien.Value;
                            var map1 = Mapper.Map<ListChiTietChungTuDatHang, ChiTietChungTuDatHang>(listCategory[index]);
                            _chiTietChungTuDatHangService.Update(map1);
                            _chiTietChungTuDatHangService.Commit();
                        }
                        else {
                              Sum += item.ThanhTien.Value;
                            _chiTietChungTuDatHangService.Add(map);
                            _chiTietChungTuDatHangService.Commit();
                        }
                    }
                    ChungTuDatHang.TongTienHang += Sum;
                    ChungTuDatHang.TongTienThanhToan=+ Sum;
                   
                    _chungTuDatHangService.Update(ChungTuDatHang);
                    _chungTuDatHangService.Commit();




                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }



        [Route("delete")]
        [HttpDelete]
        public HttpResponseMessage Delete(HttpRequestMessage request, string mahang,string mact)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var id = _chiTietChungTuDatHangService.GetByChiTietMaHang(mahang, mact);
                    _chiTietChungTuDatHangService.delete(id.MaChiTietChungTuDatHang);
                    _chiTietChungTuDatHangService.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });
        }



        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request,ChiTietChungTuDatHangViewModel chiTietChungTuDatHangVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   
                        var newThongBao = new ChiTietChungTuDatHang();
                   
                        newThongBao.UpdateChiTietChungTuDatHang(chiTietChungTuDatHangVM);
                        
                        _chiTietChungTuDatHangService.Add(newThongBao);
                        _chiTietChungTuDatHangService.Save();
                    
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }
        
        [Route("createList")]
        [HttpPost]
        public HttpResponseMessage CreateList(HttpRequestMessage request,IEnumerable<GioHangTam>  gioHangTam)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                            foreach (var item in gioHangTam)
                            {
                            var newchitiet = new ChiTietChungTuDatHang();
                            var map = Mapper.Map<GioHangTam, ChiTietChungTuDatHangViewModel>(item);
                            newchitiet.MaChungTuDatHang = item.MaChungTuDatHang;
                            newchitiet.MaHang = item.MaHang;
                            newchitiet.SoLuong = item.SoLuong;
                            newchitiet.DonGia = item.DonGia;
                            newchitiet.KhuyenMai = item.KhuyenMai;
                            newchitiet.MaDonViTinh = item.MaDonViTinh;
                            newchitiet.ThanhTien = item.ThanhTien;
                            newchitiet.UpdateChiTietChungTuDatHang(map);
                             _chiTietChungTuDatHangService.Add(newchitiet);
                            _chiTietChungTuDatHangService.Save();


                            } 

                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }



        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _chiTietChungTuDatHangService.GetAll();
                //  var responseData = Mapper.Map<IEnumerable<ChiTietChungTuDatHang>,IEnumerable<ChiTietChungTuDatHangViewModel>>(listCategory);
               // var b = listCategory.OrderBy(x => x.MaChiTietChungTuDatHang.Length + x.MaChiTietChungTuDatHang);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        [Route("getbyMaHang")]
        public HttpResponseMessage getbyMaHang(HttpRequestMessage request,string mahang,string mact)
        {
            return CreateHttpResponse(request, () =>
            {

                var listCategory = _chiTietChungTuDatHangService.GetByChiTietMaHang(mahang, mact);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }






        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage getbyid(HttpRequestMessage request,int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _chiTietChungTuDatHangService.GetByID(id);
               HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }



        [Route("getbyemail")]
        [HttpGet]
        public HttpResponseMessage getbyemail(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var iduser = User.Identity.GetUserName();
                Console.WriteLine(iduser);
                var listCategory = _chiTietChungTuDatHangService.getchitietchungtudathangbyemail(iduser);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }


        /******lấy danh sách hàng hóa của chứng từ đặt hàng đó********/
        [Route("getchitietchungtudathang")]
        [HttpGet]
        public HttpResponseMessage getchitietchungtudathang(HttpRequestMessage request,string Id)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _chiTietChungTuDatHangService.getchitietchungtudathang(Id);
               HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }
        

      

        [Route("getalll")]
        [HttpGet]
        public HttpResponseMessage getall(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _chiTietChungTuDatHangService.GetAll();
                
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }
        [Route("capnhatsaukhixacnhan")]
        [HttpPut]
        public HttpResponseMessage capnhatsaukhixacnhan(HttpRequestMessage request, IEnumerable<ChiTietChungTuDatHangViewModel> chiTietChungTuDatHangViewModel)

        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var chitietdaco = _chiTietChungTuDatHangService.GetByMaCTDH(chiTietChungTuDatHangViewModel.FirstOrDefault().MaChungTuDatHang);
                    var map = Mapper.Map<IEnumerable<ChiTietChungTuDatHang>, IEnumerable<ChiTietChungTuDatHangViewModel>>(chitietdaco).ToList();
                    foreach (var item in map)
                    {
                        var checkdaxoa = chiTietChungTuDatHangViewModel.ToList().FindIndex(x => x.MaChiTietChungTuDatHang == item.MaChiTietChungTuDatHang);
                        if (checkdaxoa == -1)
                        {
                            _chiTietChungTuDatHangService.delete(item.MaChiTietChungTuDatHang);
                            _chiTietChungTuDatHangService.Commit();
                        }
                    }
                    foreach (var item in chiTietChungTuDatHangViewModel)
                    {
                        var checkdaco = map.FindIndex(x => x.MaChiTietChungTuDatHang == item.MaChiTietChungTuDatHang);
                        if (checkdaco == -1)
                        {
                            var newPhongHoc = new ChiTietChungTuDatHang();
                            newPhongHoc.UpdateChiTietChungTuDatHang(item);
                            _chiTietChungTuDatHangService.Add(newPhongHoc);
                        }
                        else
                        {
                            var tim = _chiTietChungTuDatHangService.GetByID(item.MaChiTietChungTuDatHang);
                            tim.UpdateChiTietChungTuDatHang(item);
                            _chiTietChungTuDatHangService.Update(tim);
                        }

                        _chiTietChungTuDatHangService.Commit();
                    }
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });
        }


    }
}
