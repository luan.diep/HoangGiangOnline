﻿
using Platform.Common;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
   


    [RoutePrefix("api/phanhoi")]
   // [Authorize]
    public class PhanHoiController : ApiControllerBase
    {
        IPhanHoiService _phanHoiService;

        public PhanHoiController(ILoiService loiService, IPhanHoiService phanHoiService) : base(loiService)
        {
            this._phanHoiService = phanHoiService;
        }







        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _phanHoiService.GetAll();
                //  var responseData = Mapper.Map<IEnumerable<BaoHanhHangHoa>,IEnumerable<BaoHanhHangHoaViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, PhanHoiViewModel phanHoi)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThongBao = new PhanHoi();
                    newThongBao.UpdatePhanHoi(phanHoi);
                    newThongBao.NgayTao = DateTime.Now;
                    newThongBao.TrangThai =false;
                    string content = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Assets/teample/Contextteample.html"));
                    content = content.Replace("{{user}}", phanHoi.Ten);
                    content = content.Replace("{{Email}}", phanHoi.Email);
                    MailHelper.SendMail(phanHoi.Email, "Thông tin liên hệ", content);
                    _phanHoiService.Add(newThongBao);
                    _phanHoiService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }





    }
}