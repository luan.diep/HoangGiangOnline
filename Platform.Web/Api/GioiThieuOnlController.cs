﻿using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
    

    [RoutePrefix("api/gioithieuonl")]
    [Authorize]
    public class GioiThieuOnlController : ApiControllerBase
    {
        IGioiThieuOnlService _gioiThieuOnlService;

        public GioiThieuOnlController(ILoiService loiService, IGioiThieuOnlService gioiThieuOnlService) : base(loiService)
        {
            this._gioiThieuOnlService = gioiThieuOnlService;
        }







        [Route("getall")]
        [AllowAnonymous]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _gioiThieuOnlService.GetAll();
                //  var responseData = Mapper.Map<IEnumerable<BaoHanhHangHoa>,IEnumerable<BaoHanhHangHoaViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }





    }
}