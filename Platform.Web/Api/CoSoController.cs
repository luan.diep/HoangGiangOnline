﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.Infrastructure.Core;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/coso")]
    [Authorize]
    public class CoSoController : ApiControllerBase
    {
        ICoSoService _coSoService;

        public CoSoController(ILoiService loiService, ICoSoService coSoViecService) : base(loiService)
        {
            this._coSoService = coSoViecService;
        }
        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, CoSoViewModel cosoVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThongBao = new CoSo();
                    newThongBao.UpdateCoSo(cosoVm);

                    _coSoService.Add(newThongBao);
                    _coSoService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }

       
        [Route("layMaCuoiCung")]
        [HttpGet]
        public HttpResponseMessage layMaCuoiCung(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _coSoService.GetAll();
                var macoso="";
                if (listCategory.Count()>0)
                {
                    macoso = listCategory.OrderByDescending(x => x.MaCoSo).First().MaCoSo;

                }
                else
                {
                    macoso = null;
                }
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, macoso);
                return response;
            });
        }



        public HttpResponseMessage Create(HttpRequestMessage request, CoSo coSo)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _coSoService.Add(coSo);
                    _coSoService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, coSo);
                }
                return response;
            });

        }

        [Route("getall")]
        [AllowAnonymous]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _coSoService.GetAll().Where(x=>x.MaCoSo!="000");
                var map = Mapper.Map<IEnumerable<CoSoViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, map);


                return response;
            });
        }
         [Route("getalls")]
        [AllowAnonymous]
        public HttpResponseMessage getalls(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _coSoService.GetAll();
                var map = Mapper.Map<IEnumerable<CoSoViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, map);


                return response;
            });
        }







         [Route("getbyid")]
        [AllowAnonymous]
        public HttpResponseMessage getbyid(HttpRequestMessage request,string macoso)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _coSoService.getbyids(macoso);
               

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }



        
        [Route("pagination")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage Pagination(HttpRequestMessage request, int page, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {

                int totalRow = 0;
                var listCategory = _coSoService.GetAll().Where(x => x.MaCoSo != "000");
                var map = Mapper.Map<IEnumerable<CoSoViewModel>>(listCategory);
                var stt = 1;
                foreach (var item in map)
                {
                    item.STT = stt++;
                }
                totalRow = map.Count();
                var query = map.Skip(page * pageSize).Take(pageSize);
               
                var PaginationSet = new PaginationSet<CoSoViewModel>()
                {
                    Items = query,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
                };
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);


                return response;
            });
        }
        



        [Route("update")]
        [HttpPut]
        public HttpResponseMessage update(HttpRequestMessage request, CoSoViewModel coSoViewModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var vienchucDb = _coSoService.GetByID(coSoViewModel.MaCoSo);

                    vienchucDb.UpdateCoSo(coSoViewModel);
                    _coSoService.Update(vienchucDb);
                    _coSoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }



        //[Route("GetHoanThanhCongViec")]
        //public HttpResponseMessage GetHoanThanhCongViec(HttpRequestMessage request, string msnv)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        var model = _coSoService.GetHoanThanhCongViec(msnv);
        //        var responseData = Mapper.Map<IEnumerable<HoanThanhCongViec>, IEnumerable<HoanThanhCongViec>>(model);
        //        var response = request.CreateResponse(HttpStatusCode.OK, responseData);
        //        return response;
        //    });
        //}

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage getallkhachHang(HttpRequestMessage request, int page, int pageSize = 20, string timkiem=null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;

                var listCoSo = _coSoService.GetAll();
                if (!string.IsNullOrEmpty(timkiem))
                {
                    timkiem = timkiem.ToLower();
                    listCoSo = listCoSo.Where(x => x.MaCoSo.Contains(timkiem)||x.TenCoSo.ToLower().Contains(timkiem)||x.DiaChi.ToLower().Contains(timkiem));
                }
                totalRow = listCoSo.Count();
                var query = listCoSo.OrderBy(x => x.MaCoSo).Skip(page * pageSize).Take(pageSize);
                var responseData = Mapper.Map<IEnumerable<CoSo>, IEnumerable<CoSoViewModel>>(query);
                var PaginationSet = new PaginationSet<CoSoViewModel>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so ban ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
                };

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);

                return response;
            });
        }


        public HttpResponseMessage Put(HttpRequestMessage request, CoSo coSo)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _coSoService.Update(coSo);
                    _coSoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _coSoService.delete(id);
                    _coSoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}