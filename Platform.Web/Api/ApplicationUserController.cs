﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Platform.Common.Exceptions;
using Platform.Model;
using Platform.Model.Models;
using Platform.Service;
using Platform.Web.App_Start;
using Platform.Web.Infrastructure.Core;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/applicationUser")]
    [Authorize]

    public class ApplicationUserController : ApiControllerBase
    {
        private ApplicationUserManager _userManager;
        private IApplicationGroupService _appGroupService;
        private IApplicationUserService _appUserService;
        private IApplicationRoleService _appRoleService;
      
        private ICoSoService _coSoService;
        public ApplicationUserController(
            IApplicationGroupService appGroupService,
            IApplicationRoleService appRoleService,
         
            ICoSoService coSo,
            ApplicationUserManager userManager,
            ILoiService errorService)
            : base(errorService)
        {
            _appRoleService = appRoleService;
            _appGroupService = appGroupService;
            _userManager = userManager;
        
            _coSoService = coSo;
        }
        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                var model = _userManager.Users;
                totalRow = model.Count();
                model = model.OrderByDescending(x => x.UserName).Skip(page * pageSize).Take(pageSize);
                var responseData = Mapper.Map<IEnumerable<ApplicationUser_KH>, IEnumerable<ApplicationUserViewModel>>(model);

                var PaginationSet = new PaginationSet<ApplicationUserViewModel>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
                };

                response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);

                return response;
                //HttpResponseMessage response = null;
                //int totalRow = 0;

                //var model = _appUserService.GetUsers(filter);

                //totalRow = model.Count();
                //var query = model.OrderByDescending(x => x.UserName).Skip(page * pageSize).Take(pageSize);
                //IEnumerable<ApplicationUserViewModel> modelVm = Mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<ApplicationUserViewModel>>(query);

                //PaginationSet<ApplicationUserViewModel> pagedSet = new PaginationSet<ApplicationUserViewModel>()
                //{
                //    Page = page,
                //    TotalCount = totalRow,
                //    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                //    Items = modelVm
                //};

                //response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

               // return response;
            });
        }


       

        [Route("getbyid")]
        public HttpResponseMessage getbyid(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var Id = User.Identity.GetUserId();
                var user = _userManager.FindByIdAsync(Id);
                var app = Mapper.Map<ApplicationUser_KH, ApplicationUserViewModel>(user.Result);
               
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, app);

                return response;

            });
        }





        [Route("detail/{id}")]
        [HttpGet]
       // [Authorize(Roles = "ViewUser")]
        public HttpResponseMessage Details(HttpRequestMessage request, string id)
        {
            if (string.IsNullOrEmpty(id))
            {

                return request.CreateErrorResponse(HttpStatusCode.BadRequest, nameof(id) + " không có giá trị.");
            }
            var user = _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return request.CreateErrorResponse(HttpStatusCode.NoContent, "Không có dữ liệu");
            }
            else
            {
                var applicationUserViewModel = Mapper.Map<ApplicationUser_KH, ApplicationUserViewModel>(user.Result);
                var listGroup = _appGroupService.GetListGroupByUserId(applicationUserViewModel.Id);
                applicationUserViewModel.Groups = Mapper.Map<IEnumerable<ApplicationGroup_KH>, IEnumerable<ApplicationGroupViewModel>>(listGroup);

                return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
            }

        }

        [Route("getbyname")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetUserByUsername(HttpRequestMessage request, string username)
        {
            var user = _userManager.FindByNameAsync(username);
            if (user == null)
            {
                return request.CreateErrorResponse(HttpStatusCode.NoContent, "Không tìm thấy theo yêu cầu.");
            }
            else
            {
                var applicationUserViewModel = Mapper.Map<ApplicationUser_KH, ApplicationUserViewModel>(user.Result);
                var listGroup = _appRoleService.GetListRoleByUserId(applicationUserViewModel.Id);
                applicationUserViewModel.Roles = Mapper.Map<IEnumerable<ApplicationRole_KH>, IEnumerable<ApplicationRoleViewModel>>(listGroup);
              


                return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
            }
        }

        [Route("getRole/{id}")]
        [HttpGet]
       // [Authorize(Roles = "ViewUser")]
        public HttpResponseMessage getRole(HttpRequestMessage request, string id)
        {
            if (string.IsNullOrEmpty(id))
            {

                return request.CreateErrorResponse(HttpStatusCode.BadRequest, nameof(id) + " không có giá trị.");
            }
            var user = _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return request.CreateErrorResponse(HttpStatusCode.NoContent, "Không có dữ liệu");
            }
            else
            {
                var applicationUserViewModel = Mapper.Map<ApplicationUser_KH, ApplicationUserViewModel>(user.Result);
                var listGroup = _appRoleService.GetListRoleByUserId(applicationUserViewModel.Id);
               var role = Mapper.Map<IEnumerable<ApplicationRole_KH>, IEnumerable<ApplicationRoleViewModel>>(listGroup);
         
                

                return request.CreateResponse(HttpStatusCode.OK, role);
            }

        }
        [HttpPost]
        [Route("add")]
        //[Authorize(Roles = "AddUser")]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request, ApplicationUserViewModel applicationUserViewModel)
        {
            if (ModelState.IsValid)
            {
                var newAppUser = new ApplicationUser_KH();
                newAppUser.UpdateUser(applicationUserViewModel);
                try
                {
                    newAppUser.Id = Guid.NewGuid().ToString();
                    var result = await _userManager.CreateAsync(newAppUser, applicationUserViewModel.Password);
                    if (result.Succeeded)
                    {
                        var listAppUserGroup = new List<ApplicationUserGroup_KH>();
                        foreach (var group in applicationUserViewModel.Groups)
                        {
                            listAppUserGroup.Add(new ApplicationUserGroup_KH()
                            {
                                GroupId = group.ID,
                                UserId = newAppUser.Id
                            });
                            //add role to user
                            var listRole = _appRoleService.GetListRoleByGroupId(group.ID);
                            foreach (var role in listRole)
                            {
                                await _userManager.RemoveFromRoleAsync(newAppUser.Id, role.Name);
                                await _userManager.AddToRoleAsync(newAppUser.Id, role.Name);
                            }
                        }
                        _appGroupService.AddUserToGroups(listAppUserGroup, newAppUser.Id);
                        _appGroupService.Save();


                        return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);

                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPut]
        [Route("update")]
        //[Authorize(Roles = "UpdateUser")]
        public async Task<HttpResponseMessage> Update(HttpRequestMessage request, ApplicationUserViewModel applicationUserViewModel)
        {
            if (ModelState.IsValid)
            {
                var appUser = await _userManager.FindByIdAsync(applicationUserViewModel.Id);
                try
                {
                    appUser.UpdateUser(applicationUserViewModel);
                    var result = await _userManager.UpdateAsync(appUser);
                    if (result.Succeeded)
                    {
                        var listAppUserGroup = new List<ApplicationUserGroup_KH>();
                        foreach (var group in applicationUserViewModel.Groups)
                        {
                            listAppUserGroup.Add(new ApplicationUserGroup_KH()
                            {
                                GroupId = group.ID,
                                UserId = applicationUserViewModel.Id
                            });
                            //add role to user
                            var listRole = _appRoleService.GetListRoleByGroupId(group.ID);
                            foreach (var role in listRole)
                            {
                                await _userManager.RemoveFromRoleAsync(appUser.Id, role.Name);
                                await _userManager.AddToRoleAsync(appUser.Id, role.Name);
                            }
                        }
                        _appGroupService.AddUserToGroups(listAppUserGroup, applicationUserViewModel.Id);
                        _appGroupService.Save();
                        return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);

                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpDelete]
        [Route("delete")]
        //[Authorize(Roles = "DeleteUser")]
        public async Task<HttpResponseMessage> Delete(HttpRequestMessage request, string id)
        {
            var appUser = await _userManager.FindByIdAsync(id);
            var result = await _userManager.DeleteAsync(appUser);
            if (result.Succeeded)
                return request.CreateResponse(HttpStatusCode.OK, id);
            else
                return request.CreateErrorResponse(HttpStatusCode.OK, string.Join(",", result.Errors));
        }
       
    }
   
}
