﻿
using AutoMapper;
using Platform.Common;
using Platform.Model;
using Platform.Service;
using Platform.Web.Infrastructure.Core;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
   


    [RoutePrefix("api/tuyendung")]
    public class TuyenDungController : ApiControllerBase
    {
        ITuyenDungService _tuyenDungService;
        public TuyenDungController(ILoiService loiService, ITuyenDungService tuyenDungService) : base(loiService)
        {
            this._tuyenDungService = tuyenDungService;
        }


        [Route("TuyenDungPagination")]
        public HttpResponseMessage Get(HttpRequestMessage request,int page, int pageSize = 10, string timkiem = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var list = _tuyenDungService.TuyenDungPagination();
                if (!string.IsNullOrEmpty(timkiem))
                {
                    timkiem = timkiem.ToLower();
                    list = list.Where(x => Convert.ToString(x.MaTuyenDung).Contains(timkiem) || x.TenCoSo.ToLower().Contains(timkiem) || x.DiaChi.ToLower().Contains(timkiem));
                }
                totalRow = list.Count();
                var query = list.OrderBy(x => x.MaTuyenDung).Skip(page * pageSize).Take(pageSize);
                var responseData = Mapper.Map<IEnumerable<TuyenDungCustom>, IEnumerable<TuyenDungCustom>>(query);

                var PaginationSet = new PaginationSet<TuyenDungCustom>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so ban ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
                };
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);
                return response;
            });
        }
        [Route("getall")]
        public HttpResponseMessage getall(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var list = _tuyenDungService.TuyenDungPagination();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, list);
                return response;
            });
        }


        




        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, TuyenDungViewModel tuyenDungViewModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThongBao = new TuyenDung();
                    newThongBao.UpdateTuyenDung(tuyenDungViewModel);
                    _tuyenDungService.Add(newThongBao);
                    _tuyenDungService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }


         [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, TuyenDungViewModel tuyenDungViewModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var tuyendung =_tuyenDungService.GetByID(tuyenDungViewModel.MaTuyenDung);
                    tuyendung.UpdateTuyenDung(tuyenDungViewModel);
                    _tuyenDungService.Update(tuyendung);
                    _tuyenDungService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }





    }
}