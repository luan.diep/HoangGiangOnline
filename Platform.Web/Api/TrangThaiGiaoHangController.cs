﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;
using Platform.Web.App_Start;
using Microsoft.AspNet.Identity;

namespace Platform.Web.Api
{
    [RoutePrefix("api/chitiettrangthai")]
   // [Authorize]
    public class TrangThaiGiaoHangController : ApiControllerBase
    {
        ITrangThaiGiaoHangService _chiTietTrangThaiService;
        //INhanVienService _nhanVienService;
        IKhachHangService _khachHangService;
        IChungTuDatHangService _chungTuDatHangService;
        private ApplicationUserManager _userManager;
        public TrangThaiGiaoHangController(ILoiService loiService, IChungTuDatHangService chungTuDatHangService, ApplicationUserManager userManager, IKhachHangService khachHangService, ITrangThaiGiaoHangService chiTietTrangThaiService/*, INhanVienService nhanVienService*/) : base(loiService)
        {
            this._chiTietTrangThaiService = chiTietTrangThaiService;
            //this._nhanVienService = nhanVienService;
            this._userManager = userManager;
            this._chungTuDatHangService = chungTuDatHangService;
            this._khachHangService = khachHangService;

        }
      
        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Creates(HttpRequestMessage request, TrangThaiGiaoHangViewModel chiTietTrangThaiViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newquaTrinhDaoTao = new TrangThaiGiaoHang();
                    newquaTrinhDaoTao.UpdateTrangThaiGiaoHang(chiTietTrangThaiViewModels);
                    newquaTrinhDaoTao.NgayGio = DateTime.Now;
                    newquaTrinhDaoTao.MaNhanVienTiepNhan ="0000";
                    newquaTrinhDaoTao.ChoHienThi= true;
                    _chiTietTrangThaiService.Add(newquaTrinhDaoTao);
                    _chiTietTrangThaiService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });
        }

        [Route("getMaCT")]
        public HttpResponseMessage getMaCT(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {

                var KH = _khachHangService.GetAll();
                var MAKH = "";
                foreach (var item in KH)
                {
                    if (item.Email != null)
                    {
                        var email = _userManager.FindByEmail(item.Email);
                        if (email != null)
                        {
                            MAKH = item.MaKhachHang;
                        }
                    }

                }
                var listChungTuDatHangtegory = _chungTuDatHangService.GetAll().Where(x => x.MaTinhTrang == 1 && x.DaThanhToan == false && x.MaKhachHang == MAKH).FirstOrDefault();
                var TrngThaiGiaoHangFliter = _chiTietTrangThaiService.GetAll().Where(x => x.MaChungTuDatHang == listChungTuDatHangtegory.MaChungTuDatHang).FirstOrDefault();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, TrngThaiGiaoHangFliter);
                return response;
            });
        }
        [Route("delete")]
        [HttpDelete]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _chiTietTrangThaiService.delete(id);
                    _chiTietTrangThaiService.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });
        }




        //[Route("laytheomactdh")]
        //[HttpGet]
        //public HttpResponseMessage laytheomactdh(HttpRequestMessage request, string madon)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        var list = _chiTietTrangThaiService.LayTheoMaDon(madon);
        //        var map=Mapper.Map<IEnumerable<TrangThaiGiaoHang>,IEnumerable<TrangThaiGiaoHangViewModel>>(list);
        //        if (map.Any())
        //        {
        //            foreach (var item in map)
        //            {
        //                item.TenNhanVien = _nhanVienService.GetByID(item.MaNhanVienTiepNhan).HoVaTen;
        //            }
        //        }
        //          HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, map);
        //        return response;
        //    });
        //}
    }
}
