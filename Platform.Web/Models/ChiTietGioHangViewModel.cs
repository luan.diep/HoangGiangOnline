﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ChiTietGioHangViewModel
    {
        public long MaChiTietGioHang { get; set; }
        public long MaGioHang { get; set; }
        public string MaHang { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<double> ThanhTien { get; set; }
    }
}