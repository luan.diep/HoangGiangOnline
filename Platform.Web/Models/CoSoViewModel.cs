﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class CoSoViewModel
    {
        public string MaCoSo { get; set; }
        public string DiaChi { get; set; }
        public string TenCoSo { get; set; }
        public string CapToChuc { get; set; }
        public Nullable<double> KinhTuyen { get; set; }
        public Nullable<double> ViTuyen { get; set; }
        public string GhiChu { get; set; }
        public string HinhAnh { get; set; }
        public string DienThoai { get; set; }
        public int STT { get; set; }
    }
}