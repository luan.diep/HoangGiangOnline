﻿using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class KhoNguyenLieuViewModel
    {
        public string MaNguyenLieu { get; set; }
        public int MaDonViTinhNguyenLieu { get; set; }
        public string TenHang { get; set; }
        public int MaNhomNguyenLieu { get; set; }
        public int MaTinhChatNguyenLieu { get; set; }
        public string DonViTinh { get; set; }
        public string BaoHanh { get; set; }
        public string NguonGoc { get; set; }
        public string MoTa { get; set; }
        public Nullable<int> GiaNhap { get; set; }
       
        public Nullable<double> TonKho { get; set; }
      

        public Nullable<double> GiaNhapCuoi { get; set; }

        public Nullable<double> VAT { get; set; }
        public Nullable<double> ChietKhau { get; set; }
        public Nullable<DateTime> HanSuDung { get; set; }
        public string ThanhPham { get; set; }
        public string SoLo { get; set; }
        public string HinhAnh { get; set; }
        public string NhieuHinhAnh { get; set; }
        public string NguoiSua { get; set; }
        public Nullable<System.DateTime> NgaySua { get; set; }
        public Nullable<System.DateTime> NgayNhap { get; set; }
        public string NguoiNhap { get; set; }
        public string MaVach { get; set; }
        public string MaNhaCungCap { get; set; }
        public Nullable<int> MaThuongHieu { get; set; }
        public Nullable<int> DinhMucTonItNhat { get; set; }
        public Nullable<int> DinhMucTonNhieuNhat { get; set; }
        public Nullable<bool> NgungKinhDoanh { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public Nullable<bool> YeuThich { get; set; }
        public string GhiChu { get; set; }
        public Nullable<double> TrongLuong { get; set; }
        public string TenDonViTinhNguyenLieu { get; set; }
        public string ViTri { get; set; }
        public Nullable<double> DonGia { get; set; }
   
        public Nullable<System.DateTime> DuKienHetHang { get; set; }
      
     
        public DonViTinhViewModel DonViTinhs { get; set; }
        public IEnumerable<getdvt> getdvt { get; set; }
    }
}