﻿using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public partial class ChungTuGiaoHangViewModel
    {
        public string MaChungTuGiaoHang { get; set; }
        public string MaChungTuBanHang { get; set; }
        public string MaNguoiGiaoHang { get; set; }
        public string MaSoNhanVien { get; set; }
        public Nullable<int> TrongLuong { get; set; }
        public string DiaChiGui { get; set; }
        public string SdtNguoiNhan { get; set; }
        public Nullable<double> TongTienHang { get; set; }
        public Nullable<System.DateTime> GioGiaoDuKien { get; set; }       
       
    }
}