﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class GioiThieuViewModel
    {

        public int ID { get; set; }
        public string MoTa { get; set; }
        public string HinhAnh { get; set; }
        public string SoDienThoai { get; set; }
    }
}