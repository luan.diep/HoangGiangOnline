﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class LichSuDatHangViewModel
    {
        public long MaLichSuDatHang { get; set; }
        public string Id { get; set; }
        public string MaChungTuDatHang { get; set; }
        public Nullable<bool> DangXuLy { get; set; }
        public Nullable<System.DateTime> NgayGioDangXuLy { get; set; }
        public Nullable<bool> DaNhanDon { get; set; }
        public Nullable<System.DateTime> NgayGioDaNhanDon { get; set; }
        public Nullable<bool> DangGiaoHang { get; set; }
        public Nullable<System.DateTime> NgayGioDangGiaoHang { get; set; }
        public Nullable<bool> DaNhanHang { get; set; }
        public Nullable<System.DateTime> NgayGioDaNhanHang { get; set; }
        public Nullable<bool> DaHuy { get; set; }
        public Nullable<System.DateTime> NgayGioDaHuy { get; set; }
        public string LyDo { get; set; }
    }
}