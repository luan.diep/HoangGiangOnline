﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public  class VietHoaLoi
    {
        public static string TranslateIdentityResult(string massage)
        {

            var list = new List<KeyValuePair<string, string>>() {
            new KeyValuePair<string, string>("An unknown failure has occured", "Đã xảy ra lỗi không xác định"),
            new KeyValuePair<string, string>("Email '{0}' is already taken", "Email '{0}' đã được sử dụng"),
            new KeyValuePair<string, string>("Name {0} is already taken", "Tên {0} đã được sử dụng"),
            new KeyValuePair<string, string>("A user with that external login already exists", "Người dùng có thông tin đăng nhập bên ngoài đó đã tồn tại"),
            new KeyValuePair<string, string>("Email '{0}' is invalid", "Email '{0}' không hợp lệ"),
            new KeyValuePair<string, string>("Invalid token", "Token không hợp lệ"),
            new KeyValuePair<string, string>("User name {0} is invalid, can only contain letters or digits", "Tên người dùng {0} không hợp lệ, chỉ có thể chứa các chữ cái hoặc chữ số."),
            new KeyValuePair<string, string>("Lockout is not enabled for this user", "Đăng xuất không được bật cho người dùng này"),
            new KeyValuePair<string, string>("Incorrect password", "Mật khẩu không đúng"),
            new KeyValuePair<string, string>("Passwords must have at least one digit ('0'-'9')", "Mật khẩu phải có ít nhất một chữ số ('0' - '9')"),
            new KeyValuePair<string, string>("Passwords must have at least one lowercase ('a'-'z')", "Mật khẩu phải có ít nhất một chữ thường ('a' - 'z')"),
            new KeyValuePair<string, string>("Passwords must have at least one non letter or digit character.", "Mật khẩu phải có ít nhất một ký tự không phải chữ cái hoặc chữ số."),
            new KeyValuePair<string, string>("Passwords must have at least one uppercase ('A'-'Z')", "Mật khẩu phải có ít nhất một chữ hoa ('A' - 'Z')"),
            new KeyValuePair<string, string>("Passwords must be at least {0} characters", "Mật khẩu phải có ít nhất {0} ký tự"),
            new KeyValuePair<string, string>("{0} cannot be null or empty", "{0} không được để trống hoặc rỗng"),
            new KeyValuePair<string, string>("Role {0} does not exist.", "Quyền {0} không tồn tại"),
            new KeyValuePair<string, string>("User already has a password set", "Người dùng đã đặt mật khẩu"),
            new KeyValuePair<string, string>("User already in role", "Người dùng đã có quyền"),
            new KeyValuePair<string, string>("UserId not found", "Không tìm thấy UserId"),
            new KeyValuePair<string, string>("User {0} does not exist", "Người dùng {0} không tồn tại."),
            new KeyValuePair<string, string>("User is not in role", "Người dùng không có quyền")
        };

            return list.Find(x => x.Key.Equals(massage)).Value;
        }
    }
}