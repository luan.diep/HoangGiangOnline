﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class VideoViewModel
    {
        public int MaViDeo { get; set; }
        public string DuongDan { get; set; }
        public string GhiChu { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
    }
}