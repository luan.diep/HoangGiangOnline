﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ChiTietPhieuKiemKhoViewModel
    {
        public int MaChiTietKiemKho { get; set; }
        public string MaKiemKho { get; set; }
        public string MaHang { get; set; }
        public Nullable<double> SoLuongThucTe { get; set; }
        public Nullable<double> SoLuongLechTang { get; set; }
        public Nullable<double> SoLuongLechGiam { get; set; }
    }
}