﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Platform.Web.Models
{
    public class NhomTuyenDungViewModel
    {
        public int MaNhomTuyenDung { get; set; }
        public string TenNhomTuyenDung { get; set; }

    }
}