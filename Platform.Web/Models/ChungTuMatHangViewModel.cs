﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ChungTuMatHangViewModel
    {
        public string MaChungTuMatHang { get; set; }
        public string MaSoNhanVien { get; set; }
        public Nullable<int> TongSoHangMat { get; set; }
        public Nullable<int> TongSoMtHang { get; set; }
        public Nullable<double> TongTienMat { get; set; }
        public Nullable<System.DateTime> NgayThang { get; set; }

    }
}