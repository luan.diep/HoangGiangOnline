﻿

(function () {
    angular.module('apphome.tuyendung', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('tuyendung', {
            url: "/tuyendung",
            templateUrl: "/apphome/conponents/tuyendung/tuyenDungView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "tuyenDungController"
        })

    }
})();