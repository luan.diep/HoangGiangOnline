﻿

(function (app) {
    'use strict';

    app.controller('tuyenDungController', tuyenDungController);

    tuyenDungController.$inject = ['$scope','apiService'];
    function tuyenDungController($scope, apiService) {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);

       

        function laytuyendung() {
            apiService.get('api/tuyendung/getall', null, function (result) {
                $scope.DanhSachTuyenDung = result.data;

            }, function () {
                console.log("không lấy được danh sách tuyển dụng")
            })
        }
        laytuyendung()

    }


})(angular.module('apphome.tuyendung'));