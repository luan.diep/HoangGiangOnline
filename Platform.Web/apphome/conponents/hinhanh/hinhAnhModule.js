﻿

(function () {
    angular.module('apphome.hinhanh', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('hinhanh', {
            url: "/hinhanh",
            templateUrl: "/apphome/conponents/hinhanh/hinhAnhView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "hinhAnhController"
        })

    }
})();