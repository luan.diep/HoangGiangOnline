﻿(function (app) {
    'use strict';

    app.controller('hinhAnhController', hinhAnhController);

    hinhAnhController.$inject = ['$scope','apiService'];
    function hinhAnhController($scope, apiService) {

        $scope.AlbumHinhAnh = [];




        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        $scope.showAlbum = false
        function layHinhAnh() {
            apiService.get('api/hinhanh/layhinhanhtheocoso', null, function (result) {
                $scope.DanhSachHinhAnh = result.data;
                $(document).ready(function () {
                    $(function () {
                        var swiper = new Swiper('.swiper-container', {
                            slidesPerView: 1,
                            spaceBetween: 20,
                            loop: true,
                            breakpoints: {
                                480: {
                                    slidesPerView: 2,
                                    spaceBetween: 0,

                                },
                                768: {
                                    slidesPerView: 2,
                                },
                                1024: {
                                    slidesPerView: 3,

                                },
                            },
                            autoplay: {
                                delay: 2000,
                            },
                            navigation: {
                                nextEl: '.swiper-button-next',
                                prevEl: '.swiper-button-prev',
                            },
                        });
                    })
                });
            }, function () {
                console.log("không lấy được danh sách tuyển dụng")
            })
        }


        $scope.chage_image = function (i) {
            
           
           
            angular.forEach(JSON.parse(i.NhieuHinhAnh), function (o) {
                var a = { "HinhAnh": o, "TenCoSo": i.TenCoSo }
                $scope.AlbumHinhAnh.push(a)               
            })
            $scope.showAlbum = true
           
               

            
        }

        $scope.back_img = function () {
            $scope.showAlbum = false
            $scope.AlbumHinhAnh = [];
          
        }


        $scope.clickimg_product = function () {
            $scope.img_product = true;
            var youtubeimgsrc = document.getElementById("img_product").src;
            document.getElementById("myImg").src = youtubeimgsrc;
            $(document).ready(function () {
                $("#popup-all").fancybox({

                }).trigger('click');

            });


        }

        layHinhAnh()



    }


})(angular.module('apphome.hinhanh'));