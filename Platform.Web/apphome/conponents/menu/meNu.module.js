﻿

(function () {
    angular.module('apphome.menu', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('menu', {
            url: "/menu",
            templateUrl: "/apphome/conponents/menu/meNuView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "meNuController"
        })
            
    }
})();