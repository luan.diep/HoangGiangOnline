﻿

(function () {
    angular.module('apphome.gioithieu', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('gioithieu', {
            url: "/gioithieu",
            templateUrl: "/apphome/conponents/gioithieu/gioiThieuView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "gioiThieuController"
        })
            
    }
})();