﻿(function (app) {
    'use strict';

    app.controller('gioiThieuController', gioiThieuController);

    gioiThieuController.$inject = ['$scope', 'apiService', 'notificationService', '$ngBootbox','$stateParams'];
    function gioiThieuController($scope, apiService, notificationService, $ngBootbox, $stateParams) {
       
       
        $scope.gioithieu = gioithieu;
       
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        $scope.CreatePhanHoi = CreatePhanHoi;
        $scope.TatCaGioiThieu = {};
        $scope.PhanHoiModel = {};
        $scope.status = true;
       
        $(document).ready(function () {
            // Initialize select2
            $("#constact_tinh").select2({
                placeholder: 'Tìm Kiếm...',
            });
            $('b[role="presentation"]').hide();


        });

        $scope.clickimg_product = function () {
            $scope.img_product = true;
            var youtubeimgsrc = document.getElementById("img_product");
          
           
            document.getElementById("myImg").src = youtubeimgsrc.src;
            $(document).ready(function () {
                //$("#popup-all").fancybox({

                //}).trigger('click');
                $("#popup-all").fancybox({
                   
                    'width': 800,
                    'transitionIn': 'fade',
                    'transitionOut': 'elastic',
                    'speedIn': 600,
                    'speedOut': 400,
                    'autoSize': true,

                }).trigger('click');

              
            });
        }
     
        function gioithieu() {
            apiService.get('api/gioithieuonl/getall', null, function (result) {
                if (result.data.length != 0) {
                    $scope.TatCaGioiThieu = result.data[0];}
               
            }, function () {
                console.log('Khong lay duoc gioi thieu');
            })
        }
        /*-----------------------------------start  thêm mới phản hồi ----------------------------------------------*/
        function CreatePhanHoi() {
            apiService.post('api/phanhoi/create', $scope.PhanHoiModel, function (result) {
                notificationService.displaySuccess("Thêm thành công");  
                $scope.PhanHoiModel = {};
                $scope.status = false;
                $scope.status_send = true;
            }, function () {
                console.log('têm mới thất bại');
            })




        }




        $scope.gettinhthanh = function () {

            apiService.get('api/danhsachtinh_tp/getall', null, function (result) {
                $scope.selectTinh = result.data;
            }, function () {
                console.log('khong lay dc vi tri ');
            })



        }
        $scope.getquanhuyen = function (i) {
            var config = {
                params: {
                    matinh: i

                }
            }
            apiService.get('api/danhsachquan_huyen/ChonTheoTinh', config, function (result) {
                $scope.selectQuan = result.data;
            }, function () {
                console.log('khong lay dc vi tri ');
            })


        }

        $scope.gettinhthanh()
        /*-----------------------------------end  thêm mới phản hồi ----------------------------------------------*/
    /************slider***********/
        $scope.width = document.body.clientWidth;
        $scope.$watch('width', v => {
          

        }, true)
        

        gioithieu();
       

       
        
        
    }
   
    
})(angular.module('apphome.gioithieu'));