﻿(function (app) {
    'use strict';

    app.controller('viDeoController', viDeoController);

    viDeoController.$inject = ['$scope','apiService'];
    function viDeoController($scope, apiService) {

        $scope.linkvideo = "../../../Assets/apphome/images/y2mate.com%20-%20%c4%90en%20x%20JustaTee%20-%20%c4%90i%20V%e1%bb%81%20Nh%c3%a0%20(MV)_v240P.mp4";
        $('img').click(function () {
            $(this).hide();
            // use the parameters to play the video now..
        })
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        function LayVideo() {
            apiService.get('api/video/getall', null, function (result) { 
                $scope.allvideo = result.data
                $(document).ready(function () {

                    var swiper = new Swiper('.swiper-container.video', {
                        slidesPerView: 1,
                        spaceBetween: 20,
                        loop:true,
                        breakpoints: {
                            480: {
                                slidesPerView: 2,
                                spaceBetween: 0,

                            },
                            768: {
                                slidesPerView: 2,
                            },
                            1024: {
                                slidesPerView: 3,

                            },
                        },
                        autoplay: {
                            delay: 1000,
                        },
                        navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        },
                    });
                })

            }, function () { })
        }
        LayVideo();

    }


})(angular.module('apphome.video'));