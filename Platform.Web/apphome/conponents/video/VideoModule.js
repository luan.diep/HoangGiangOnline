﻿

(function () {
    angular.module('apphome.video', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('video', {
            url: "/video",
            templateUrl: "/apphome/conponents/video/viDeoView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "viDeoController"
        })

    }
})();