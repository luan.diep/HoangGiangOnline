﻿


(function (app) {
    'use strict';

    app.controller('chiTietHangHoaController', chiTietHangHoaController);

    chiTietHangHoaController.$inject = ['$scope', 'apiService', '$stateParams', 'notificationService', '$state', '$rootScope', '$location','Slider'];
    function chiTietHangHoaController($scope, apiService, $stateParams, notificationService, $state, $rootScope, $location, Slider) {
        $scope.active = true;
        $scope.layChiTietHangHoa = layChiTietHangHoa;
        $scope.layThanhPham = layThanhPham;
        $("#loading").css("display", "block")
        $scope.troLai = troLai;
        $scope.SliderProduct=[]
        $scope.ThanhPham = [];
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        Slider.autorun(
            {
                time: '6000',
                auto: 'false'
            }
        );
        Slider.Slider()
        
        $scope.clickimg_product = function (img) {
            $scope.img_product = true;
           
            document.getElementById("myImg").src = 'http://hoanggiang.mekong-tech.com'+img;
            $(document).ready(function () {
                //$("#popup-all").fancybox({

                //}).trigger('click');
                $("#popup-all").fancybox({
                    onClosed: function () {
                        $('#sub_cont').hide(250, function () {
                            $('#IDsearchform input').val('');
                        })
                    },
                    'width': 800,
                    'transitionIn': 'fade',
                    'transitionOut': 'elastic',
                    'speedIn': 600,
                    'speedOut': 400, 
                    'autoSize': true,
                   
                }).trigger('click');
           
            
            });
        }


        $(document).ready(function () {
            $("#toggle").click(function () {
                var elem = $("#toggle").text();
                if (elem == "Xem thêm") {
                    //Stuff to do when btn is in the read more state
                    $("#toggle").text("Thu nhỏ");
                    $("#toggle").css({ 'style': 'cursor:pointer' });
                    $("#text").slideDown();
                } else {
                    //Stuff to do when btn is in the read less state
                    $("#toggle").text("Xem thêm");
                    $("#toggle").css({ 'style': 'cursor:pointer' });
                    $("#text").slideUp();
                }
            });
        });

        //$(document).ready(function () {

        //    $(".fancybox").fancybox({
        //        openEffect: "none",
        //        closeEffect: "none",
                
        //    });
        //});



      

        $scope.changeimage = function (i) {
            $scope.ChiTietHangHoa.HinhAnh=i
        }

        function layChiTietHangHoa(item) {
            var mahang;
            if ($stateParams.id == '999999999999') {
                mahang = '000000000016'
            } else {
                mahang = $stateParams.id;
            }
            var config = {
                params: {
                    mahang: mahang,
                }
            }
           
            apiService.get('api/hanghoa/chitiethanghoadathang', config, function (result) {
                $scope.ChiTietHangHoa = result.data;
                if ($stateParams.id == '999999999999') {
                  
                    $scope.ChiTietHangHoa.MaHang = '999999999999'
                    $scope.ChiTietHangHoa.HinhAnh = '/Assets/giaovien/img/files/vituqaynuacon.png'
                    $scope.ChiTietHangHoa.TenHang = 'Vịt quay nửa con';
                }
             

                $scope.listimage = [];
                var image = [];
                $scope.listimage = JSON.parse($scope.ChiTietHangHoa.NhieuHinhAnh);

                angular.forEach($scope.listimage, function (i) {
                    var json = { HinhAnh: i }
                    image.push(json)

                })
                
                $scope.ChiTietHangHoa.NhieuHinhAnhs = image;
                if ($scope.stateParams == '999999999999') {
                    $scope.ChiTietHangHoa.SoLuong = 0.5;
                } else {
                    $scope.ChiTietHangHoa.SoLuong = 1;
                }
               
                $scope.Mota = $scope.ChiTietHangHoa.MoTa;
                $('#mota').html($scope.Mota)
                $('#mota2').html($scope.Mota)
                layThanhPham()
               

                $("html, body").animate({
                    scrollTop: 0
                }, 600);
              
               

            }, function () {
                console.log('Khong lay duoc chi tiet hang hoa');
            })
        }

        $scope.layChiTietHangHoas = function (item) {
            $state.go('dathang.chitiethanghoa', {
                id: item,
               
            });
           
        }
        
        function layThanhPham() {
          
            var config = {
                params: {
                    mahang: $stateParams.id
                }
            }
            apiService.get('api/thanhpham/getbymahang', config, function (result) {
                $scope.ThanhPham = result.data;
                var cao = $('.col-9_more').height();
                var w = $('.col-9_more').width();
                document.getElementById('list_prodct').setAttribute("style", "height:" + cao + 'px' + " ");
                $("#loading").css("display", "none")
            }, function () {
                console.log('khong lay dc thanh pham')
            })
        }

        $scope.getListproduct = () => {
            apiService.get("api/hanghoa/slide_product",null, function (results) {
                $scope.SliderProduct = results.data;
              

                $(document).ready(function () {
                    $(".navss").slick({

                        slidestoshow: 3,
                        slidesToScroll: 1,
                        draggable: true,
                        infinite: true,
                        prevArrow: false,
                        nextArrow: false,
                        swipeToSlide: true,
                        vertical: true,
                        verticalSwiping: true,
                        centerMode: false,
                        autoplay: true,
                        autoplaySpeed: 2000,
                    });



                });

            }, function () { })
        }
        $scope.getListproduct()
      

       
      
        function troLai() {
            if ($rootScope.previousState) {
                $state.go($rootScope.previousState);
            }
            else {
                $state.go('dathang')
            }
           
        }
        
      

       
       
        /*tabs */
            $(document).ready(function () {
            $("#content").find("[id^='tab']").hide(); // Hide all content
            $("#tabs li:first").attr("id", "current"); // Activate the first tab
            $("#content #tab1").fadeIn(); // Show first tab's content

            $('#tabs a').click(function (e) {
                e.preventDefault();
                if ($(this).closest("li").attr("id") == "current") { //detection for current tab
                    return;
                }
                else {
                    $("#content").find("[id^='tab']").hide(); // Hide all content
                    $("#tabs li").attr("id", ""); //Reset id's
                    $(this).parent().attr("id", "current"); // Activate this
                    $('#' + $(this).attr('name')).fadeIn(); // Show content for the current tab
                }
            });
            });



       


       


            





















        layChiTietHangHoa();
        $scope.hide()
      
       
    }
   
    
})(angular.module('apphome.dathang'));

//app.directive('fancybox', function ($templateRequest, $compile) {
//    return {
//        scope: true,
//        restrict: 'A',
//        controller: function ($scope) {
//            $scope.openFancybox = function (url) {
//                $templateRequest(url).then(function (html) {
//                    var template = $compile(html)($scope);
//                    $.fancybox.open({ content: template, type: 'html' });
//                });
//            };
//        },
//        link: function link(scope, elem, attrs) {
//            elem.bind('click', function () {
//                var url = attrs.fancyboxTemplate;
//                scope.openFancybox(url);
//            });
//        },
//    }
//});