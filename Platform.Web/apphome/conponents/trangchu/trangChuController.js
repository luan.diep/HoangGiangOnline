﻿
(function (app) {
    app.controller('trangChuController', trangChuController);
    trangChuController.$inject = ['$scope', 'loginService', '$injector', 'notificationService', 'localStorageService', 'apiService','$timeout'];
    function trangChuController($scope, loginService, $injector, notificationService, localStorageService, apiService, $timeout) {
        $scope.image1 = true
        $scope.image2 = true
        $scope.image3 = true
        //$scope.themGioHang = themGioHang;
      
        $scope.openvideo1 = function () {
            $scope.video1 = true;

            $scope.image1 = false;

        }
        $scope.openvideo2 = function () {

            $scope.video2 = true;

            $scope.image2 = false;
        }
        $scope.openvideo3 = function () {

            $scope.video3 = true;

            $scope.image3 = false;
        }


       
        var slidesInSlideshow = 2;
        var slidesTimeIntervalInMs = 4000;

        $scope.slideshow = 1;
        var slideTimer =
            $timeout(function interval() {
                $scope.slideshow = ($scope.slideshow % slidesInSlideshow) + 1;
                slideTimer = $timeout(interval, slidesTimeIntervalInMs);
            }, slidesTimeIntervalInMs);

       
      
        $(document).ready(function () {
            $("#toggle").click(function () {
                var elem = $("#toggle").text();
                if (elem == "XEM THÊM") {
                    //Stuff to do when btn is in the read more state
                    $("#toggle").text("Thu nhỏ");
                    $("#toggle").css({ 'style': 'cursor:pointer' });
                    $("#text").slideDown();
                } else {
                    //Stuff to do when btn is in the read less state
                    $("#toggle").text("XEM THÊM");
                    $("#toggle").css({ 'style': 'cursor:pointer' });
                    $("#text").slideUp();
                }
            });
        });



       
        //if add to cart btn clicked
        $(document).on("click", ".cart_icon", function () {
      
            if ($(window).width() >= 768) {
                let cart = $('.cart-nav');
                // find the img of that card which button is clicked by user
                let imgtodrag = $(this).parent('.left_product').parent('.name_product').parent('.field-imgs').find(".img-product").eq(0);
                if (imgtodrag) {
                    // duplicate the img
                    var imgclone = imgtodrag.clone().offset({
                        top: imgtodrag.offset().top,
                        left: imgtodrag.offset().left
                    }).css({
                        'opacity': '0.8',
                        'position': 'absolute',
                        'height': '250px',
                        'width': '250px',
                        'z-index': '100',
                        'display': 'block'
                    }).appendTo($('body')).animate({
                        'top': cart.offset().top + 20,
                        'left': cart.offset().left + 30,
                        'width': 30,
                        'height': 30,
                        'display': 'block'
                    }, 1500, 'easeInOutExpo');



                    imgclone.animate({
                        'width': 0,
                        'height': 0
                    }, function () {
                        $(this).detach();

                    });
                }
            }
        });

       
        //if ($(document).width() <= 480) {
        //    setInterval(function () {
        //        var count = $(".container.testimonial-group .flex-nowrap").find(".col-sm-4").length - 1;
        //        var index = $(".container.testimonial-group .flex-nowrap").find(".active").index() + 1
        //        var indexwidth = ($(".container.testimonial-group .flex-nowrap .col-sm-4").eq(index).width() * (index)) + (60 * index)

        //        if (count == ($(".container.testimonial-group .flex-nowrap").find(".active").index())) {
        //            $(".container.testimonial-group .flex-nowrap .col-sm-4").css({ "transform": "translate3d(-" + 0 + "px, 0px, 0px)", 'transition': '0s' })
        //            $(".container.testimonial-group .flex-nowrap").find(".active").removeClass("active")
        //            $(".container.testimonial-group .flex-nowrap .col-sm-4").eq(0).addClass("active")
        //        } else {
        //            $(".container.testimonial-group .flex-nowrap").find(".active").removeClass("active")
        //            $(".container.testimonial-group .flex-nowrap .col-sm-4").eq(index).addClass("active")
        //            $(".container.testimonial-group .flex-nowrap .col-sm-4").css({ "transform": "translate3d(-" + indexwidth + "px, 0px, 0px)", 'transition': '0.5s' })



        //        }

        //    }, 1000000000)
           
           
        //}



      





    }
})(angular.module('apphome.trangchu'));
