﻿





(function (app) {
    app.controller('rootController', rootController);

    rootController.$inject = ['$state', 'authData', 'loginService', '$scope', 'authenticationService', 'apiService', '$q', '$timeout', '$interval', '$filter', 'notificationService', '$window', '$ngBootbox', 'localStorageService', '$state', 'vcRecaptchaService'];

    function rootController($state, authData, loginService, $scope, authenticationService, apiService, $q, $timeout, $interval, $filter, notificationService, $window, $ngBootbox, localStorageService, $state, vcRecaptchaService) {
        $scope.DaDangNhap = false;
        $scope.GioHangTest = [];
        $scope.AllNhomHang = [];
        $scope.PhieuCTDH = {};
        $scope.Buttom = false;
        $scope.Thongtin = {};
        $scope.layGioHangTest = layGioHangtest;
        $scope.tongSoMatHang = 0;
        $scope.checkCoTK = true;
        $scope.MuaHangKCTK = true;
        $scope.TongTienTruocKM = 0;
        $scope.alltinhthanh = []
        $scope.DangKy = {};
        $scope.ChiTietHangHoa = {};
        $scope.DangNhap = {
            UserName: "",
            PassWord: "",
        }
        $scope.chitiet = false;
        $scope.hide = function () {
            $scope.chitiet = true;

        }
        $scope.GioHanglengt = 0;
        ///Lấy cơ sở nổi bật
        $scope.TatCaCoSoNoibat = [];
        $scope.tatcacosonoibat = function () {
            apiService.get('api/coso/getall', null, function (result) { $scope.TatCaCoSoNoibat = result.data }, function () { console.log("lấy ko được hàng hóa nổi bật") });
        }
        $scope.tatcacosonoibat();
        //end Lấy cơ sở nổi bật
       
    /***select quận huyên tỉnh */
        $(document).ready(function () {
            // Initialize select2
            $("#tinh").select2({
                placeholder: 'Tìm Kiếm...',
            });
            $("#quan").select2({
                placeholder: 'Tìm Kiếm...',
            });
            $("#tinhdk").select2({
                placeholder: 'Tìm Kiếm...',
            });
            $("#quandk").select2({
                placeholder: 'Tìm Kiếm...',
            });
            $('b[role="presentation"]').hide();


        });

        /***open đăng nhập đăng ký */
        $scope.openlogin = function (i) {
            $('#LoginModal').modal('show');
            if (i == "DN") {

            }
            else {

            }
        }

        $scope.Topfootder = function () {
            $state.go("huongdanthanhtoan")
            $("html, body").animate({
                scrollTop: 0
            }, 1200);
        }

        $("#messlogin").popover({
            trigger: "manual",
            html: true,

            content: function () {
                var elementId = $(this).attr("data-popover-content");
                return $(elementId).html();
            },
            container: 'body',
            placement: 'auto',


        }).on("mouseenter", function () {
            var _this = this;
            $(this).popover("show");
            $(this).siblings("#messlogin").on("mouseleave", function () {
                $(_this).popover('hide');
            });
        }).on("mouseleave", function () {
            var _this = this;
            setTimeout(function () {
                if (!$("#messlogin:hover").length) {
                    $(_this).popover("hide")
                }
            }, 100);
        });



       
        const burgerMenu = document.getElementById("burger");
        const navbarMenu = document.getElementById("menu");
        $scope.hidemenu = function () {
            if ($(document).width() <= 480) {
                if ($("#navbars ").find(".active").length > 0) {
                    document.body.style.overflow = "visible";

                } else {
                    document.body.style.overflow = "hidden";
                }
            }
           

            $('#menu').removeClass('active');
            $('#burger').removeClass('active');
        }

        // Responsive Navbar Toggle
        burgerMenu.addEventListener("click", function () {
            if ($(document).width() <= 480) {
                if ($("#navbars ").find(".active").length > 0) {
                    document.body.style.overflow = "visible";

                } else {
                    document.body.style.overflow = "hidden";
                }
            }
            
            navbarMenu.classList.toggle("active");
            burgerMenu.classList.toggle("active");
          
        });
        $(window).resize(function () {
            var width = $(window).width();
            if (width > 768) {
                //navbarMenu.classList.toggle("active");
                //burgerMenu.classList.toggle("active");

                //if ($("#navbars ").find(".active").length > 0) {
                //    document.body.style.overflow = "visible";

                //} else {
                //    document.body.style.overflow = "hidden";
                //}

                $('#menu').removeClass('active');
                $('#burger').removeClass('active');
            }
        });





        //Quản lý giỏ hàng
        $scope.themGioHang_TT = function (mahang, hinhanh, tenhang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap, SoLuongCT) {
            $scope.Buttom = true
            if ($scope.DaDangNhap == false) {
                suaGioHangKTK(mahang, hinhanh, tenhang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap, SoLuongCT);
            }
            else {
                if ($scope.GioHangTest.length == 0) {
                  
                    //lấy mã chứng từ mới nhất
                    apiService.get('api/chungtudathang/laymamoinhat', null, function (result) {
                        $scope.MaGioHangMoi = result.data;

                        taoMoiGioHang(result.data, mahang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap, SoLuongCT);
                    }, function () {
                        console.log('Không lấy đươc mã chứng từ')
                    })

                }
                else {
                    suaGioHang(mahang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap, SoLuongCT)

                }
            }
            $state.go("xemgiohang");
        }
        $scope.themGioHang = function (mahang, hinhanh, tenhang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap,SoLuongCT) {
            $scope.Buttom = true
            if ($scope.DaDangNhap == false) {
                suaGioHangKTK(mahang, hinhanh, tenhang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap, SoLuongCT);
            }
            else if ($scope.DaDangNhap == true) {
                apiService.get('api/chitietchungtudathang/getbyemail', null, function (result) {
                    if (result.data.length == 0) {
                        //lấy mã chứng từ mới nhất
                        apiService.get('api/chungtudathang/laymamoinhat', { ignoreLoadingBar: true }, function (result1) {
                            $scope.MaGioHangMoi = result1.data;
                            taoMoiGioHang(result1.data, mahang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap, SoLuongCT);
                        }, function () {
                            console.log('Không lấy đươc mã chứng từ')
                        })

                    }
                    else {
                        suaGioHang(mahang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap, SoLuongCT)

                    }
                }, function () { })
               






              
            }

        }
        function taoMoiGioHang(MaCTDTMoi, mahang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap, SoLuongCT) {
            var vitnuacon = 0;
            if (mahang == '999999999999') {
                mahang = '000000000016'
                vitnuacon = 1;
               

            }
           
            var PhieuCTDHMoi =
            {
                MaChungTuDatHang: MaCTDTMoi,
               
                DaThayDoi: false,
                DaThanhToan: false,
                MaKenhBanHang: '04',

                MaTinhTrang: 1,
                IsViewed: false,
                Email: $scope.authentication.userName,
               
            }
            var chitiet = {
                MaChungTuDatHang: MaCTDTMoi,
                MaHang: mahang,
                DonGia: giaban,
                MaDonViTinh: madvt,
            }
            if (SoLuongCT != null || SoLuongCT != undefined) {
                chitiet.SoLuong = SoLuongCT
                chitiet.ThanhTien = giaban * SoLuongCT
                chitiet.GiaKhuyenMai = giakm * SoLuongCT
                chitiet.TienThueGTGT = ((GiaNhap * VAT) / 100) * SoLuongCT
                chitiet.TienChietKhau = ((GiaNhap * ChietKhau) / 100) * SoLuongCT
                PhieuCTDHMoi.TongTienHang = giaban * SoLuongCT
                PhieuCTDHMoi.TongTienThanhToan = (giaban - giakm) * SoLuongCT
                PhieuCTDHMoi.TienThueGTGT = ((GiaNhap * ChietKhau) / 100) * SoLuongCT
                PhieuCTDHMoi.TienChietKhau = ((GiaNhap * ChietKhau) / 100) * SoLuongCT


            } else {
                if (vitnuacon == 1) {
                    chitiet.SoLuong = 0.5
                } else {
                    chitiet.SoLuong = 1
                }
                
                chitiet.ThanhTien = giaban * chitiet.SoLuong
                chitiet.GiaKhuyenMai = giakm * chitiet.SoLuong
                chitiet.TienThueGTGT = ((GiaNhap * VAT) / 100)*chitiet.SoLuong,
                 chitiet.TienChietKhau = ((GiaNhap * ChietKhau) / 100) * chitiet.SoLuong
                PhieuCTDHMoi.TongTienHang = giaban* chitiet.SoLuong,
                PhieuCTDHMoi.TongTienThanhToan = (giaban - giakm )* chitiet.SoLuong,
                PhieuCTDHMoi.TienThueGTGT = ((GiaNhap * VAT) / 100)*chitiet.SoLuong,
                PhieuCTDHMoi.TienChietKhau = ((GiaNhap * ChietKhau) / 100) * chitiet.SoLuong

            }
           

            if (itemKM != null) {
                chitiet.KhuyenMai2 = itemKM.MaHang
            }
            if (itemKM != null) {
                var chitietkm = {
                    MaChungTuDatHang: MaCTDTMoi,
                    MaHang: itemKM.MaHang,
                    //SoLuong: itemKM.SoLuongKhuyenMai2,
                    DonGia: 0,
                    KhuyenMai: mahang,
                    ThanhTien: 0,
                    MaDonViTinh: itemKM.MaDonViTinh,
                    GiaKhuyenMai: 0,
                    KhuyenMai2: null,
                    TienThueGTGT: 0,
                    TienChietKhau: 0

                }
                if (SoLuongCT != null || SoLuongCT != undefined) { chitietkm.SoLuong = itemKM.SoLuongKhuyenMai2 * SoLuongCT } else { chitietkm.SoLuong = itemKM.SoLuongKhuyenMai2; }

                //$scope.GioHangTest.push(chitiet)
            }
            var chitiettrangthaiModel = {
                MaChungTuDatHang: MaCTDTMoi,
                MaTrangThai: 1,
            }
            PhieuCTDHMoi.TongTienKhuyenMai = chitiet.GiaKhuyenMai
            apiService.post('api/chungtudathang/create', PhieuCTDHMoi, success1, falses1, { ignoreLoadingBar:true})
            function success1() {

                apiService.post('api/chitietchungtudathang/create', chitiet, success2, falses2, {ignoreLoadingBar:true})
                function success2() {
                    console.log("chi tiet crteate")
                    if (chitietkm) {
                        apiService.post('api/chitietchungtudathang/create', chitietkm, () => { }, () => { }, { ignoreLoadingBar: true })

                    }

                    apiService.post('api/chitiettrangthai/create', chitiettrangthaiModel, success3, falses3, {ignoreLoadingBar:true})
                    function success3() {
                        $scope.layGioHangTest();
                        /*notificationService.displaySuccess("thêm vào giỏ hàng thành công")*/
                        //if ($scope.GioHangTest.length > 0) {
                        //    $scope.TongTienTruocKM = $scope.PhieuCTDH.TongTienHang + $scope.GioHangTest.map(o => o.GiaKhuyenmai).reduce((a, c) => { return a + c });
                        //}
                        $scope.Buttom = false;

                    }
                    function falses3() {
                        console.log("Thêm mới trạng thái chứng từ không thành công")
                    }

                }
                function falses2() {
                    console.log("Thêm mới chi tiết chứng từ không thành công")
                }

            }
            function falses1() {
                console.log("Thêm mới chứng từ không thành công")

            }



        }
        function suaGioHang(mahang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap, SoLuongCT) {
            $scope.Buttom = true
            var trangthai;
            var luutam = {};
            var vitnuacon = 0;
            if (mahang == '999999999999') {
                mahang = '000000000016'
                vitnuacon = 1;
               

            }
            angular.forEach($scope.GioHangTest, function (i) {
                if (i.MaHang == mahang && i.KhuyenMai == undefined) {
                    trangthai = 0;
                    luutam = i;
                }


            })

            if (trangthai == 0) {
                //kiểm tra trùng 
                if (SoLuongCT != null || SoLuongCT != undefined) {
                    luutam.SoLuong += SoLuongCT;
                }
                else if (vitnuacon == 1) {
                    luutam.SoLuong += 0.5;
                }
                else {

                    luutam.SoLuong += 1;
                }
                
                luutam.ThanhTien = (giaban) * luutam.SoLuong
                $scope.PhieuCTDH.TongTienKhuyenMai -= luutam.GiaKhuyenMai
                luutam.GiaKhuyenMai = giakm * luutam.SoLuong
                luutam.TienThueGTGT = ((GiaNhap * VAT) / 100) * luutam.SoLuong
                luutam.TienChietKhau = ((GiaNhap * ChietKhau)) * luutam.SoLuong
                $scope.PhieuCTDH.TongTienHang = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                $scope.PhieuCTDH.TongTienThanhToan = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c }) - $scope.GioHangTest.map(o => o.GiaKhuyenMai).reduce((a, c) => { return a + c });
                $scope.PhieuCTDH.TienThueGTGT = $scope.GioHangTest.map(o => o.TienThueGTGT).reduce((a, c) => { return a + c });
                $scope.PhieuCTDH.TienChietKhau = $scope.GioHangTest.map(o => o.TienChietKhau).reduce((a, c) => { return a + c });
                $scope.PhieuCTDH.TongTienKhuyenMai += luutam.GiaKhuyenMai

                apiService.put('api/chungtudathang/update', $scope.PhieuCTDH, success2, falses2, {ignoreLoadingBar:true})
                function success2() {
                    apiService.put('api/chitietchungtudathang/update', luutam, success3, falses3, {ignoreLoadingBar:true})
                    function success3() {
                        if (itemKM != null) {
                            angular.forEach($scope.GioHangTest, function (item, index) {
                                if (item.MaHang == itemKM.MaHang && item.KhuyenMai == mahang) {//Khuyenmai là mả hàng gốc
                                    //item.SoLuong = itemKM.SoLuongKhuyenMai2 * luutam.SoLuong
                                    if (SoLuongCT != null || SoLuongCT != undefined) {
                                        item.SoLuong += itemKM.SoLuongKhuyenMai2 * SoLuongCT
                                    } else {
                                        item.SoLuong += itemKM.SoLuongKhuyenMai2;
                                    }

                                    apiService.put('api/chitietchungtudathang/update', item, () => {
                                        $scope.layGioHangTest();

                                    }, () => {
                                            $scope.layGioHangTest();

                                    }, {ignoreLoadingBar:true})
                                }
                            })
                        }
                        $scope.Buttom = false
                        trangthai = "";
                        $scope.layGioHangTest();
                        /*notificationService.displaySuccess("thêm vào giỏ hàng thành công")*/
                        //$scope.TongTienTruocKM = $scope.PhieuCTDH.TongTienHang + $scope.GioHangTest.map(o => o.KhuyenMai).reduce((a, c) => { return a + c });
                    }
                    function falses3() {
                        console.log("sửa ChiTiet DH ko thành công")
                    }

                }
                function falses2() {
                    console.log("sửa CTDH ko thành công")
                }
                //end kiểm tra trùng
            }
            else {
                //kiểm tra không trùng 
                var chitiet = {
                    MaChungTuDatHang: $scope.PhieuCTDH.MaChungTuDatHang,
                    MaHang: mahang,
                   
                    DonGia: giaban,
                    //KhuyenMai: giakm,
                   // GiaKhuyenmai: giakm,
                    //ThanhTien: giaban - giakm,
                    MaDonViTinh: madvt,
                    //TienThueGTGT: ((GiaNhap * VAT) / 100),
                    //TienChietKhau: ((GiaNhap * ChietKhau) / 100)
                }
                if (itemKM != null) {
                    chitiet.KhuyenMai2 = itemKM.MaHang
                }

                if (SoLuongCT != null || SoLuongCT != undefined) {
                    chitiet.SoLuong = SoLuongCT
                    chitiet.ThanhTien = (giaban - giakm) * SoLuongCT
                    chitiet.GiaKhuyenMai = giakm * SoLuongCT
                    chitiet.TienThueGTGT = ((GiaNhap * VAT) / 100) * SoLuongCT
                    chitiet.TienChietKhau = ((GiaNhap * ChietKhau) / 100) * SoLuongCT
                   


                } else {
                    if (vitnuacon == 1) {

                        chitiet.SoLuong = 0.5
                    } else {
                        chitiet.SoLuong = 1

                    }
                   
                    chitiet.ThanhTien = (giaban - giakm) * chitiet.SoLuong
                    chitiet.GiaKhuyenMai = giakm * chitiet.SoLuong
                    chitiet.TienThueGTGT = ((GiaNhap * VAT) / 100) * chitiet.SoLuong,
                    chitiet.TienChietKhau = ((GiaNhap * ChietKhau) / 100) * chitiet.SoLuong
                   

                }
                //$scope.GioHangTest.push(chitiet)
                if (itemKM != null) {
                    var chitiet1 = {
                        MaChungTuDatHang: $scope.PhieuCTDH.MaChungTuDatHang,
                        MaHang: itemKM.MaHang,
                        KhuyenMai: mahang,////là mả hàng gốc của khuyến mãi
                        //SoLuong: itemKM.SoLuongKhuyenMai2,
                        DonGia: 0,
                        //KhuyenMai: 0,
                        ThanhTien: 0,
                        MaDonViTinh: itemKM.MaDonViTinh,
                        GiaKhuyenMai: 0,
                        KhuyenMai2: null,
                        TienThueGTGT: 0,
                        TienChietKhau: 0
                    }
                    if (SoLuongCT != null || SoLuongCT != undefined) { chitiet1.SoLuong = itemKM.SoLuongKhuyenMai2 * SoLuongCT } else { chitiet1.SoLuong = itemKM.SoLuongKhuyenMai2; }
                    apiService.post('api/chitietchungtudathang/create', chitiet1, () => { }, () => { }, {ignoreLoadingBar:true})
                }
                apiService.post('api/chitietchungtudathang/create', chitiet, success1, falses1, {ignoreLoadingBar:true})
                function success1() {
                    if (SoLuongCT != null || SoLuongCT != undefined) {
                        $scope.PhieuCTDH.TongTienHang = $scope.PhieuCTDH.TongTienHang + ((giaban) * SoLuongCT);
                        $scope.PhieuCTDH.TongTienThanhToan = $scope.PhieuCTDH.TongTienThanhToan + ((giaban - giakm) * SoLuongCT);
                        $scope.PhieuCTDH.TienThueGTGT = $scope.PhieuCTDH.TienThueGTGT + (((GiaNhap * VAT) / 100) * SoLuongCT);
                        $scope.PhieuCTDH.TienChietKhau = $scope.PhieuCTDH.TienChietKhau + (((GiaNhap * VAT) / 100) * SoLuongCT);
                    }
                    else {
                        $scope.PhieuCTDH.TongTienHang = $scope.PhieuCTDH.TongTienHang + (giaban);
                        $scope.PhieuCTDH.TongTienThanhToan = $scope.PhieuCTDH.TongTienThanhToan + (giaban - giakm);
                        $scope.PhieuCTDH.TienThueGTGT = $scope.PhieuCTDH.TienThueGTGT + ((GiaNhap * VAT) / 100);
                        $scope.PhieuCTDH.TienChietKhau = $scope.PhieuCTDH.TienChietKhau + ((GiaNhap * ChietKhau) / 100);
                    }
                    $scope.PhieuCTDH.TongTienKhuyenMai += chitiet.GiaKhuyenMai;
                    apiService.put('api/chungtudathang/update', $scope.PhieuCTDH, success2, falses2, {ignoreLoadingBar:true})
                    function success2() {
                        
                        $scope.layGioHangTest();
                        $scope.Buttom = false
                        trangthai = "";
                        /*notificationService.displaySuccess("thêm vào giỏ hàng thành công")*/
                        //$scope.TongTienTruocKM = $scope.PhieuCTDH.TongTienHang + $scope.GioHangTest.map(o => o.GiaKhuyenmai).reduce((a, c) => { return a + c });
                    }
                    function falses2() {
                        console.log("sửa CTDH ko thành công")
                    }

                }
                function falses1() {
                    console.log("Thêm mới chi tiết chứng từ không thành công")
                }
                //end kiểm tra không trùng

            }





        }
        /**Sửa và thêm giỏ hàng không tài khoản  */
        function suaGioHangKTK(mahang, hinhanh, tenhang, giaban, giakm, madvt, tendvt, km2, itemKM, VAT, ChietKhau, GiaNhap, SoLuongCT) {
            var vitnuacon = 0;
            if (mahang == '999999999999') {
                mahang = '000000000016'
                vitnuacon = 1;
                angular.forEach($scope.TatCaHangHoa, function (item) {
                    if (item.MaHang == mahang) {
                        tenhang = item.TenHang
                        hinhanh = item.HinhAnh
                    }
                })

            }
            var trangthai;
            var luutam = {};
            angular.forEach($scope.GioHangTest, function (i) {
                if (i.MaHang == mahang && i.KhuyenMai == undefined) {
                    trangthai = 0;
                    luutam = i;
                    tenhang = i.TenHang
                    hinhanh = i.HinhAnh
                }


            })


            if (trangthai == 0) {

                //kiểm tra trùng 
                /*notificationService.displaySuccess("thêm vào giỏ hàng thành công")*/
               
                if (SoLuongCT != null || SoLuongCT != undefined) {
                    luutam.SoLuong += SoLuongCT;
                }
                else if (vitnuacon == 1) {
                    luutam.SoLuong += 0.5;
                }
                else {
                    luutam.SoLuong += 1;
                }
              
                //luutam.ThanhTien = (giaban - giakm) * luutam.SoLuong
                luutam.ThanhTien = (giaban) * luutam.SoLuong
                luutam.GiaKhuyenMai = giakm * luutam.SoLuong
                luutam.TienThueGTGT = ((GiaNhap * VAT) / 100) * luutam.SoLuong
                luutam.TienChietKhau = ((GiaNhap * ChietKhau)) * luutam.SoLuong

                //$scope.TongTienTruocKM = $scope.PhieuCTDH.TongTienHang + $scope.GioHangTest.map(o => o.GiaKhuyenmai).reduce((a, c) => { return a + c });
                if (itemKM != null) {
                    angular.forEach($scope.GioHangTest, function (item) {
                        if (item.MaHang == itemKM.MaHang && item.KhuyenMai == mahang) {
                            if (SoLuongCT != null || SoLuongCT != undefined) { item.SoLuong += itemKM.SoLuongKhuyenMai2 * SoLuongCT } else { item.SoLuong += itemKM.SoLuongKhuyenMai2; }
                          
                        }
                    })
                }

                $scope.PhieuCTDH.TongTienHang = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                $scope.PhieuCTDH.TongTienThanhToan = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c }) - $scope.GioHangTest.map(o => (o.GiaKhuyenMai)).reduce((a, c) => { return a + c });;
                $scope.Buttom = false;
                //end kiểm tra trùng
            }
            else {

               
                var chitiet = {
                    MaChungTuDatHang: "chuaghi",
                    HinhAnh: hinhanh,
                    TenHang: tenhang,
                    TenDonViTinh: tendvt,
                    MaHang: mahang,
                    DonGia: giaban,
                    MaDonViTinh: madvt,
                   
                }
               
                if (itemKM != null) {
                    chitiet.KhuyenMai2 = itemKM.MaHang
                }
                if (SoLuongCT != null || SoLuongCT != undefined) {
                    chitiet.SoLuong = SoLuongCT
                    //chitiet.ThanhTien = (giaban - giakm) * SoLuongCT
                    chitiet.ThanhTien = (giaban) * SoLuongCT
                    chitiet.GiaKhuyenMai = giakm*SoLuongCT
                    chitiet.TienThueGTGT = ((GiaNhap * VAT) / 100) *SoLuongCT
                    chitiet.TienChietKhau = ((GiaNhap * ChietKhau) / 100) * SoLuongCT
                    soluongdefaulse = SoLuongCT
                  
                } else {
                    if (vitnuacon == 1) {
                        chitiet.SoLuong = 0.5
                    } else {
                        chitiet.SoLuong = 1
                    }
                    
                    //chitiet.ThanhTien = (giaban - giakm) * chitiet.SoLuong
                    chitiet.ThanhTien = (giaban) * chitiet.SoLuong
                    chitiet.GiaKhuyenMai = giakm * chitiet.SoLuong
                    chitiet.TienThueGTGT = ((GiaNhap * VAT) / 100)*chitiet.SoLuong,
                    chitiet.TienChietKhau = ((GiaNhap * ChietKhau) / 100) * chitiet.SoLuong
                    soluongdefaulse = chitiet.SoLuong
                  
                }

                $scope.GioHangTest.push(chitiet)
                if (itemKM != null) {
                    var chitiet1 = {
                        MaChungTuDatHang: "chuaghi",
                        HinhAnh: itemKM.HinhAnh,
                        TenHang: itemKM.TenHang,
                        TenDonViTinh: itemKM.TenDonViTinh,
                        MaHang: itemKM.MaHang,
                        KhuyenMai: mahang,
                        //SoLuong: itemKM.SoLuongKhuyenMai2,
                        DonGia: 0,
                        //KhuyenMai: 0,
                        ThanhTien: 0,
                        MaDonViTinh: itemKM.MaDonViTinh,
                        GiaKhuyenMai: 0,
                        KhuyenMai2: "",
                        TienThueGTGT: 0,
                        TienChietKhau: 0
                        
                    }
                    if (SoLuongCT != null || SoLuongCT != undefined) {
                        chitiet1.SoLuong = itemKM.SoLuongKhuyenMai2 * SoLuongCT

                    } else {
                        chitiet1.SoLuong = itemKM.SoLuongKhuyenMai2

                    }


                    $scope.GioHangTest.push(chitiet1)
                }
                $scope.PhieuCTDH.TongTienHang = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                $scope.PhieuCTDH.TongTienThanhToan = $scope.GioHangTest.map(o => (o.ThanhTien)).reduce((a, c) => { return a + c }) - $scope.GioHangTest.map(o => (o.GiaKhuyenMai)).reduce((a, c) => { return a + c });
               

                $scope.Buttom = false;
                trangthai = "";
                /*notificationService.displaySuccess("thêm vào giỏ hàng thành công")*/
                //$scope.TongTienTruocKM = $scope.PhieuCTDH.TongTienHang + $scope.GioHangTest.map(o => o.GiaKhuyenmai).reduce((a, c) => { return a + c });
                //end kiểm tra không trùng

            }





        }
        //end Quản lý giỏ hàng
        
        //capcha
        $scope.responses = null;
        $scope.widgetIds = null;
        $scope.model = {
            key: '6Lc8V_8ZAAAAABVK6vaMg7r-ga_RS4TQV3a8MWQJ'
        };
        $scope.setResponses = function (response) {
            console.info('Response available');


            $scope.responses = response;
            console.info(response);
        };

        $scope.setWidgetIds = function (widgetId) {
            console.info('Created widget ID: %s', widgetId);

            $scope.widgetIds = widgetId;
        };

        $scope.cbExpirations = function () {
            console.info('Captcha expired. Resetting response object');

            vcRecaptchaService.reload($scope.widgetIds);

            $scope.responses = null;
        };


        $scope.singleItem = {
            single: null, options: {
                'A': { value: 'C', text: 'Chuck Testa' },
                'B': { value: 'N', text: 'Nikola Tesla' },
                'C': { value: 'R', text: 'Rosa Testa' }
            }
        };



         
        $scope.token = localStorageService.get("TokenInfo");

        //check token
        if ($scope.token) {
            layGioHangtest()
        }
        /**Lấy hàng hóa*/

       /********************lấy cơ sở***************************************/
        $scope.TatCaCoSo = [];
        $scope.laycoso = function () {
            apiService.get('api/coso/getall', null, function (result) {
                $scope.TatCaCoSo = result.data
            }, function () {
                console.log('Khong lay duoc co so');
            })
        }
       

        $scope.laycoso();
    /******************lấy hàng hóa cho slider********************************/
        $scope.TatCaHangHoa = [];
        $scope.TatCaHangHoaDH = [];
        $scope.latHangHoaDatHang= function (page,fliter) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        pageSize: 100,
                        keyword: $scope.keyword,
                        filter: fliter
                    }
                }
            apiService.get('api/hanghoa/layhanghoadathang', config, function (result) {
                if (fliter == undefined) {
                    $scope.TatCaHangHoa = angular.copy(result.data.Items)
                }
                $scope.TatCaHangHoaDH = angular.copy(result.data.Items);

                var index = angular.copy(result.data.Items).findIndex(x => x.MaHang == '000000000016')
                var vitnuacon = result.data.Items[index]
                if (vitnuacon != undefined || vitnuacon != null) {
                    vitnuacon.MaHang = '999999999999'
                    vitnuacon.HinhAnh = '/Assets/giaovien/img/files/vituqaynuacon.png'
                    vitnuacon.TenHang = 'Vịt quay nửa con';

                    $scope.TatCaHangHoaDH.push(vitnuacon)
                    if (fliter == undefined) {
                        $scope.TatCaHangHoa.push(vitnuacon)
                    }
                  
                }
               
                   
                 $scope.AllNhomHang = [];

                    angular.forEach($scope.TatCaHangHoaDH, function (a) {
                        if (!this[a.MaNhomHH] && !this[a.TenNhom]) {
                            this[a.MaNhomHH] = { MaNhomHH: a.MaNhomHH, TenNhom: a.TenNhom };
                            $scope.AllNhomHang.push(this[a.MaNhomHH]);
                        }

                    }, Object.create(null));

                      
                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
                $("#loading").css("display","none")
                    

                }, function () {
                    console.log('Khong lay duoc hang hoa');
                })
            }
        
        $scope.latHangHoaDatHang();
      




        /********start lấy vị trí trí tỉnh thành việt nam********/

        $scope.gettinhthanh = function () {

            apiService.get('api/danhsachtinh_tp/getall', null, function (result) {
                $scope.alltinhthanh = result.data;


            }, function () {
                console.log('khong lay dc vi tri ');
            })
           


        }
         $scope.getquanhuyen = function () {

          
            apiService.get('api/danhsachquan_huyen/getall', null, function (result) {
                $scope.allquanhuyen = result.data;
              

            }, function () {
                console.log('khong lay dc vi tri ');
            })


        }
        $scope.searchquan = function (ma) {

            var config = {
                params: {
                    matinh: ma
                   
                }
            }
            apiService.get('api/danhsachquan_huyen/ChonTheoTinh', config, function (result) {
                $scope.allquanhuyen = result.data;
              

            }, function () {
                console.log('khong lay dc vi tri ');
            })


        }





        /*********************Đăng ký****/
        $scope.changeTinh = function (item) {
            $scope.gettinhthanh()
           
            $scope.DangKy.Tinh_TP = item.MaTinh_TP
            $scope.DangKy.MaQuan_Huyen = item.MaQuan_Huyen
        }

        $scope.changeQuan = function () {
            $scope.itemquanhuyen = {}
            $scope.searchquan($scope.DangKy.Tinh_TP)
        }
        /*****************************không tài khoản */
        $scope.changeTinhs = function (item) {
            $scope.gettinhthanh()
            $scope.Thongtin.MaTinh_TP = item.MaTinh_TP
            $scope.Thongtin.MaQuan_Huyen = item.MaQuan_Huyen
        }
        $scope.changeQuans = function () {
            $scope.itemquanhuyens = {}
            $scope.searchquan($scope.Thongtin.MaTinh_TP)
        }

        //khi bấm tiến hành mua hàng
        $scope.ThemKhachHang = function () {
            if ($scope.responses == null) {
                alert("Vui lòng xác thực bạn không phải là robot")
            } else {
                $scope.MuaHangKCTK = false;
                // thanhtoan()
                $state.go("xemgiohang")
                $('#LoginModal').modal('hide');
                $('#hinhthucthanhtoan').modal('show');
            }



        }

        // nếu chọn mua hàng không tài khoàn thì show moadl mua hàng ko tài khỏan lên
        $scope.checkmuahang = function (status) {
            if (status == "n") {
                $scope.checkKoTK = true;
                $scope.checkCoTK = false;
                vcRecaptchaService.reload($scope.widgetIds);
                $scope.responses = null;
            }
            else {
                vcRecaptchaService.reload($scope.widgetIds);
                $scope.responses = null;
                $scope.checkKoTK = false;
                $scope.checkCoTK = true;

            }

            $scope.DangKy = {};
            $scope.ErroEmail = "";
            $scope.ErroPass = "";
            $scope.DangNhap = {};


            //cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        }
        function DuyTriDangNhap() {
            if (localStorageService.get("TokenInfo") != null) {
            apiService.get('api/applicationUser/getbyid', null, function (result) {
                $scope.DaDangNhap = true;
            }, function () {
                //console.log('chưa đăng nhập');
                $scope.DaDangNhap = false;
            })
            }
        }
        function layGioHangtest(status) {
            //nếu khách chọn lưu khi chuẩn bị đăng nhập
            if (status == true) {
                apiService.get('api/chitietchungtudathang/getbyemail', {ignoreLoadingBar: true}, function (result) {
                    $scope.DaDangNhap = true;
                    //nếu khách chọn là lưu và giỏ hàng cũ  có hàng hóa thì update
                    if (result.data.length > 0) {
                        angular.forEach($scope.GioHangTest, function (i) {
                            i.MaChungTuDatHang = result.data[0].MaChungTuDatHang
                        })
                        apiService.put('api/chitietchungtudathang/updateList', $scope.GioHangTest, succcess5, falses5, {ignoreLoadingBar: true })
                        function succcess5() {
                            layGioHangtest(false)
                            $state.reload();
                            $('#LoginModal').modal('hide')

                        }
                        function falses5() { }


                    }
                    //nếu khách chọn là lưu và giỏ hàng cũ ko có gì thì tạo mới
                    else {
                        apiService.get('api/chungtudathang/laymamoinhat', null, function (result1) {
                            $scope.MaCTDHMoi = result1.data;
                            var TongGiaGioHang = $scope.GioHangTest.map(o => o.DonGia).reduce((a, c) => { return a + c });
                            var TongKhuyenMaiGioHang = $scope.GioHangTest.map(o => o.KhuyenMai).reduce((a, c) => { return a + c });
                            var PhieuCTDHMoi =
                            {
                                MaChungTuDatHang: $scope.MaCTDHMoi,
                                TongTienHang: TongGiaGioHang - TongKhuyenMaiGioHang,
                                TongTienThanhToan: TongGiaGioHang - TongKhuyenMaiGioHang,
                                DaThayDoi: false,
                                DaThanhToan: false,
                                MaKenhBanHang: '04',
                                MaTinhTrang: 1,
                                IsViewed: false,
                                Email: $scope.authentication.userName
                            }
                            var chitiettrangthaiModel = {
                                MaChungTuDatHang: $scope.MaCTDHMoi,
                                MaTrangThai: 1,
                            }
                            apiService.post('api/chungtudathang/create', PhieuCTDHMoi, success1, falses1)
                            function success1() {
                                apiService.post('api/chitiettrangthai/create', chitiettrangthaiModel, success3, falses3)
                                function success3() {
                                    angular.forEach($scope.GioHangTest, function (i) {
                                        i.MaChungTuDatHang = $scope.MaCTDHMoi
                                    })
                                    apiService.post('api/chitietchungtudathang/createList', $scope.GioHangTest, success2, falses2)
                                    function success2() {
                                        alert("Thành công")
                                        layGioHangtest(false)
                                        $state.reload();
                                        $('#LoginModal').modal('hide')
                                    }
                                    function falses2() { }
                                }
                                function falses3() { console.log("Thêm mới trạng thái chứng từ không thành công") }
                            }
                            function falses1() { console.log("Thêm mới chứng từ không thành công") }

                        }, function () {
                            console.log('Không lấy đươc mã chứng từ')
                        })

                    }

                }, function () {
                    $scope.DaDangNhap = false;
                    console.log('Không lấy được giỏ hàng')
                })

            }
            else {
                apiService.get('api/chitietchungtudathang/getbyemail', null, function (result) {
                    $scope.GioHangTest = result.data;
                    $scope.tongSoMatHang = $scope.GioHangTest.length;
                    $scope.DaDangNhap = true;


                    if (result.data.length > 0) {
                        var config = {
                            params: {
                                mact: $scope.GioHangTest[0].MaChungTuDatHang
                            }
                        }
                        apiService.get('api/chungtudathang/getByMaCT', config, function (result1) {
                            $scope.PhieuCTDH = result1.data
                            $scope.PhieuCTDH.MaHinhThucThanhToan = 5;
                        }, function () {
                            console.log('Không lấy được giỏ hàng')
                        })
                        //axios
                    }

                }, function () {
                    $scope.DaDangNhap = false;
                    console.log('Không lấy được giỏ hàng')
                })
            }

            

        }
        $scope.logOut = function () {
            $scope.Thongtin = {};
            $scope.DaDangNhap = false;
            loginService.logOut();
            localStorageService.set("TokenInfo", null);
            localStorageService.remove("TokenInfo");
            $window.location.reload();
            $state.go('trangchu');

        }
        $scope.authentication = authData.authenticationData;
        $scope.link = $state.current.path


        $scope.kiemtrarole = function (name) {
            var a;
            angular.forEach(authData.authenticationData.nameRoles, function (item) {
                if (item.Name == name) {
                    a = item.Name
                }
            });
            return a
        }
        $scope.refreshToken = function () {
            loginService.refreshToken();
        }

        $scope.dangKy = dangKy;
        function dangKy() {
            $scope.ErroEmail = "";
            $scope.ErroPass = "";
            if ($scope.DangKy.Tinh_TP == "" || $scope.DangKy.MaQuan_Huyen == "" || $scope.DangKy.Tinh_TP == null || $scope.DangKy.MaQuan_Huyen == null) {
                alert("Tỉnh (tp) hoặc quận (huyện) không được trống")
            }
            else if ($scope.DangKy.Password !== $scope.confirm_Password) {
                $scope.ErroPass = "Mật khẩu không trùng khớp"
            }
            else if ($scope.DangKy.Password.length < 6) {
                $scope.ErroPass = "Mật khẩu dài ít nhất 6 ký tự"
            }
            else if ($scope.responses == null) {
                alert("Vui lòng xác thực bạn không phải là robot")
            }
            else {

                apiService.post('/api/account/dangky', $scope.DangKy, function (result) {
                    notificationService.displaySuccess('Đăng ký thành công')
                    vcRecaptchaService.reload($scope.widgetIds);
                    $scope.DangNhap.UserName = $scope.DangKy.Email;
                    $scope.DangNhap.PassWord = $scope.DangKy.Password;
                    dangNhap();
                    $scope.DangNhap = {};
                    $scope.responses = null;
                    $scope.DangKy = {};
                    $scope.ErroEmail = "";
                    $scope.ErroPass = "";
                    $scope.confirm_Password = "";
                    $scope.confirm_Password = "";
                    $('#LoginModal').modal('hide')
                    $state.reload();
                }, addFailed)
            }


        }
        function addFailed(response) {
            // $scope.errors = parseErrors(response);
            if (!response.data.ModelState) {
                notificationService.displayError(response.data.Message);

            }
            $scope.errors = [];
            for (var key in response.data.ModelState) {
                for (var i = 0; i < response.data.ModelState[key].length; i++) {
                    $scope.errors.push(response.data.ModelState[key][i]);

                    if (response.data.ModelState[key][i] == "Email đã tồn tại") {
                        $scope.ErroEmail = response.data.ModelState[key][i];
                    }
                    if (response.data.ModelState[key][i] == "Mật khẩu phải có ít nhất một chữ hoa ('A' - 'Z')" || response.data.ModelState[key][i] == "Mật khẩu phải có ít nhất một chữ số ('0' - '9')") {
                        $scope.ErroPass = response.data.ModelState[key][i];
                    }




                }
            }

        }

        $scope.dangNhap = dangNhap;

        function dangNhap() {
            loginService.login($scope.DangNhap.UserName, $scope.DangNhap.PassWord).then(function (response) {
                if (response !== null && response.data.error !== undefined) {
                    notificationService.displayError("Đăng nhập không đúng.");//đăng nhập sai
                }
                else {
                    //đăng nhập đúng đi vào trang chủ
                    // layGioHang();
                    $scope.DaDangNhap = true;
                    $scope.GioHangTest.length = 0;
                    $scope.Thongtin = {};//xó thông tin bên mua hàng ko tài khoản
                    //var save;
                    //if ($scope.GioHangTest.length > 0) {
                    //    $ngBootbox.confirm('Giỏ hàng của bạn đã có sản phẩm bạn có muốn thêm vào không ?').then(function () {
                    //        save = true
                    //        layGioHangtest(save);
                    //    }, function () {
                    //        save = false
                    //        layGioHangtest(save);
                    //    })
                    //}
                    //else {
                    //    save = false
                    //    layGioHangtest(save);
                    //    $state.reload();
                    //    $('#LoginModal').modal('hide')
                    //}
                    $('#LoginModal').modal('hide')
                    layGioHangtest();
                    $state.reload();



                }
            });
        }

        
        $scope.$watch('GioHangTest', v => {
            $scope.GioHanglengt = 0;
            angular.forEach($scope.GioHangTest, function (i) {
                if (i.ThanhTien > 0 || i.DonGia>0) {
                    $scope.GioHanglengt += 1;
                }

            })

        }, true)

       

       
        document.addEventListener('scroll', function (event) {
            var scroll = $(window).scrollTop();
         
           if (scroll > 85) {
                $(".navbar").addClass("active")
            }
            if (scroll == 0) {
                $("#header-id div .active").removeClass("active")
            }
         
        }, true /*Capture event*/);
       
      
        DuyTriDangNhap();
        $scope.gettinhthanh();
        $scope.getquanhuyen();
     
    }
    app.run(function ($rootScope, $interval, apiService, $q, $location, $locale, notificationService, authData) {
        $locale.NUMBER_FORMATS.DECIMAL_SEP = ',';
        $locale.NUMBER_FORMATS.GROUP_SEP = '.';
        $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            //assign the "from" parameter to something
            $rootScope.previousState = from.name;
        });
    });

   

})(angular.module('apphome'));

 
