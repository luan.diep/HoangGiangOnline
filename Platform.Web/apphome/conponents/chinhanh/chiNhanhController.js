﻿


(function (app) {
    'use strict';

    app.controller('chiNhanhController', chiNhanhController);

    chiNhanhController.$inject = ['$scope', 'apiService', '$state', '$stateParams', '$compile'];
    function chiNhanhController($scope, apiService, $state, $stateParams, $compile) {

        $scope.layTatCaCoSo = layTatCaCoSo;
        $scope.TatCaCoSo = [];

        $scope.Tinh_TP_ChiNhanh = "";
        $scope.Quan_Huyen_ChiNhanh = "";
        $scope.pages = 0;
        $scope.click = $scope.click;

        $("html, body").animate({
            scrollTop: 0
        }, 1000);

        $("#loading").css("display", "block")

        function layTatCaCoSo(volume) {

            if (volume) {
                if (volume == "cong") {
                    $scope.pages += 1
                }
                else if (volume == "tru") {
                    if ($scope.pages > 0) {
                        $scope.pages -= 1
                    }

                }
            }
            var config = {
                params: {
                    page: $scope.pages,
                    pageSize: 4,

                }
            }
            apiService.get('api/coso/pagination', config, function (result) {
                $scope.TatCaCoSo = result.data.Items;

                $scope.TotalPages = result.data.TotalPages;
                $scope.Page = result.data.Page + 1;
                // jqvery
                if ($scope.TotalPages == $scope.Page) {
                    $("#next").addClass('disabled');
                    // remove .disabled when you are done
                } else {
                    $("#next").removeClass('disabled');
                }
                if (result.data.Page == 0) {
                    $("#Previous").addClass('disabled');
                    // remove .disabled when you are done
                } else {
                    $("#Previous").removeClass('disabled');
                }

                angular.forEach($scope.TatCaCoSoNoibat, function (item) {
                    item.lat = item.KinhTuyen;
                    item.long = item.ViTuyen;
                    item.city = item.TenCoSo;
                    item.desc = item.DiaChi;


                })
                $scope.highlighters = [];
                $scope.gMap = null;

                var winInfo = new google.maps.InfoWindow();

                var googleMapOption = {
                    zoom: 11,
                    center: new google.maps.LatLng(10, 105.2),
                    mapTypeId: google.maps.MapTypeId.TERRAIN
                };

                $scope.gMap = new google.maps.Map(document.getElementById('googleMap'), googleMapOption);



                var createHighlighter = function (citi) {

                    var citiesInfo = new google.maps.Marker({
                        map: $scope.gMap,
                        position: new google.maps.LatLng(citi.lat, citi.long),
                        title: citi.city,
                        id: citi.MaCoSo,
                        CapToChuc: citi.CapToChuc,
                        DienThoai: citi.DienThoai,
                        DiaChi: citi.DiaChi,
                        GhiChu: citi.GhiChu,
                    });

                    citiesInfo.content = '<div>' + citi.desc + '</div>';
                    if (!citi.GhiChu) {
                        citi.GhiChu = ""
                    }
                    var contentString =
                        '<div class="poin" >' +
                        '<h2 ng-click="InfoCoSo(\'' + citi.MaCoSo + '\')">' + citi.city + '</h2>' +
                        '<img onclick="clickQR(id)" id="idimg" src="' + citi.CapToChuc + '"style="" align="left">' +
                        '<div class="diachi_map">' + citi.DiaChi + '</div>' +
                       /* '<p>' + citi.GhiChu + '</p>' +*/ '<p style="font-weight:bold">' + 'Quét mã QR để tìm đường đến cửa hàng' + '</p>' +
                        '<p>' + 'Điện thoại: ' + citi.DienThoai + '</p>' +
                        '<p>' + 'Hotel: 1900 1966' + '</p>' +
                        '<div class="button_map">' + '  <button  type="button" style="background:#f5a21f;font-weight:bold;color:#000;" class="btn" ng-click="AddLoc();">Mua hàng</button>' + '</div>' +
                        '<div class="link_map">' + '<a ng-click="linkbotom()" >Hướng dẫn quét mã QR</a>' + '</div>' +
                        "</div>";


                    $scope.click = function (id) {
                        angular.forEach($scope.highlighters, function (item) {
                            if (item.id == id) {
                                var newcitiesInfo = item;
                                var newcontentString =
                                    '<div class="poin" >' +
                                    '<h2 ng-click="InfoCoSo(\'' + item.id + '\')">' + item.title + '</h2>' +
                                    '<img onclick="clickQR(id)" id="idimg" src="' + item.CapToChuc + '"style="padding:10px;width: 20%;height:20%" align="left">' +
                                    '<div>' + item.DiaChi + '</div>' +
                                   /* '<p>' + item.GhiChu + '</p>' + */'</br>' + '<p style="font-weight:bold">' + 'Quét mã QR để tìm đường đến cửa hàng' + '</p>' +
                                    '<p>' + 'Điện thoại: ' + item.DienThoai + '</p>' +
                                    '<p>' + 'Hotel: 1900 1966' + '</p>' +
                                    '<div>' + '  <button ui-sref="dathang" type="button" style="background:#f5a21f;font-weight:bold;color:#000" class="btn" ng-click="AddLoc();">Mua hàng</button>' + '</div>' +
                                    '<div class="link_map">' + '<a ng-click="linkbotom()" >Hướng dẫn quét mã QR</a>' + '</div>' +
                                    "</div>";


                                var note = $('#googleMap').offset().top;
                                $("html, body").animate({
                                    scrollTop: note / 2
                                }, 1000);
                                var elTooltips = $compile(newcontentString)($scope);
                                winInfo.setContent(elTooltips[0]);
                                winInfo.open($scope.gMap, newcitiesInfo);
                            }
                        })

                    }
                    google.maps.event.addListener(citiesInfo, 'click', function () {
                        var elTooltip = $compile(contentString)($scope);
                        winInfo.setContent(elTooltip[0]);
                        winInfo.open($scope.gMap, citiesInfo);

                    });
                    $scope.highlighters.push(citiesInfo);
                };

                for (var i = 0; i < $scope.TatCaCoSoNoibat.length; i++) {
                    createHighlighter($scope.TatCaCoSoNoibat[i]);
                }
                $("#loading").css("display", "none")
               


            }, function () {
                console.log('Khong lay duoc co so');
            })


        }



        $scope.reload = function () {

            new Swiper('.swiper-container', {

                slidesPerView: 3,

                loopFillGroupWithBlank: true,

                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },

                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });

        }



        $scope.AddLoc = function () {
            $state.go("dathang")
        };

        $scope.linkbotom = function () {
            $state.go("huongdanquetqr")
        };
        $scope.InfoCoSo = function (i) {
            $state.go("thongtincuahang", {
                id: i
            });
        };




        $scope.reload();
        layTatCaCoSo();

    }


})(angular.module('apphome.chinhanh'));