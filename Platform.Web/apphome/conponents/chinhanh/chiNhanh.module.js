﻿
(function () {
    angular.module('apphome.chinhanh', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('chinhanh', {
            url: "/chinhanh?:id",
            templateUrl: "/apphome/conponents/chinhanh/chiNhanhView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "chiNhanhController"
        })
            
    }
})();