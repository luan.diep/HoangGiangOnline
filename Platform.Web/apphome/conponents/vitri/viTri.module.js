﻿

(function () {
    angular.module('apphome.vitri', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('vitri', {
            url: "/vitri",
            templateUrl: "/apphome/conponents/vitri/viTriView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "viTriController"
        })
            
    }
})();