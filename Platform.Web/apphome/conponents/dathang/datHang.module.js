﻿

(function () {
    angular.module('apphome.dathang', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('dathang', {
            url: "/dathang",
            templateUrl: "/apphome/conponents/dathang/datHangView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "datHangController"
        })
            .state('dathang.chitiethanghoa', {
                url: "/chitiethanghoa/:id",
                templateUrl: "/apphome/conponents/chitiethanghoa/chiTietHangHoaView.html?v=" + window.appVersion,
                //parent: 'base',
                controller: "chiTietHangHoaController"
            })

    }
})();