﻿

(function (app) {
    'use strict';

    app.controller('datHangController', datHangController);

    datHangController.$inject = ['$scope', 'apiService', 'notificationService', '$timeout', '$state'];
    function datHangController($scope, apiService, notificationService, $timeout, $state) {
        $scope.filters = "000";
        $scope.index = 0;
        $scope.hideIt = false;
        $scope.$watch(function () {
            return $state.$current.name
        }, function (newVal, oldVal) {
            if (newVal == 'dathang') {
                $scope.hideIt = false;

            }
            else {
                $scope.hideIt = true;
            }
            
        })  



        var baner = $('#baner-id').height();
        var header = $('#header-id').height();
     
        $("html, body").animate({
            scrollTop: baner + header
        }, 1200);
        $scope.NhomHang = [];
 
        //if add to cart btn clicked
        $(document).on("click", ".cart_icon", function () {


            let cart = $('.cart-nav');
            // find the img of that card which button is clicked by user
            let imgtodrag = $(this).parent('.left_product').parent('.name_product').parent('.field-imgs').find(".img-product").eq(0);
            if (imgtodrag) {
                // duplicate the img
                var imgclone = imgtodrag.clone().offset({
                    top: imgtodrag.offset().top,
                    left: imgtodrag.offset().left
                }).css({
                    'opacity': '0.8',
                    'position': 'absolute',
                    'height': '250px',
                    'width': '250px',
                    'z-index': '100'
                }).appendTo($('body')).animate({
                    'top': cart.offset().top + 20,
                    'left': cart.offset().left + 30,
                    'width': 30,
                    'height': 30
                }, 1500, 'easeInOutExpo');



                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach()
                });
            }
        });



        //quản lý slide show baner
        var slidesInSlideshow = 2;
        var slidesTimeIntervalInMs = 4000;
        $scope.slideshow = 1;
        var slideTimer =
        $timeout(function interval() {
                $scope.slideshow = ($scope.slideshow % slidesInSlideshow) + 1;
                slideTimer = $timeout(interval, slidesTimeIntervalInMs);
            }, slidesTimeIntervalInMs);
        //end quản lý slide show baner


        ////bộ lọc
        //$scope.layHangHoaDatHang = function () {
        //    $scope.page =0;
        //    $scope.layhanghoa($scope.page)
        //}
        //lấy nhóm hàng hóa
        $scope.CoSo = function () {
            apiService.get('api/coso/getalls', null, (result) => { $scope.fullCoSo = result.data }, () => { })
        }


        $scope.layhanghoa = function (page) {
            page = page || 0;
            if ($scope.index == 0) {
                var config = {
                    params: {
                        page: page,
                        pageSize: 100,
                        filter: $scope.filters
                    },
                }
                $scope.index+=1
            } else {
                var config = {
                    params: {
                        page: page,
                        pageSize: 100,
                        filter: $scope.filters
                    },
                    ignoreLoadingBar: true
                }
            }
           

            apiService.get('api/hanghoa/layhanghoacoso', config, function (result) {
                $scope.AllProduct = angular.copy(result.data.Items);

                var index = angular.copy(result.data.Items).findIndex(x => x.MaHang == '000000000016')
                var vitnuacon = result.data.Items[index]
                if (vitnuacon != undefined || vitnuacon != null) {
                    vitnuacon.MaHang = '999999999999'
                    vitnuacon.HinhAnh = '/Assets/giaovien/img/files/vituqaynuacon.png'
                    vitnuacon.TenHang = 'Vịt quay nửa con';
                    $scope.AllProduct.push(vitnuacon)
                }


                $scope.AllGroupProduct = [];

                angular.forEach($scope.AllProduct, function (a) {
                    if (!this[a.MaNhomHH] && !this[a.TenNhom]) {
                        this[a.MaNhomHH] = { MaNhomHH: a.MaNhomHH, TenNhom: a.TenNhom };
                        $scope.AllGroupProduct.push(this[a.MaNhomHH]);
                    }

                }, Object.create(null));


                $scope.page = result.data.Page;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
               


            }, function () {
                console.log('Khong lay duoc hang hoa');
            })
        }

        setInterval(function () {
            $scope.layhanghoa() 
        }, 100000000000000000000)



        $scope.layhanghoa()
        $scope.CoSo()
       
     
       
    }


})(angular.module('apphome.dathang'));


