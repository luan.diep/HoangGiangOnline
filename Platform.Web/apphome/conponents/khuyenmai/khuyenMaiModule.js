﻿

(function () {
    angular.module('apphome.khuyenmai', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('khuyenmai', {
            url: "/khuyenmai",
            templateUrl: "/apphome/conponents/khuyenmai/khuyenMaiView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "khuyenMaiController"
        })

    }
})();