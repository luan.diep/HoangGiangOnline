﻿(function (app) {
    'use strict';

    app.controller('UserProfileController', UserProfileController);

    UserProfileController.$inject = ['$scope', 'apiService', 'notificationService', '$ngBootbox', '$stateParams', '$location'];
    function UserProfileController($scope, apiService, notificationService, $ngBootbox, $stateParams, $location) {

        $scope.thongtintaikhoan = true;
        $scope.getlichsu = getlichsu;
        $scope.getuser = getuser;
      //  $scope.getTinhThanh = getTinhThanh;
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        $scope.AllHangHoaChiTiet = [];
        $scope.AllLichSu = [];
        $scope.AllTinhTrang = [];
        $scope.Itemchitietdondathang = {};
        $scope.ThongtinUser = {};
        $scope.ChangePassword = {}
        $scope.update = update;
        $scope.Params = $stateParams.id;
        var geocoder;
        var map;
        if ($scope.authentication.fullName.split(" ").length > 2) {
            var Name = $scope.authentication.fullName.split(" ");
            $scope.Ho = Name[0].substring(0, 1);
            $scope.Ten = Name[2].substring(0, 1);
        }
        if ($scope.authentication.fullName.split(" ").length == 2) {
            var Name = $scope.authentication.fullName.split(" ");
            $scope.Ho = Name[0].substring(0, 1);
            $scope.Ten = Name[1].substring(0, 1);
        }
        if ($scope.authentication.fullName.split(" ").length = 1) {
            var Name = $scope.authentication.fullName.split(" ");
            $scope.Ho = Name[0].substring(0, 1);

        }




        /**************start thay đổi mật khẩu************* */
        function update() {
            $ngBootbox.confirm('Bạn chắc muốn đổi mật khẩu không ?').then(function () {
                apiService.post('/api/account/DoiMatKhau', $scope.ChangePassword, addSuccessed, addFailed);
            });

        }
        function addSuccessed() {
            notificationService.displaySuccess('Đổi mật khẩu thành công');
            $scope.confirmNewpass = "";
            $location.url('trangchu');
        }
        function addFailed(response) {
            // $scope.errors = parseErrors(response);
            if (!response.data.ModelState) {
                notificationService.displayError(response.data.Message);

            }
            $scope.errors = [];
            for (var key in response.data.ModelState) {
                for (var i = 0; i < response.data.ModelState[key].length; i++) {
                    $scope.errors.push(response.data.ModelState[key][i]);
                    notificationService.displayError(response.data.ModelState[key][i]);
                    $scope.confirmNewpass = response.data.ModelState[key][i];
                }
            }

        }

        /**************end thay đổi mật khẩu************* */






        $scope.changeMenu = function (item) {

            if (item == "info" || $scope.Params == 1) {
                $scope.thongtintaikhoan = true;
                $scope.viewInfo = true;
                $scope.EditInfo = false;
                $scope.doimatkhau = false;
                $scope.lichsu = false;
                $scope.chitietlichsu = false;
                $scope.Params = 0;


            }
            if (item == "Editinfo") {
                $scope.thongtintaikhoan = true;
                $scope.viewInfo = false;
                $scope.EditInfo = true;
                $scope.doimatkhau = false;
                $scope.lichsu = false;
                $scope.chitietlichsu = false;
                $scope.Params = 0;


            }




            if (item == "change_password") {

                $scope.doimatkhau = true;
                $scope.thongtintaikhoan = false;
                $scope.viewInfo = false;
                $scope.lichsu = false;
                $scope.chitietlichsu = false;
                $scope.Params = 0;
            }
            if (item == "orders_history" || $scope.Params == 2) {

                $scope.doimatkhau = false;
                $scope.thongtintaikhoan = false;
                $scope.viewInfo = false;
                $scope.lichsu = true;
                $scope.chitietlichsu = false;
                $scope.Params = 0;
            }



        }

        $scope.getchitietlichsu = function (item) {
            $scope.chitietlichsu = true;
            $scope.doimatkhau = false;
            $scope.thongtintaikhoan = false;
            $scope.lichsu = false;
            $scope.Itemchitietdondathang = item;
            $scope.DanhSachHangHoaChiTiet($scope.Itemchitietdondathang.MaChungTuDatHang);

            console.log($scope.Itemchitietdondathang)


        }

        $scope.DanhSachHangHoaChiTiet = function (Id) {
            var config = {
                params: {
                    Id: Id
                }
            }
            apiService.get('api/chitietchungtudathang/getchitietchungtudathang', config, function (result) {
                $scope.AllHangHoaChiTiet = result.data;



            }, function () {
                console.log('Khong lay duoc chi tiet');
            })




        }



        /********lấy lịch sử đặt hàng********/
        function getlichsu() {
            apiService.get('api/chungtudathang/DanhSachChiTietTrangThaiDonGiaoHang', null, function (result) {
                $scope.AllLichSu = result.data;
                angular.forEach($scope.AllLichSu, function (item) {
                    item.TenTrangThai = item.TrangThaiGiaoHang[item.TrangThaiGiaoHang.length - 1].TenTrangThai;
                });
                console.log($scope.AllLichSu);
            }, function () {
                console.log('Khong lay duoc lich su');
            })

        }
        /********start lấy thông tin user********/
        function getuser() {

            apiService.get('api/applicationUser/getbyid', null, function (result) {
                $scope.ThongtinUser = result.data;
                if ($scope.TenDiaChi == null) {
                    $scope.TenDiaChi == "";
                }
                else {
                    $scope.TenDiaChi = $scope.ThongtinUser.Address.split("");
                }
            }, function () {
                console.log('Khong lay duoc lich su');
            })
        }

        /********end lấy thông tin user********/

        /********start lấy vị trí trí tỉnh thành việt nam********/
        //function getTinhThanh() {

        //    apiService.get('apphome/shared/Location/local.json', null, function (result) {
        //        $scope.AllTinhThanh = result.data;
        //    }, function () {
        //        console.log('khong lay dc vi tri ');
        //    })
        //}

        ///***************** Lấy quận huyện ****************************/
        //$scope.getquanhuyen = function (item) {
        //    apiService.get('apphome/shared/Location/local.json', null, function (result) {
        //        for (var i = 0; i < result.data.length; i++) {
        //            if (result.data[i].id == item.id) {
        //                $scope.Allquanhuyen = result.data[i].districts;
        //                console.log(result.data[i].districts)
        //                /*****************Lấy phường xã ****************************/
        //                $scope.getphuongxa = function (items) {
        //                    apiService.get('apphome/shared/Location/local.json', null, function (result) {
        //                        for (var j = 0; j < result.data[item.id - 1].districts.length; j++) {
        //                            if (result.data[item.id - 1].districts[j].id == items.id) {
        //                                $scope.Allphuongxa = result.data[item.id - 1].districts[j].wards;
        //                                console.log(result.data[item.id - 1].districts[j].wards)
        //                            }

        //                        }


        //                    }, function () {
        //                        console.log('khong lay dc vi tri');
        //                    })
        //                }
                        /*****************End Lấy phường xã ****************************/

        //            }

        //        }

        //    }, function () {
        //        console.log('khong lay dc vi tri');
        //    })
        //}

        /*****************End Lấy quận huyện ****************************/


        /********end lấy vị trí trí tỉnh thành việt nam********/


        /******************Update ******************************/

        $scope.updateUser = function () {

            //var nameAddress = $scope.ThongtinUser.Address;
            //var Tinh = $scope.Tinh.name;
            //var ThanhPho = $scope.ThanhPho.name;
            //var Phuongxa = $scope.Phuongxa.name;
            //var Address = nameAddress + " " + Phuongxa + " " + ThanhPho + " " +Tinh;

            //$scope.ThongtinUser.Address = Address;
            $ngBootbox.confirm('Bạn chắc muốn sửa thông tin ?').then(function () {
                apiService.put('/api/account/update', $scope.ThongtinUser, addSuccessed, addFailed);
            });
            function addSuccessed() {
                notificationService.displaySuccess('Sửa thông tin  thành công');
                $scope.confirmNewpass = "";
                $location.url('trangchu');
            }
            function addFailed(response) {
                // $scope.errors = parseErrors(response);
                if (!response.data.ModelState) {
                    notificationService.displayError(response.data.Message);

                }
                $scope.errors = [];
                for (var key in response.data.ModelState) {
                    for (var i = 0; i < response.data.ModelState[key].length; i++) {
                        $scope.errors.push(response.data.ModelState[key][i]);
                        notificationService.displayError(response.data.ModelState[key][i]);
                        $scope.confirmNewpass = response.data.ModelState[key][i];
                    }
                }

            }

        }






        /**********************lấy vị trí gần nhất**************************/
        //$scope.layTatCaCoSo = layTatCaCoSo;
        //$scope.Location = [];
        //function layTatCaCoSo(page) {
        //    apiService.get('api/coso/getall', null, function (result) {
        //        $scope.Location = result.data;
        //        angular.forEach($scope.Location, function () {


        //        })
        //        //locationA.distanceTo(locationB);


        //    })




        //}
        /**********************Hủy đon hàng****************************/
        $scope.huydonhang = function (item) {
            $ngBootbox.confirm('Bạn chắc muốn xóa đơn hàng không ?').then(function () {
                var chitiettrangthaiModel = {
                    MaChungTuDatHang: item.MaChungTuDatHang,
                    MaTrangThai: 4,
                }

                apiService.post('api/chitiettrangthai/create', chitiettrangthaiModel, function () {
                    notificationService.displaySuccess('Hủy hàng thành công');
                    $scope.PhieuCTDH.MaTinhTrang = 4;
                    apiService.put('api/chungtudathang/update', $scope.PhieuCTDH, function (result) {
                        notificationService.displaySuccess('Sửa Chứng từ  thành công');

                        $scope.layGioHangTest();
                        $scope.PhieuCTDH = {}
                        $scope.GioHangTest = [];
                        getlichsu();
                        $scope.changeMenu();
                    }, function () {
                        notificationService.displayError('Sửa Chứng từ không thành công');
                    })





                }, function () {
                    notificationService.displaySuccess('Hủy hàng không thành công');
                })
            });

            
            
        } 


        getlichsu();
       // getTinhThanh();
        getuser();
        $scope.changeMenu()
       // $scope.layTatCaCoSo()
    }


})(angular.module('apphome.userprofile'));