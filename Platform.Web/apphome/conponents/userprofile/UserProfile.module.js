﻿

(function () {
    angular.module('apphome.userprofile', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('userprofile', {
            url: "/userprofile/:id",
            templateUrl: "/apphome/conponents/userprofile/UserProfileView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "UserProfileController"
        })
            
    }
})();