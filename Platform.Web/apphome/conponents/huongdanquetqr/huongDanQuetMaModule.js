﻿

(function () {
    angular.module('apphome.huongdanquetqr', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('huongdanquetqr', {
            url: "/huongdanquetqr",
            templateUrl: "/apphome/conponents/huongdanquetqr/huongDanQuetMaView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "huongDanQuetMaController"
        })

    }
})();