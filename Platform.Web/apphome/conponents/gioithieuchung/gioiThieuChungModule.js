﻿

(function () {
    angular.module('apphome.gioithieuchung', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('gioithieuchung', {
            url: "/gioithieuchung",
            templateUrl: "/apphome/conponents/gioithieuchung/gioiThieuChungView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "gioiThieuChungController"
        })

    }
})();