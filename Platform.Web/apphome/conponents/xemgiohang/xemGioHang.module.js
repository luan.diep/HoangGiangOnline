﻿
(function () {
    angular.module('apphome.xemgiohang', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('xemgiohang', {
            url: "/xemgiohang",
            templateUrl: "/apphome/conponents/xemgiohang/xemGioHangView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "xemGioHangController"
        })
            
    }
})();