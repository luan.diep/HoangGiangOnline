﻿

(function (app) {
    'use strict';

    app.controller('xemGioHangController', xemGioHangController);

    xemGioHangController.$inject = ['$scope', 'apiService', 'notificationService', '$ngBootbox', '$filter', '$state', '$window', 'vcRecaptchaService', '$rootScope'];
    function xemGioHangController($scope, apiService, notificationService, $ngBootbox, $filter, $state, $window, vcRecaptchaService, $rootScope) {
       
        $('.fixed-toggle .closes').css('display', 'none')
        $('.show-icon').css('display', 'block')
        $('.fixed-toggle .toggle-icon ').css('display', 'none')
       
        $("html, body").animate({
            scrollTop: 0
        }, 1000);

        $scope.viewpayment = true;

        $scope.xoaHangHoa = xoaHangHoa;
        //$scope.kiemTraSoLuong = kiemTraSoLuong;

        // $scope.thanhToan = thanhToan;
        //$scope.layMaChungTuMoiNhat = layMaChungTuMoiNhat;
        //$scope.MA = "";
        $("nav.navbar.bootsnav > .side").removeClass("on");
        $("body").removeClass("on-side");
        //capcha
        $scope.response = null;
        $scope.widgetId = null;
        $scope.model = {
            key: '6Lc8V_8ZAAAAABVK6vaMg7r-ga_RS4TQV3a8MWQJ'
        };
        $scope.setResponse = function (response) {
            console.info('Response available');


            $scope.response = response;
            console.info(response);
        };

        $scope.setWidgetId = function (widgetId) {
            console.info('Created widget ID: %s', widgetId);

            $scope.widgetId = widgetId;
        };

        $scope.cbExpiration = function () {
            console.info('Captcha expired. Resetting response object');

            vcRecaptchaService.reload($scope.widgetId);

            $scope.response = null;
        };


        $scope.capNhat = function (mahang, giaban, sl, index, giakm, tendvt, item) {
            ///lấy vị trí hàng hóa cũ
            var ItemHangHoaCu = {};
            angular.forEach($scope.TatCaHangHoa, function (a) {
                if (a.MaHang == mahang) {

                    ItemHangHoaCu = a;
                }
            })

            if ($scope.DaDangNhap == false) {
                if (tendvt == "Kg") {
                    if (sl != 0) {
                        if (ItemHangHoaCu.GiaKhuyenMai != undefined || ItemHangHoaCu.GiaKhuyenMai != null || ItemHangHoaCu.GiaKhuyenMai > 0) {
                            var tinhgram = (ItemHangHoaCu.GiaBan) / 10;
                        }
                        else {
                            var tinhgram = ItemHangHoaCu.GiaBan / 10;
                        }


                        ///sửa chi tiết chứng từ đặt hàng
                        $scope.GioHangTest[index].SoLuong = sl;
                        $scope.GioHangTest[index].ThanhTien = ((tinhgram * sl) * 10);
                        $scope.GioHangTest[index].GiaKhuyenMai = ItemHangHoaCu.GiaKhuyenMai * sl;
                        $scope.GioHangTest[index].TienThueGTGT = ((((ItemHangHoaCu.GiaNhap * ItemHangHoaCu.VAT) / 100) / 10) * sl) * 10,
                        $scope.GioHangTest[index].TienChietKhau = ((((ItemHangHoaCu.GiaNhap * ItemHangHoaCu.ChietKhau) / 100) / 10) * sl) * 10

                        ///kiểm tra hàng gốc có khuyến mãi không
                        var HangGocKM = {};
                        angular.forEach($scope.TatCaHangHoa, function (i) {
                            if (i.MaHang == mahang) {
                                HangGocKM = i.ChiTietKhuyenMai;
                            }
                        })
                        if (HangGocKM != null) {
                            angular.forEach($scope.GioHangTest, function (item) {
                                if (item.MaHang == HangGocKM.MaHang && item.KhuyenMai == mahang) {
                                    item.SoLuong = HangGocKM.SoLuongKhuyenMai2 * sl;

                                }
                            })
                        }
                        ///sửa chứng từ đặt hàng
                        $scope.PhieuCTDH.TongTienHang = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TongTienThanhToan = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c }) - $scope.GioHangTest.map(o => o.GiaKhuyenMai).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TienThueGTGT = $scope.GioHangTest.map(o => o.TienThueGTGT).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TienChietKhau = $scope.GioHangTest.map(o => o.TienChietKhau).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TongTienKhuyenMai = $scope.GioHangTest.map(o => o.GiaKhuyenMai).reduce((a, c) => { return a + c });
                    }
                    if (sl == 0) {
                        item.SoLuong = 0.1
                    }




                }
                ///end sửa chi tiết chứng từ đặt hàng
                else {
                    ///sửa chi tiết chứng từ đặt hàng
                    $scope.GioHangTest[index].SoLuong = sl;
                    if (ItemHangHoaCu.GiaKhuyenMai != undefined || ItemHangHoaCu.GiaKhuyenMai != null || ItemHangHoaCu.GiaKhuyenMai >= 0) {
                        $scope.GioHangTest[index].ThanhTien = (ItemHangHoaCu.GiaBan) * sl;
                    } else {
                        $scope.GioHangTest[index].ThanhTien = ItemHangHoaCu.GiaBan * sl;
                    }
                    $scope.GioHangTest[index].TienThueGTGT = ((ItemHangHoaCu.GiaNhap * ItemHangHoaCu.VAT) / 100) * sl,
                    $scope.GioHangTest[index].TienChietKhau = ((ItemHangHoaCu.GiaNhap * ItemHangHoaCu.ChietKhau) / 100) * sl
                    $scope.GioHangTest[index].GiaKhuyenMai = ItemHangHoaCu.GiaKhuyenMai * sl;

                    ///sửa chứng từ đặt hàng
                    $scope.PhieuCTDH.TongTienHang = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                    $scope.PhieuCTDH.TongTienThanhToan = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c }) - $scope.GioHangTest.map(o => o.GiaKhuyenMai).reduce((a, c) => { return a + c });
                    $scope.PhieuCTDH.TienThueGTGT = $scope.GioHangTest.map(o => o.TienThueGTGT).reduce((a, c) => { return a + c });
                    $scope.PhieuCTDH.TienChietKhau = $scope.GioHangTest.map(o => o.TienChietKhau).reduce((a, c) => { return a + c });
                    $scope.PhieuCTDH.TongTienKhuyenMai = $scope.GioHangTest.map(o => o.GiaKhuyenMai).reduce((a, c) => { return a + c });
                    ///kiểm tra hàng gốc có khuyến mãi không
                    var HangGocKM = {};
                    angular.forEach($scope.TatCaHangHoa, function (i) {
                        if (i.MaHang == mahang) {
                            HangGocKM = i.ChiTietKhuyenMai;
                        }
                    })
                    if (HangGocKM != null) {
                        angular.forEach($scope.GioHangTest, function (item) {
                            if (item.MaHang == HangGocKM.MaHang && item.KhuyenMai == mahang) {

                                item.SoLuong = HangGocKM.SoLuongKhuyenMai2 * sl;

                            }
                        })
                    }

                }
            }
            else {
                if (tendvt == "Kg") {
                    if (sl != 0) {
                        var tinhgram = (giaban) / 10;
                        ///sửa chi tiết chứng từ đặt hàng
                        $scope.GioHangTest[index].SoLuong = sl;
                        $scope.GioHangTest[index].ThanhTien = (tinhgram * sl) * 10;
                        $scope.GioHangTest[index].GiaKhuyenMai = ItemHangHoaCu.GiaKhuyenMai * sl;
                        $scope.GioHangTest[index].TienThueGTGT = ((((ItemHangHoaCu.GiaNhap * ItemHangHoaCu.VAT) / 100) / 10) * sl) * 10,
                        $scope.GioHangTest[index].TienChietKhau = ((((ItemHangHoaCu.GiaNhap * ItemHangHoaCu.ChietKhau) / 100) / 10) * sl) * 10

                        ///kiểm tra hàng gốc có khuyến mãi không
                        var HangGocKM = {};
                        angular.forEach($scope.TatCaHangHoa, function (i) {
                            if (i.MaHang == mahang) {
                                HangGocKM = i.ChiTietKhuyenMai;
                            }
                        })
                        if (HangGocKM != null) {
                            angular.forEach($scope.GioHangTest, function (item) {
                                if (item.MaHang == HangGocKM.MaHang && item.KhuyenMai == mahang) {
                                    item.SoLuong = HangGocKM.SoLuongKhuyenMai2 * sl;
                                    apiService.put('api/chitietchungtudathang/update', item, () => { }, () => { })
                                }
                            })
                        }
                        apiService.put('api/chitietchungtudathang/update', $scope.GioHangTest[index], success1, falses1)
                        function success1() {
                            ///sửa chứng từ đặt hàng
                            $scope.PhieuCTDH.TongTienHang = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                            $scope.PhieuCTDH.TongTienThanhToan = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c }) - $scope.GioHangTest.map(o => o.GiaKhuyenMai).reduce((a, c) => { return a + c });
                            $scope.PhieuCTDH.TienThueGTGT = $scope.GioHangTest.map(o => o.TienThueGTGT).reduce((a, c) => { return a + c });
                            $scope.PhieuCTDH.TienChietKhau = $scope.GioHangTest.map(o => o.TienChietKhau).reduce((a, c) => { return a + c });
                            $scope.PhieuCTDH.TongTienKhuyenMai = $scope.GioHangTest.map(o => o.GiaKhuyenMai).reduce((a, c) => { return a + c });
                            apiService.put('api/chungtudathang/update', $scope.PhieuCTDH, success2, falses2)
                            function success2() {
                                // $scope.layGioHangTest();
                            }
                            function falses2() {
                                console("sửa CTDH ko thành công")
                            }

                        }
                        function falses1() {
                            console.log("sua chi tiet chung tu dat hang khong thanh cong")
                        }
                        ///end sửa chi tiết chứng từ đặt hàng
                    }

                    if (sl == 0) {
                        item.SoLuong = 0.1
                    }



                }
                else {
                    ///sửa chi tiết chứng từ đặt hàng
                    $scope.GioHangTest[index].SoLuong = sl;
                    $scope.GioHangTest[index].ThanhTien = (giaban) * sl;
                    $scope.GioHangTest[index].GiaKhuyenMai = ItemHangHoaCu.GiaKhuyenMai * sl;
                    $scope.GioHangTest[index].TienThueGTGT = ((ItemHangHoaCu.GiaNhap * ItemHangHoaCu.VAT) / 100) * sl,
                        $scope.GioHangTest[index].TienChietKhau = ((ItemHangHoaCu.GiaNhap * ItemHangHoaCu.ChietKhau) / 100) * sl
                    ///kiểm tra hàng gốc có khuyến mãi không
                    var HangGocKM = {};
                    angular.forEach($scope.TatCaHangHoa, function (i) {
                        if (i.MaHang == mahang) {
                            HangGocKM = i.ChiTietKhuyenMai;
                        }
                    })
                    if (HangGocKM != null) {
                        angular.forEach($scope.GioHangTest, function (item) {
                            if (item.MaHang == HangGocKM.MaHang && item.KhuyenMai == mahang) {
                                item.SoLuong = HangGocKM.SoLuongKhuyenMai2 * sl
                                apiService.put('api/chitietchungtudathang/update', item, () => { }, () => { })
                            }
                        })
                    }
                    apiService.put('api/chitietchungtudathang/update', $scope.GioHangTest[index], success1, falses1)
                    function success1() {
                        ///sửa chứng từ đặt hàng
                        $scope.PhieuCTDH.TongTienHang = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TongTienThanhToan = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c }) - $scope.GioHangTest.map(o => o.GiaKhuyenMai).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TienThueGTGT = $scope.GioHangTest.map(o => o.TienThueGTGT).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TienChietKhau = $scope.GioHangTest.map(o => o.TienChietKhau).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TongTienKhuyenMai = $scope.GioHangTest.map(o => o.GiaKhuyenMai).reduce((a, c) => { return a + c });
                        apiService.put('api/chungtudathang/update', $scope.PhieuCTDH, success2, falses2)
                        function success2() {
                            //$scope.layGioHangTest();
                        }
                        function falses2() {
                            console("sửa CTDH ko thành công")
                        }
                        ///end sửa chứng từ đặt hàng
                    }
                    function falses1() {
                        console.log("sua chi tiet chung tu dat hang khong thanh cong")
                    }
                    ///end sửa chi tiết chứng từ đặt hàng

                }
            }
        }





        $scope.ThanhToan = function () {


            //if ($scope.response == null) {
            //    //reCaptcha not verified
            //    alert("Vui lòng xác thực bạn không phải là người máy!");
            //    return false;
            //}
            //else { 

            var DungNgay = false;
            if ($scope.PhieuCTDH.ThoiGianMuonNhanHang == null || $scope.PhieuCTDH.ThoiGianMuonNhanHang == "") {
                Swal.fire({
                    title: 'Thời gian giao hàng không được bỏ trống?',
                    icon: 'warning',


                })
                return;
            }
            else {
                var now = moment().add(1, 'hours').format('MM/DD/YYYY HH:mm:ss');
                var ngaychon = moment($scope.PhieuCTDH.ThoiGianMuonNhanHang);


                if (ngaychon.isBefore(now)) {

                    Swal.fire({
                        title: 'Thời gian giao hàng phải lớn hơn thời gian hiện tại là 1 giờ?',
                        icon: 'warning',


                    })

                }
                else {
                    DungNgay = true;
                    if ($scope.Thongtin.TenKhachHang == null || $scope.Thongtin.MaTinh_TP == null || $scope.Thongtin.MaQuan_Huyen == null || $scope.Thongtin.SoDienThoai == null || $scope.Thongtin.TenKhachHang == "" || $scope.Thongtin.MaTinh_TP == "" || $scope.Thongtin.MaQuan_Huyen == "" || $scope.Thongtin.SoDienThoai == "") {
                        if ($scope.DaDangNhap != true) {
                            $scope.MuaHangKCTK = true;
                            $('#LoginModal').modal('show');

                        }
                    }
                    else {
                        $scope.MuaHangKCTK = false;
                    }
                    if ($scope.MuaHangKCTK == false) {
                        if (DungNgay == true) {

                            $('#hinhthucthanhtoan').modal('show');
                            if ($scope.PhieuCTDH.MaHinhThucThanhToan == 2) {
                                $('#hinhthucthanhtoan').modal('hide');
                                if ($scope.DaDangNhap == true) {
                                    $scope.ThanhToanHoaDKCoTK()
                                } else {
                                    $scope.ThanhToanHoaDKKoTK()
                                }

                            }
                            //kiểm tra chọn hình thức thanh toán chưa
                            else if ($scope.PhieuCTDH.MaHinhThucThanhToan ==4) {
                                $('#hinhthucthanhtoan').modal('hide');

                                Swal.fire({
                                    title: 'Bạn chắc muốn thanh toán hay không ?',
                                    showDenyButton: true,
                                    confirmButtonText: `Xác nhận thanh toán`,

                                }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        $scope.ThanhToanHoaDKKoTK();
                                        $scope.PhieuCTDH.MaHinhThucThanhToan = 4; // = 5 trong database là hình thức thanh toán test, lỗi chọn httt r out ra, hiện 2 modal
                                    } else if (result.isDenied) {
                                        $scope.PhieuCTDH.MaHinhThucThanhToan = 4;
                                    }
                                })
                                //$ngBootbox.confirm('Bạn chắc muốn thanh toán hay không ?').then(function () {
                                //    $scope.ThanhToanHoaDKKoTK();
                                //    $scope.PhieuCTDH.MaHinhThucThanhToan = 5; // = 5 trong database là hình thức thanh toán test, lỗi chọn httt r out ra, hiện 2 modal

                                //}, function () {
                                //        $scope.PhieuCTDH.MaHinhThucThanhToan = 5;
                                //})
                            }




                        }
                    }
                    if ($scope.DaDangNhap == true) {
                        if (DungNgay == true) {

                            $('#hinhthucthanhtoan').modal('show');
                            if ($scope.PhieuCTDH.MaHinhThucThanhToan == 2) {
                                $('#hinhthucthanhtoan').modal('hide');
                                //$('#thanhtoantknganhang').modal('show');
                                $scope.PhieuCTDH.MaHinhThucThanhToan = 2;
                                if ($scope.DaDangNhap == true) {
                                    $scope.ThanhToanHoaDKCoTK()
                                } else {
                                    $scope.ThanhToanHoaDKKoTK()
                                }
                            }
                            else if ($scope.PhieuCTDH.MaHinhThucThanhToan ==4) {
                                $('#hinhthucthanhtoan').modal('hide');
                                Swal.fire({
                                    title: 'Bạn chắc muốn thanh toán hay không ?',
                                    showDenyButton: true,
                                    confirmButtonText: `Xác nhận thanh toán`,

                                }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        $scope.ThanhToanHoaDKCoTK()
                                        $scope.PhieuCTDH.MaHinhThucThanhToan = 4;
                                    } else if (result.isDenied) {
                                        $scope.checkremove()
                                    }
                                })
                                //$ngBootbox.confirm('Bạn chắc muốn thanh toán hay không ?').then(function () {
                                //    $scope.ThanhToanHoaDKCoTK()
                                //    $scope.PhieuCTDH.MaHinhThucThanhToan = 5;
                                //}, function () {
                                //        $scope.PhieuCTDH.MaHinhThucThanhToan = 5;
                                //})
                            }



                        }
                    }
                    else if ($scope.DaDangNhap == false && $scope.MuaHangKCTK == true) {
                        $('#LoginModal').modal('show');
                    }

                }



            }

            //}


            // Hỏa điều kiện có tài khoản
            $scope.ThanhToanHoaDKCoTK = function () {
                var chitiettrangthaiModel = {
                    MaChungTuDatHang: $scope.PhieuCTDH.MaChungTuDatHang,
                    MaTrangThai: 2,
                }


                apiService.post('api/chitiettrangthai/create', chitiettrangthaiModel, success1, falses1)
                function success1() {
                    $scope.PhieuCTDH.DaThanhToan = false;
                    $scope.PhieuCTDH.MaTinhTrang = 2;
                    ///sửa lại trạng thái chứng từ đặt hàng là đã thanh toán
                    apiService.put('api/chungtudathang/update', $scope.PhieuCTDH, success2, falses2)
                    function success2(result) {
                        getuser();
                        $('#thanhtoantknganhang').modal('hide');
                        $('#hinhthucthanhtoan').modal('hide');
                        $scope.viewpayment = false;
                        $scope.viewsucces = true;
                        document.body.style.overflow = 'hidden';
                        $scope.Thongtin.TenKhachHang = null
                        $scope.Thongtin.SoDienThoai = null
                        $scope.Thongtin.MaTinh_TP = null
                        $scope.Thongtin.MaQuan_Huyen = null
                        $scope.PhieuCTDH.TongTienHang = null
                        $scope.PhieuCTDH.TongTienThanhToan = null
                        $scope.PhieuCTDH.ThoiGianMuonNhanHang = null
                        $scope.PhieuCTDH.MaHinhThucThanhToan = null
                        $scope.Thongtin = {};
                        $scope.PhieuCTDH = {};
                        $scope.GioHangTest.length = 0;
                        $scope.succsess = result.data
                        $scope.succsess.ThoiGianMuonNhanHang = moment($scope.succsess.ThoiGianMuonNhanHang, "MM/DD/YYYY hh:mm:ss A").format("DD/MM/YYYY HH:mm:ss")
                        //$scope.layGioHangTest();
                        gettimeout();
                        notificationService.displaySuccess("Đặt hàng thành công");


                    }
                    function falses2() {
                        console.log("sửa CTDH ko thành công")
                    }
                }
                function falses1() {
                    console.log("Thêm mới trạng thái chứng từ không thành công")
                }
            }
            // Hỏa điều kiện ko có tài khoản
            $scope.ThanhToanHoaDKKoTK = function () {
                var tentinh = ""
                var tenquan = ""
                angular.forEach($scope.alltinhthanh, function (i) {
                    if (i.MaTinh_TP == $scope.Thongtin.MaTinh_TP) {
                        tentinh = i.TenTinh_TP
                    }
                })
                angular.forEach($scope.allquanhuyen, function (i) {
                    if (i.MaQuan_Huyen == $scope.Thongtin.MaQuan_Huyen) {
                        tenquan = i.TenQuan_Huyen
                    }
                })
                var DiaChi = tenquan + ' ' + tentinh

                var Model = {
                    TenKhachHang: $scope.Thongtin.TenKhachHang,
                    SoDienThoai: $scope.Thongtin.SoDienThoai,
                    DiaChi: DiaChi,
                    chitiet: $scope.GioHangTest,
                    ThoiGianMuonNhanHang: $scope.PhieuCTDH.ThoiGianMuonNhanHang,
                    MaHinhThucThanhToan: $scope.PhieuCTDH.MaHinhThucThanhToan,
                    Tinh_TP: $scope.Thongtin.MaTinh_TP,
                    MaQuan_Huyen: $scope.Thongtin.MaQuan_Huyen,

                }

                apiService.post('/api/khachhangkhongtaikhoan/create', Model, succses, falses)
                function succses(result) {
                    $scope.Thongtincopy = {};
                    angular.copy($scope.Thongtin, $scope.Thongtincopy);

                    $scope.viewpayment = false;
                    $scope.viewsucces = true;
                    document.body.style.overflow = 'hidden';
                    $scope.Thongtin.TenKhachHang = null
                    $scope.Thongtin.SoDienThoai = null
                    $scope.Thongtin.MaTinh_TP = null
                    $scope.Thongtin.MaQuan_Huyen = null
                    $scope.PhieuCTDH.TongTienHang = null
                    $scope.PhieuCTDH.TongTienThanhToan = null
                    $scope.PhieuCTDH.ThoiGianMuonNhanHang = null
                    $scope.PhieuCTDH.MaHinhThucThanhToan = null
                    $scope.Thongtin = {};
                    $scope.PhieuCTDH = {};
                    $scope.GioHangTest.length = 0;
                    $scope.succsess = result.data
                    $scope.succsess.ThoiGianMuonNhanHang = moment($scope.succsess.ThoiGianMuonNhanHang, "MM/DD/YYYY hh:mm:ss A").format("DD/MM/YYYY HH:mm:ss")



                    $('#hinhthucthanhtoan').modal('hide');
                    $('#thanhtoantknganhang').modal('hide');
                    gettimeout();
                    notificationService.displaySuccess("Đặt hàng thành công");


                }
                function falses() {
                    console.log("ko thanh cong")
                }
            }
        }

        function xoaHangHoa(item, i) {
            var HangGocKM = {};
            angular.forEach($scope.TatCaHangHoa, function (item1) {
                if (item1.MaHang == item.MaHang) {
                    HangGocKM = item1.ChiTietKhuyenMai;
                }
            })
            if ($scope.DaDangNhap == false) {
                if (HangGocKM != null) {
                    angular.forEach($scope.GioHangTest, function (value, index) {
                        if (value.MaHang == HangGocKM.MaHang) {

                            $scope.GioHangTest.splice(index, 1);

                        }
                    })
                }

                $scope.GioHangTest.splice(i, 1);
                ///sửa chứng từ đặt hàng
                if ($scope.GioHangTest.length == 0) {
                    $scope.PhieuCTDH.TongTienHang = 0
                    $scope.PhieuCTDH.TongTienThanhToan = 0
                    $scope.TongTienTruocKM = 0
                } else {
                    $scope.PhieuCTDH.TongTienHang = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                    $scope.PhieuCTDH.TongTienThanhToan = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                    $scope.PhieuCTDH.TienThueGTGT = $scope.GioHangTest.map(o => o.TienThueGTGT).reduce((a, c) => { return a + c });
                    $scope.PhieuCTDH.TienChietKhau = $scope.GioHangTest.map(o => o.TienChietKhau).reduce((a, c) => { return a + c });
                }





            }
            else {
                if ($scope.GioHangTest.length <= 1 || $scope.GioHangTest.length == 2 && HangGocKM != null) {



                    if (HangGocKM != null) {
                        angular.forEach($scope.GioHangTest, function (value) {
                            if (value.MaHang == HangGocKM.MaHang) {

                                var config1 = {
                                    params: {
                                        mahang: value.MaHang,
                                        mact: value.MaChungTuDatHang
                                    }
                                }
                                apiService.del('api/chitietchungtudathang/delete', config1, () => { }, () => { })

                            }
                        })
                    }

                    var config = {
                        params: {
                            mahang: item.MaHang,
                            mact: item.MaChungTuDatHang
                        }
                    }

                    apiService.del('api/chitietchungtudathang/delete', config, success1, falses1)
                    function success1() {
                        $scope.GioHangTest = [];
                        //xóa chứng từ đặt hàng
                        var config1 = {
                            params: {
                                id: $scope.PhieuCTDH.MaChungTuDatHang

                            }
                        }
                        apiService.del('api/chungtudathang/delete', config1, success2, falses2)
                        function success2() {
                            $scope.PhieuCTDH = {};
                            $scope.layGioHangTest();

                        }
                        function falses2() {
                            console("xóa CTDH ko thành công")
                        }
                        //end xóa chứng từ đặt hàng
                    }
                    function falses1() {
                        console.log("xóa thất bại")
                    }

                }
                else {
                    var HangGocKM = {};
                    angular.forEach($scope.TatCaHangHoa, function (item1) {
                        if (item1.MaHang == item.MaHang) {
                            HangGocKM = item1.ChiTietKhuyenMai;
                        }
                    })

                    if (HangGocKM != null) {
                        angular.forEach($scope.GioHangTest, function (value, indexs) {
                            if (value.MaHang == HangGocKM.MaHang) {

                                var config = {
                                    params: {
                                        mahang: value.MaHang,
                                        mact: value.MaChungTuDatHang
                                    }
                                }
                                apiService.del('api/chitietchungtudathang/delete', config, () => {
                                    /*$scope.GioHangTest.splice(indexs, 1);*/
                                 }, () => { })

                            }
                        })
                    }


                    var config = {
                        params: {
                            mahang: item.MaHang,
                            mact: item.MaChungTuDatHang
                        }
                    }
                    apiService.del('api/chitietchungtudathang/delete', config, success1, falses1)
                    function success1() {

                        $scope.GioHangTest.splice(i, 1);
                        ///sửa chứng từ đặt hàng
                        $scope.PhieuCTDH.TongTienHang = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TongTienThanhToan = $scope.GioHangTest.map(o => o.ThanhTien).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TienThueGTGT = $scope.GioHangTest.map(o => o.TienThueGTGT).reduce((a, c) => { return a + c });
                        $scope.PhieuCTDH.TienChietKhau = $scope.GioHangTest.map(o => o.TienChietKhau).reduce((a, c) => { return a + c });

                        apiService.put('api/chungtudathang/update', $scope.PhieuCTDH, success2, falses2)
                        function success2() {
                            $scope.layGioHangTest();
                        }
                        function falses2() {
                            console.log("sửa CTDH ko thành công")
                        }
                        //end sửa chứng từ đặt hàng
                    }
                    function falses1() {
                        console.log("xóa thất bại")
                    }
                }
            }


        }
        function getuser() {

            apiService.get('api/applicationUser/getbyid', null, function (result) {
                $scope.ThongtinUser = result.data;
                if ($scope.TenDiaChi == null) {
                    $scope.TenDiaChi == "";
                }
                else {
                    $scope.TenDiaChi = $scope.ThongtinUser.Address.split("");
                }
            }, function () {
                console.log('Khong lay duoc lich su');
            })
        }

        function gettimeout() {
            setTimeout(function () {


            }, 8000);
        }

        $scope.trangchu = function () {

            $scope.PhieuCTDHs = {}
            $scope.GioHangTests = [];

            $state.go('trangchu')


        }


        //$scope.getcapchart = function (evt) {
        //    var response = grecaptcha.getResponse();
        //    if (response.length == 0) {
        //        //reCaptcha not verified
        //        alert("please verify you are humann!");
        //        evt.preventDefault();
        //        return false;
        //    }
        //}
        $scope.troLai = function () {
            if ($rootScope.previousState) {
                $state.go($rootScope.previousState);
            }
            else {
                $state.go('dathang')
            }

        }
        $scope.checkremove = function () {
            $scope.PhieuCTDH.MaHinhThucThanhToan = ""
        }
        $scope.$watch('GioHangTest', v => {
            if (v.length > 0) {
                $scope.TongTienTruocKM = 0;
                v.forEach(item => {
                    if (item.DonGia > 0) {
                        $scope.TongTienTruocKM += item.DonGia * item.SoLuong
                    }
                    item.TongKhuyenMai = item.GiaKhuyenMai;

                })
            } else {
                $scope.TongTienTruocKM = 0

            }

        }, true)

        $scope.ThanhToanChuyenKhoan = function () {
            Swal.fire({
                title: 'Quý khách vui lòng chuyển vào tài khoản sau',
                html:
                    '<h4>Ngân hàng ngoại thương Việt Nam chi nhánh Kiên Giang</h4> ' +
                    ' <h4 style="color:red">Chủ tài khoản : Nguyễn Quang Chí </h4>' +
                    '<h4>Số tài khoản: 009100583567 </h4>' +
                    '<h4 style="font-weight:bold;font-size:18px">Với mẫu:"số điện thoại - thanh toán đặt hàng  vd: 09xxxxxxxx - thanh toán đặt hàng <span style="cursor:pointer;color:red;font-size:14px" id="chuyenkhoan" > xem chi tiết</span></h4>',

                showDenyButton: true,
                confirmButtonText: `Xác nhận thanh toán`,

            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    $scope.PhieuCTDH.MaHinhThucThanhToan = 2; // = 5 trong database là hình thức thanh toán test, lỗi chọn httt r out ra, hiện 2 modal
                    $scope.ThanhToan()
                } else if (result.isDenied) {

                }
            })

        }
        $(document).on('click', "#chuyenkhoan", function () {
            $state.go('thanhtoanchuyenkhoan')
            Swal.close()
        });

    }


})(angular.module('apphome.xemgiohang'));