﻿

(function (app) {
    'use strict';

    app.controller('thongTinCuaHangController', thongTinCuaHangController);

    thongTinCuaHangController.$inject = ['$scope', 'apiService', '$state','$stateParams'];
    function thongTinCuaHangController($scope, apiService, $state, $stateParams) {
        $scope.city = $stateParams.id;
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        

        $scope.getbyid = function () {

            var config = {
                params: {
                    macoso: $stateParams.id
                }
            }
            apiService.get('api/coso/getbyid', config, function (result) {
               
                $('#GhiChu').html(result.data.GhiChu)
            }, function () {
                console.log('khong lay dc thanh pham')
            })
        }
        $scope.getbyid();


    }


})(angular.module('apphome.thongtincuahang'));