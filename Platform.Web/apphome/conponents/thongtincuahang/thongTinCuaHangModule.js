﻿

(function () {
    angular.module('apphome.thongtincuahang', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('thongtincuahang', {
            url: "/thongtincuahang/:id",
            templateUrl: "/apphome/conponents/thongtincuahang/thongTinCuaHangView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "thongTinCuaHangController"
        })

    }
})();