﻿

(function () {
    angular.module('apphome.huongdanthanhtoan', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('huongdanthanhtoan', {
            url: "/huongdanthanhtoan",
            templateUrl: "/apphome/conponents/huongdanthanhtoan/huongDanThanhToanView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "huongDanThanhToanController"
        }).state('thanhtoanchuyenkhoan', {
            url: "/thanhtoanchuyenkhoan",
            templateUrl: "/apphome/conponents/huongdanthanhtoan/thanhToanChuyenKhoanView.html?v=" + window.appVersion,
            parent: 'base'
           
          
        }).state('thanhtoanatm', {
            url: "/thanhtoanatm",
            templateUrl: "/apphome/conponents/huongdanthanhtoan/thanhToanATMView.html?v=" + window.appVersion,
            parent: 'base'
           
          
        }).state('thanhtoanmobile', {
            url: "/thanhtoanmobile",
            templateUrl: "/apphome/conponents/huongdanthanhtoan/thanhToanMobileView.html?v=" + window.appVersion,
            parent: 'base'
           
          
        }).state('thanhtoanqr', {
            url: "/thanhtoanqr",
            templateUrl: "/apphome/conponents/huongdanthanhtoan/thanhToanQRView.html?v=" + window.appVersion,
            parent: 'base'
           
          
        })

    }
})();