﻿

(function () {
    angular.module('apphome.common', ['ui.router',
        'ui.router.state.events',
        'ngBootbox',
        'LocalStorageModule',
        'ngCookies',
        'kendo.directives',
        'angular-loading-bar',
  
        
        'angularjs-datetime-picker'
      ])
    
})();