﻿(function () {
    angular.module('apphome',
        ['apphome.common',
            'apphome.trangchu',
            'apphome.dathang',
            'apphome.menu',
            'apphome.gioithieu',
            'apphome.vitri',
            //'apphome.chitiethanghoa',
            'apphome.chinhanh',
            'apphome.xemgiohang',
            'apphome.userprofile',
            'vcRecaptcha',
            'apphome.tuyendung',
            'apphome.hinhanh',
            'apphome.khuyenmai',
            'apphome.video',
            'apphome.huongdanthanhtoan',
            'apphome.gioithieuchung',
            'apphome.thongtincuahang',
            'apphome.huongdanquetqr',
            'ngAnimate',
            'angular.filter',
           
          

        ])

        .config(config)
        .config(configAuthentication)


    config.$inject = ['$stateProvider', '$urlRouterProvider', '$compileProvider', '$qProvider','cfpLoadingBarProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider, $compileProvider, $qProvider, cfpLoadingBarProvider) {
        $stateProvider.state('base', {
            url: '',
            templateUrl: '/apphome/shared/views/baseView.html?v=' + window.appVersion,
            abstract: true
        })
        $urlRouterProvider.otherwise('/trangchu');
        $qProvider.errorOnUnhandledRejections(false);
        cfpLoadingBarProvider.includeBar = false;
        cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
        cfpLoadingBarProvider.spinnerTemplate = '<div class="bg-load">' +
            ' <div class="loader">' +
            '<div class="loading" style = "opacity:0.8!important;" >' +
            '<lottie-player src="Assets/apphome/js/9825-loading-screen-loader-spinning-circle.json" background="transparent" speed="1" style="width: 100%; height: 100%;" loop autoplay></lottie-player>' +
            '</div >' +
            '</div >';
        //$compileProvider.debugInfoEnabled(false);
    }


    function configAuthentication($httpProvider) {

        //Intercept all http requests
        $httpProvider.interceptors.push(['$injector', '$q', '$location', 'notificationService', 'localStorageService', function ($injector, $q, $location, notificationService, localStorageService) {
            var cachedRequest = null;
            return {
                request: function (config) {

                    return config;
                },
                responseError: function (response) {
                    switch (response.status) {
                        //Detect if reponse error is 401 (Unauthorized)
                        case 401:
                            var token = localStorageService.get("TokenInfo");
                            var giohang = "api/giohang/getbyuser";
                            var url = response.config.url
                            if (!token) {
                                if (url != giohang) {
                                    // notificationService.displayError('Vui lòng đăng nhập để thực hiện thao tác này');
                                   // $("#LoginModal").modal('show');
                                }
                            }
                            //Cache this request
                            var deferred = $q.defer();
                            if (!cachedRequest) {
                                //Cache request for renewing Access Token and wait for Promise
                                var loginService = $injector.get('loginService');

                                cachedRequest = loginService.refreshAccessToken();
                            }
                            //When Promise is resolved, new Access Token is returend 
                            cachedRequest.then(function (accessToken) {
                                cachedRequest = null;
                                if (accessToken) {
                                    //Resend this request when Access Token is renewed
                                    var $http = $injector.get('$http');
                                    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
                                    $http.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
                                    response.config.headers = $http.defaults.headers.common;
                                    $injector.get("$http")(response.config).then(function (resp) {
                                        //Resolve this request (successfully this time)
                                        deferred.resolve(resp);
                                    }, function (resp) {
                                        deferred.reject();
                                    });
                                } else {
                                    //If any error occurs reject the Promise
                                    deferred.reject();
                                }
                            }, function (response) {
                                //If any error occurs reject the Promise
                                cachedRequest = null;
                                deferred.reject();
                                //$location.path('/dangnhap');
                                //notificationService.displayError('Đã hết hạn phiên bản làm việc');
                                return;
                            });
                            return deferred.promise;

                        case 404:
                            $state.go('trangchu');
                            localStorageService.put("TokenInfo", null);
                            localStorageService.remove("TokenInfo");
                            $window.location.reload();
                            return deferred.promise

                    }
                    //If any error occurs reject the Promise
                    return $q.reject(response);
                }
            };
        }]);
    }


})();