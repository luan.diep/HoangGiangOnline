﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface IHangHoaRepository : IRepository<HangHoa>
    {
      
        string GetMaHangHoaCuoiCung();
        IEnumerable<HangHoaOnl> layhanghoaonl();
        IEnumerable<HangHoaOnl> layhanghoacoso(string fliter);
        HangHoaOnl layhanghoaonlById(string id);
        IEnumerable<HangHoa> GetByName(string id);
        IEnumerable<HangHoaSlider> slide_product();
       
    }

    class HangHoaRepository : RepositoryBase<HangHoa>, IHangHoaRepository
    {
        public HangHoaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
       
        public string GetMaHangHoaCuoiCung()
        {
            var MaHangHoaCuoiCung = (from a in DbContext.hangHoas orderby a.MaHang descending select a);
            if (MaHangHoaCuoiCung.Count()>0)
            {
                return MaHangHoaCuoiCung.FirstOrDefault().MaHang;
            }
            return null;
        }

       
        public IEnumerable<HangHoa> GetByName(string id)
        {
            var query = from a in DbContext.hangHoas
                        where a.TenHang.ToLower().Contains(id.ToLower())
                        select a;
            return query;
        }

        public IEnumerable<HangHoaOnl> layhanghoaonl()
        {
            var query = from b in DbContext.HangHoa_CoSos
                        join a in DbContext.hangHoas
                        on b.MaHang equals a.MaHang
                        join c in DbContext.donViTinhs
                        on a.MaDonViTinh equals c.MaDonViTinh
                        join d in DbContext.CoSo
                        on b.MaCoSo equals d.MaCoSo
                        join e in DbContext.nhomHangHoas
                        on a.MaNhomHH equals e.MaNhomHH
                        where b.MaCoSo == "000" && a.GiaBan != 0 && a.GiaBan != null
                        select new HangHoaOnl
                        {
                            MaHang = a.MaHang,
                            TenHang = a.TenHang,
                            MaDonViTinh = a.MaDonViTinh,
                            TenDonViTinh = c.TenDonViTinh,
                            MoTa = a.MoTa,
                            GiaNhap = a.GiaNhap,
                            GiaBan = a.GiaBan,
                            GiaKhuyenMai = b.GiaKhuyenMai,

                            KhuyenMai2 = (from f in DbContext.HangHoa_CoSos
                                          join g in DbContext.hangHoas
                                          on f.KhuyenMai2 equals g.MaHang
                                          where f.KhuyenMai2 == b.KhuyenMai2
                                          select g.TenHang
                                                  ).FirstOrDefault(),
                            ChiTietKhuyenMai= (from f in DbContext.HangHoa_CoSos
                                               join g in DbContext.hangHoas
                                               on f.KhuyenMai2 equals g.MaHang
                                               join j in DbContext.donViTinhs
                                               on g.MaDonViTinh equals j.MaDonViTinh
                                               where f.KhuyenMai2 == b.KhuyenMai2
                                               select new HangHoaKhuyenMai() { 
                                               MaHang=b.KhuyenMai2,
                                               TenHang=g.TenHang,
                                               MaDonViTinh=j.MaDonViTinh,
                                               TenDonViTinh=j.TenDonViTinh,
                                               SoLuongKhuyenMai2=b.SoLuongKhuyenMai2,
                                               HinhAnh=g.HinhAnh
                                               
                                               }).FirstOrDefault(),

                            SoLuongKhuyenMai2 = b.SoLuongKhuyenMai2,
                            GiaNhapCuoi = b.GiaNhapCuoi,
                            HinhAnh = a.HinhAnh,
                            NhieuHinhAnh = a.NhieuHinhAnh,
                            VAT = a.VAT,
                            ChietKhau = a.ChietKhau,
                            ThanhPham = a.ThanhPham,
                            MaCoSo=b.MaCoSo,
                            TenCoSo=d.TenCoSo,
                            MaNhomHH=a.MaNhomHH,
                            TenNhom=e.TenNhom,
                            NgungKinhDoanh=b.NgungKinhDoanh
                            

                        };
            return query;
        }
        public IEnumerable<HangHoaOnl> layhanghoacoso(string fliter)
        {
            var query = from b in DbContext.HangHoa_CoSos
                        join a in DbContext.hangHoas
                        on b.MaHang equals a.MaHang
                        join c in DbContext.donViTinhs
                        on a.MaDonViTinh equals c.MaDonViTinh
                        join d in DbContext.CoSo
                        on b.MaCoSo equals d.MaCoSo
                        join e in DbContext.nhomHangHoas
                        on a.MaNhomHH equals e.MaNhomHH
                        where b.MaCoSo == fliter && a.GiaBan != 0 && a.GiaBan != null
                        select new HangHoaOnl
                        {
                            MaHang = a.MaHang,
                            TenHang = a.TenHang,
                            MaDonViTinh = a.MaDonViTinh,
                            TenDonViTinh = c.TenDonViTinh,
                            MoTa = a.MoTa,
                            GiaNhap = a.GiaNhap,
                            GiaBan = a.GiaBan,
                            GiaKhuyenMai = b.GiaKhuyenMai,
                            SoLuongTonThuc=b.SoLuongTonThuc,
                            KhuyenMai2 = (from f in DbContext.HangHoa_CoSos
                                          join g in DbContext.hangHoas
                                          on f.KhuyenMai2 equals g.MaHang
                                          where f.KhuyenMai2 == b.KhuyenMai2
                                          select g.TenHang
                                                  ).FirstOrDefault(),
                            ChiTietKhuyenMai= (from f in DbContext.HangHoa_CoSos
                                               join g in DbContext.hangHoas
                                               on f.KhuyenMai2 equals g.MaHang
                                               join j in DbContext.donViTinhs
                                               on g.MaDonViTinh equals j.MaDonViTinh
                                               where f.KhuyenMai2 == b.KhuyenMai2
                                               select new HangHoaKhuyenMai() { 
                                               MaHang=b.KhuyenMai2,
                                               TenHang=g.TenHang,
                                               MaDonViTinh=j.MaDonViTinh,
                                               TenDonViTinh=j.TenDonViTinh,
                                               SoLuongKhuyenMai2=b.SoLuongKhuyenMai2,
                                               HinhAnh=g.HinhAnh
                                               
                                               }).FirstOrDefault(),

                            SoLuongKhuyenMai2 = b.SoLuongKhuyenMai2,
                            GiaNhapCuoi = b.GiaNhapCuoi,
                            HinhAnh = a.HinhAnh,
                            NhieuHinhAnh = a.NhieuHinhAnh,
                            VAT = a.VAT,
                            ChietKhau = a.ChietKhau,
                            ThanhPham = a.ThanhPham,
                            MaCoSo=b.MaCoSo,
                            TenCoSo=d.TenCoSo,
                            MaNhomHH=a.MaNhomHH,
                            TenNhom=e.TenNhom,
                            NgungKinhDoanh=b.NgungKinhDoanh
                            

                        };
            return query;
        }
        
        
        public IEnumerable<HangHoaSlider> slide_product()
        {
            var query = from b in DbContext.HangHoa_CoSos
                        join a in DbContext.hangHoas
                        on b.MaHang equals a.MaHang
                        join c in DbContext.donViTinhs
                        on a.MaDonViTinh equals c.MaDonViTinh
                        join d in DbContext.CoSo
                        on b.MaCoSo equals d.MaCoSo
                        join e in DbContext.nhomHangHoas
                        on a.MaNhomHH equals e.MaNhomHH
                        where b.MaCoSo == "000" && a.GiaBan != 0 && a.GiaBan != null
                        select new HangHoaSlider
                        {
                            MaHang = a.MaHang,
                            TenHang = a.TenHang,
                            GiaNhap = a.GiaNhap,
                            GiaBan = a.GiaBan,
                            GiaKhuyenMai = b.GiaKhuyenMai,
                            HinhAnh = a.HinhAnh,
                            TenDonViTinh=c.TenDonViTinh
                        };
            return query;
        }









        public HangHoaOnl layhanghoaonlById(string id)
        {
            var query = from b in DbContext.HangHoa_CoSos
                        join a in DbContext.hangHoas
                        on b.MaHang equals a.MaHang
                        join c in DbContext.donViTinhs
                        on a.MaDonViTinh equals c.MaDonViTinh
                        join e in DbContext.nhomHangHoas
                        on a.MaNhomHH equals e.MaNhomHH
                        where b.MaCoSo == "000" && a.MaHang == id 
                        select new HangHoaOnl
                        {
                            MaHang = a.MaHang,
                            TenHang = a.TenHang,
                            MaDonViTinh = a.MaDonViTinh,
                            TenDonViTinh = c.TenDonViTinh,
                            MoTa = a.MoTa,
                            GiaNhap = a.GiaNhap,
                            GiaBan = a.GiaBan,
                            GiaKhuyenMai = b.GiaKhuyenMai,
                            KhuyenMai2 = (from f in DbContext.HangHoa_CoSos
                                          join g in DbContext.hangHoas
                                          on f.KhuyenMai2 equals g.MaHang
                                          where f.KhuyenMai2 == b.KhuyenMai2
                                          select g.TenHang
                                          ).FirstOrDefault(),
                            ChiTietKhuyenMai = (from f in DbContext.HangHoa_CoSos
                                                join g in DbContext.hangHoas
                                                on f.KhuyenMai2 equals g.MaHang
                                                join j in DbContext.donViTinhs
                                                on g.MaDonViTinh equals j.MaDonViTinh
                                                where f.KhuyenMai2 == b.KhuyenMai2
                                                select new HangHoaKhuyenMai()
                                                {
                                                    MaHang = b.KhuyenMai2,
                                                    TenHang = g.TenHang,
                                                    MaDonViTinh = j.MaDonViTinh,
                                                    TenDonViTinh = j.TenDonViTinh,
                                                    SoLuongKhuyenMai2 = b.SoLuongKhuyenMai2,
                                                    HinhAnh = g.HinhAnh

                                                }).FirstOrDefault(),

                            SoLuongKhuyenMai2 = b.SoLuongKhuyenMai2,
                            NhieuHinhAnh = a.NhieuHinhAnh,
                            GiaNhapCuoi = b.GiaNhapCuoi,
                            HinhAnh = a.HinhAnh,
                            NgungKinhDoanh = b.NgungKinhDoanh,
                            VAT = a.VAT,
                            ChietKhau = a.ChietKhau,
                            ThanhPham = a.ThanhPham,
                            TenNhom=e.TenNhom,
                            MaNhomHH=e.MaNhomHH
                          

                        };
            return query.FirstOrDefault();
        }

       
    }
}
