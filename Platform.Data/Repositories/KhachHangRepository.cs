﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IKhachHangRepository : IRepository<KhachHang>
    {
        string GetLastID();
        string getDiaChiKH(string matinh, string maquan);

    }

    public class KhachHangRepository : RepositoryBase<KhachHang>, IKhachHangRepository
    {
        public KhachHangRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public string getDiaChiKH(string matinh, string maquan)
        {
            var query = from A in DbContext.danhSachTinh_TPs
                        join B in DbContext.danhSachQuan_Huyens
                        on A.MaTinh_TP equals B.MaTinh_TP
                        where B.MaTinh_TP == matinh && B.MaQuan_Huyen == maquan
                        select A.TenTinh_TP +" "+ B.TenQuan_Huyen;

            return query.First();

                        
                        
        }

        // Lấy mã cuối cùng của một khách hàng

        public string GetLastID()
        {
            var query = (from KH in DbContext.khachHangs orderby KH.MaKhachHang descending select KH);
            if (query.Count()>0)
            {
                return query.FirstOrDefault().MaKhachHang;
            }
            return "KH000000";
        }

       
        public IEnumerable<KhachHang> getTheoSinhNhat(DateTime? ngayDau, DateTime? ngayCuoi)
        {
            IEnumerable<KhachHang> danhSachKhachHangTheoSinhNhat = from kh in DbContext.khachHangs
                                                                   where (ngayDau.Value.Month <= kh.NgaySinh.Value.Month && kh.NgaySinh.Value.Month <= ngayCuoi.Value.Month)
                                                                   && (ngayDau.Value.Day <= kh.NgaySinh.Value.Day && kh.NgaySinh.Value.Day <= ngayCuoi.Value.Day)
                                                                   select kh;
            if (danhSachKhachHangTheoSinhNhat != null)
            {
                return danhSachKhachHangTheoSinhNhat;
            }
            return null;
            
        }

      

    }
}
