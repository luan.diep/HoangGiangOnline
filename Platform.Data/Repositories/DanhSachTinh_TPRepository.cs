﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IDanhSachTinh_TPRepository : IRepository<DanhSachTinh_TP>
    {

    }

    class DanhSachTinh_TPRepository : RepositoryBase<DanhSachTinh_TP>, IDanhSachTinh_TPRepository
    {
        public DanhSachTinh_TPRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
