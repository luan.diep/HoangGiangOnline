﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
   
    public interface IPhanHoiRepository : IRepository<PhanHoi>
    {

    }

    class PhanHoiRepository : RepositoryBase<PhanHoi>, IPhanHoiRepository
    {
        public PhanHoiRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }


    }
}
