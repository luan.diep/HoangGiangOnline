﻿using Platform.Data.Infrastructure;
using Platform.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
   
    public interface INhomTuyenDungRepository : IRepository<NhomTuyenDung>
    {
        
    }

    class NhomTuyenDungRepository : RepositoryBase<NhomTuyenDung>, INhomTuyenDungRepository
    {
        public NhomTuyenDungRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            
            
        }
        


    }
}
