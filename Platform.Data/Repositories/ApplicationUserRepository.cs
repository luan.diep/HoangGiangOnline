﻿using System;
using System.Collections.Generic;
using System.Linq;
using Platform.Data.Infrastructure;
using Platform.Model.Models;

namespace Platform.Data.Repositories
{
    public interface IApplicationUserRepository : IRepository<ApplicationUser_KH>
    {
    }

    public class ApplicationUserRepository : RepositoryBase<ApplicationUser_KH>, IApplicationUserRepository
    {
        public ApplicationUserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        
    }
}