﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    
    public interface IKhachHang_KhongTaiKhoanRepository : IRepository<KhachHang_KhongTaiKhoan>
    {
        string GetLastID();


    }

    public class KhachHang_KhongTaiKhoanRepository : RepositoryBase<KhachHang_KhongTaiKhoan>, IKhachHang_KhongTaiKhoanRepository
    {
        public KhachHang_KhongTaiKhoanRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        // Lấy mã cuối cùng của một khách hàng

        public string GetLastID()
        {
            var query = (from KH in DbContext.khachHang_KhongTaiKhoans orderby KH.MaKhachHang descending select KH);
            if (query.Count() > 0)
            {
                return query.FirstOrDefault().MaKhachHang;
            }
            return "KTK000000";
        }






    }
}
