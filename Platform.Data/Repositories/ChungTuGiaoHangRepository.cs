﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface IChungTuGiaoHangRepository : IRepository<ChungTuGiaoHang>
    {
      
        string GetLastID();
       

    }

    class ChungTuGiaoHangRepository : RepositoryBase<ChungTuGiaoHang>, IChungTuGiaoHangRepository
    {
        public ChungTuGiaoHangRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

      

        public string GetLastID()
        {
            var query = (from DTGH in DbContext.chungTuGiaoHangs orderby DTGH.MaChungTuGiaoHang descending select DTGH);
            if (query.Count()>0)
            {
                return query.FirstOrDefault().MaChungTuGiaoHang;
            }
            return "GH000000";
        }
      

       
    }
}
