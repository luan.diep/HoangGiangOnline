﻿using Platform.Data.Infrastructure;
using Platform.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
   
    public interface ITuyenDungRepository : IRepository<TuyenDung>
    {
        IEnumerable<TuyenDungCustom> TuyenDungPagination();
    }

    class TuyenDungRepository : RepositoryBase<TuyenDung>, ITuyenDungRepository
    {
        public TuyenDungRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            
            
        }
         public IEnumerable<TuyenDungCustom> TuyenDungPagination()
        {
            var query = from A in DbContext.tuyenDungs
                        join B in DbContext.nhomTuyenDungs
                        on A.MaNhomTuyenDung equals B.MaNhomTuyenDung
                        join C in DbContext.CoSo
                        on A.MaCoSoTuyenDung equals C.MaCoSo
                        select new TuyenDungCustom()
                        {
                            TenNhomTuyenDung = B.TenNhomTuyenDung,
                            MaTuyenDung=A.MaTuyenDung,
                            TenCoSo = C.TenCoSo,
                            TieuDe = A.TieuDe,
                            Email = A.Email,
                            NgayTao = A.NgayTao,
                            MaCoSoTuyenDung=A.MaCoSoTuyenDung,
                            DiaChi=A.DiaChi,
                            SoDienThoai=A.SoDienThoai,
                            NoiDung=A.NoiDung,
                            MaNhomTuyenDung=B.MaNhomTuyenDung

                           

                        };
            return query;

        }


    }
}
