﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface IChiTietChungTuDatHangRepository : IRepository<ChiTietChungTuDatHang>
    {
        IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathang(string Id);
        IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathangbyemail(string email);

    }

    class ChiTietChungTuDatHangRepository : RepositoryBase<ChiTietChungTuDatHang>, IChiTietChungTuDatHangRepository
    {
        public ChiTietChungTuDatHangRepository(IDbFactory dbFactory) : base(dbFactory)
        {



        }

        public IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathang(string Id)
        {
            var query = from A in DbContext.chiTietChungTuDatHangs
                        join C in DbContext.hangHoas
                        on A.MaHang equals C.MaHang
                        join D in DbContext.donViTinhs
                        on C.MaDonViTinh equals D.MaDonViTinh
                        where A.MaChungTuDatHang.Equals(Id)
                        select new ListChiTietChungTuDatHang
                        {
                            MaChiTietChungTuDatHang = A.MaChiTietChungTuDatHang,
                            MaChungTuDatHang = A.MaChungTuDatHang,
                            MaHang = C.MaHang,
                            HinhAnh = C.HinhAnh,
                            TenHang = C.TenHang,
                            SoLuong = A.SoLuong,
                            DonGia = A.DonGia,
                            KhuyenMai = A.KhuyenMai,
                            ThanhTien = A.ThanhTien,

                            MaDonViTinh = A.MaDonViTinh,
                            TenDonViTinh = D.TenDonViTinh,



                        };

            return query;

        }

        public IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathangbyemail(string email)
        {
            var query = from B in DbContext.chungTuDatHangs
                        join E in DbContext.khachHangs
                        on B.MaKhachHang equals E.MaKhachHang
                        join A in DbContext.chiTietChungTuDatHangs
                        on B.MaChungTuDatHang equals A.MaChungTuDatHang
                        join C in DbContext.hangHoas
                        on A.MaHang equals C.MaHang
                        join D in DbContext.donViTinhs
                        on C.MaDonViTinh equals D.MaDonViTinh
                        where E.Email == email && B.MaTinhTrang == 1
                        select new ListChiTietChungTuDatHang
                        {
                            MaChiTietChungTuDatHang = A.MaChiTietChungTuDatHang,
                            MaChungTuDatHang = A.MaChungTuDatHang,
                            MaHang = C.MaHang,
                            HinhAnh = C.HinhAnh,
                            TenHang = C.TenHang,
                            SoLuong = A.SoLuong,
                            DonGia = A.DonGia,
                            KhuyenMai = A.KhuyenMai,
                            GiaKhuyenMai=A.GiaKhuyenMai,
                            KhuyenMai2=A.KhuyenMai2,
                            ThanhTien = A.ThanhTien,
                            TenDonViTinh = D.TenDonViTinh,
                            KhuyenMaiVatPham = C.KhuyenMai2,
                            TienChietKhau=A.TienChietKhau,
                            TienThueGTGT=A.TienThueGTGT,
                        };
            return query;
        }

    }
}
