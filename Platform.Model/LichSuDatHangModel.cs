﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class LichSuDatHangModel
    {
        public long MaLichSuDatHang { get; set; }
        public string Id { get; set; }
        public string FullName { get; set; }
        public string Address { set; get; }
        public string PhoneNumber { set; get; }
        public string MaChungTuDatHang { get; set; }
        public Nullable<System.DateTime> NgayChungTu { get; set; }
        public Nullable<double> TongTienThanhToan { get; set; }
        public string TenHinhThucThanhToan { get; set; }

        public Nullable<bool> DangXuLy { get; set; }
        public Nullable<System.DateTime> NgayGioDangXuLy { get; set; }
        public Nullable<bool> DaNhanDon { get; set; }
        public Nullable<System.DateTime> NgayGioDaNhanDon { get; set; }
        public Nullable<bool> DangGiaoHang { get; set; }
        public Nullable<System.DateTime> NgayGioDangGiaoHang { get; set; }
        public Nullable<bool> DaNhanHang { get; set; }
        public Nullable<System.DateTime> NgayGioDaNhanHang { get; set; }
        public Nullable<bool> DaHuy { get; set; }
        public Nullable<System.DateTime> NgayGioDaHuy { get; set; }
        public string LyDo { get; set; }


    }
}
