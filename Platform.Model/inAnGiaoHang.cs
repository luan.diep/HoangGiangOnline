﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
  public  class inAnGiaoHang
    {
        public string MaChungTuGiaoHang { get; set; }
        public string MaChungTuBanHang { get; set; }
        public string MaNguoiGiaoHang { get; set; }
        public string MaSoNhanVien { get; set; }
        public Nullable<int> TrongLuong { get; set; }
        public string DiaChiGui { get; set; }
        public string SdtNguoiNhan { get; set; }
        public Nullable<double> TongTienHang { get; set; }
        public Nullable<DateTime> GioGiaoDuKien { get; set; }
        public Nullable<DateTime> NgayChungTu { get; set; }
        public string TenNguoiGiaoHang { get; set; }
        public string DiaChiNguoiGiaoHang { get; set; }
        public string SoDTNguoiGiaoHang { get; set; }
        public string HoVaTen { get; set; }
      
        public string DiaChiNhanVien { get; set; }
        public string SoDienThoaiNV { get; set; }

        public string TenKhachHang { get; set; }
       

    }
}
