//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Platform.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DanhSachQuan_Huyen")]
    public partial class DanhSachQuan_Huyen
    {
        
        [Key]
        public string MaQuan_Huyen { get; set; }
        public string TenQuan_Huyen { get; set; }
        public string MaTinh_TP { get; set; }
        public string MaCoSo { get; set; }
    
        
    }
}
