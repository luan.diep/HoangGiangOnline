﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Model
{
    public class TongPhiGiaoHang
    {
        public string MaNguoiGiaoHang { get; set; }
        public double? PhiGiaoHang { get; set; }
    }
}