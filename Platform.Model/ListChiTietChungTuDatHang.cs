﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public  class ListChiTietChungTuDatHang
    {
        public int MaChiTietChungTuDatHang { get; set; }
        public string MaChungTuDatHang { get; set; }
        public string MaHang { get; set; }
        public string HinhAnh { get; set; }
        public string TenHang { get; set; }
        public string TenDonViTinh { get; set; }
        public string KhuyenMaiVatPham { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<double> DonGia { get; set; }
        public string KhuyenMai { get; set; }
        public Nullable<double> ThanhTien { get; set; }
        public Nullable<int> MaDonViTinh { get; set; }
        public Nullable<double> GiaKhuyenMai { get; set; }
        public string KhuyenMai2 { get; set; }
        public Nullable<double> TienChietKhau { get; set; }
        public Nullable<double> TienThueGTGT { get; set; }
    }
}
