﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    
    [Table("HinhAnh")]
    public partial class HinhAnhs
    {
        [Key]
        public int MaHinhAnh { get; set; }
        public string MaCoSo { get; set; }
        public string HinhAnh { get; set; }
        public string NhieuHinhAnh { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }





    }
}
