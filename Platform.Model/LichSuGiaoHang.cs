﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    public class LichSuGiaoHang
    {        

        // Một người giao hàng có thể giao nhiều đơn giao hàng -> các đơn giao hàng chứa 1 đơn bán hàng, 1 nhân viên và có nhiều trạng thái
        public IEnumerable<ChungTuGiaoHang> chungTuGiaoHangs { get; set; }
          
    }
}
