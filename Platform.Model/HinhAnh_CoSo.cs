﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
  public  class HinhAnh_CoSo
    {

        public int ID { get; set; }
        public string MaCoSo { get; set; }
        public string NhieuHinhAnh { get; set; }
        public string GhiChu { get; set; }
    }
}
