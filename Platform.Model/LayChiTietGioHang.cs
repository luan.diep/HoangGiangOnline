﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    public class LayChiTietGioHang
    {
        public long MaChiTietGioHang { get; set; }
        public long MaGioHang { get; set; }
        public string MaHang { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<double> GiaKhuyenMai { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<int> MaDonViTinh { get; set; }
        public Nullable<double> ThanhTien { get; set; }
        public string TenHang { get; set; }
        public string TenDonViTinh { get; set; }
        public string HinhAnh { get; set; }
    }
}
