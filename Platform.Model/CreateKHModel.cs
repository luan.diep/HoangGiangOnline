﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
  public  class CreateKHModel
    {
        public string MaKhachHang { get; set; }
        public string TenKhachHang { get; set; }
        public Nullable<DateTime> ThoiGianMuonNhanHang { get; set; }
        public string DiaChi { get; set; }
        public string SoDienThoai { get; set; }
        public int MaHinhThucThanhToan { get; set; }
        public string MaQuan_Huyen { get; set; }
        public string Tinh_TP { get; set; }
        public IEnumerable<ChiTietChungTuDatHang> chitiet { get; set; }
    }
}
