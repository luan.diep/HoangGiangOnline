﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    public class DonGiaoHang
    {
        public string MaChungTuGiaoHang { get; set; }
        public string MaChungTuBanHang { get; set; }
        public DateTime?  ThoiGian { get; set; }
        public double? GiaTriHoaDon { get; set; }
        public Nullable<double> CanThu { get; set; }
        public string TrangThai { get; set; }
    }
}
