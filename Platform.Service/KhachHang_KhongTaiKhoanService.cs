﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IKhachHang_KhongTaiKhoanService
    {
        void Add(KhachHang_KhongTaiKhoan khachHang);
        void Update(KhachHang_KhongTaiKhoan khachHang);
        void delete(int id);
        KhachHang_KhongTaiKhoan DELETE(int ID);
        IEnumerable<KhachHang_KhongTaiKhoan> GetAll();
     
        KhachHang_KhongTaiKhoan GetByID(int id);
        KhachHang_KhongTaiKhoan GetByID(string id);
        //IEnumerable<KhachHang_KhongTaiKhoan> getexcel(string id);
        KhachHang_KhongTaiKhoan GetByEmail(string Email);
     
        String GetLastID();
       

        void Commit();
        void Save();
        IEnumerable<KhachHang_KhongTaiKhoan> GetKhachHangTheoMa(string maKhachHang);
        IEnumerable<KhachHang_KhongTaiKhoan> GetPhanLoai(string phanLoai);
        IEnumerable<KhachHang_KhongTaiKhoan> GetTheoNhomKhachHang(string maNhomKhachHang);
      
    }
    public class KhachHang_KhongTaiKhoanService : IKhachHang_KhongTaiKhoanService
    {
        IKhachHang_KhongTaiKhoanRepository _khachHangRepository;
        IUnitOfWork _unitOfWork;
        public KhachHang_KhongTaiKhoanService(IKhachHang_KhongTaiKhoanRepository khachHangRepository, IUnitOfWork unitOfWork)
        {
            this._khachHangRepository = khachHangRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(KhachHang_KhongTaiKhoan khachHang)
        {
            _khachHangRepository.Add(khachHang);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }


        public void delete(int id)
        {
            _khachHangRepository.Delete(id);
        }

        public IEnumerable<KhachHang_KhongTaiKhoan> GetAll()
        {
            return _khachHangRepository.GetAll();
        }

        public KhachHang_KhongTaiKhoan GetByID(int id)
        {
            return _khachHangRepository.GetSingleById(id);
        }



        public void Update(KhachHang_KhongTaiKhoan khachHang)
        {
            _khachHangRepository.Update(khachHang);
        }



        public KhachHang_KhongTaiKhoan DELETE(int ID)
        {
            return _khachHangRepository.Delete(ID);
        }

      

        public KhachHang_KhongTaiKhoan GetByID(string id)
        {
            return _khachHangRepository.GetSingleById(id);
        }

        public string GetLastID()
        {
            return _khachHangRepository.GetLastID();
        }

        public IEnumerable<KhachHang_KhongTaiKhoan> GetKhachHangTheoMa(string maKhachHang)
        {
            return _khachHangRepository.GetMulti(x => x.MaKhachHang == maKhachHang);
        }

        public IEnumerable<KhachHang_KhongTaiKhoan> GetPhanLoai(string phanLoai)
        {
            return _khachHangRepository.GetMulti(x => x.PhanLoai.Contains(phanLoai));
        }

        public IEnumerable<KhachHang_KhongTaiKhoan> GetTheoNhomKhachHang(string maNhomKhachHang)
        {
            return _khachHangRepository.GetMulti(x => x.NhomKH_NCC == maNhomKhachHang);
        }

      
        public KhachHang_KhongTaiKhoan GetByEmail(string Email)
        {
            return _khachHangRepository.GetSingleByCondition(x => x.Email == Email);
        }

        
    }
}
