﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
  



    public interface IHinhAnhsService
    {
        void Add(HinhAnhs HinhAnhs);
        void Update(HinhAnhs HinhAnhs);
        void delete(int id);
        IEnumerable<HinhAnhs> GetAll();
        IEnumerable<AlbumHinhAnh> gethinhanhcoso();
        HinhAnhs GetByID(int id);

        void Commit();
        void Save();

    }
    public class HinhAnhService : IHinhAnhsService
    {
        IHinhAnhRepository _HinhAnhsRepository;
        IUnitOfWork _unitOfWork;
        public HinhAnhService(IHinhAnhRepository HinhAnhsRepository, IUnitOfWork unitOfWork)
        {
            this._HinhAnhsRepository = HinhAnhsRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(HinhAnhs HinhAnhs)
        {
            _HinhAnhsRepository.Add(HinhAnhs);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }


        public void delete(int id)
        {
            _HinhAnhsRepository.Delete(id);
        }

        public IEnumerable<HinhAnhs> GetAll()
        {
            return _HinhAnhsRepository.GetAll();
        }

        public HinhAnhs GetByID(int id)
        {
            return _HinhAnhsRepository.GetSingleById(id);
        }



        public void Update(HinhAnhs HinhAnhs)
        {
            _HinhAnhsRepository.Update(HinhAnhs);
        }

        public IEnumerable<AlbumHinhAnh> gethinhanhcoso()
        {
          return  _HinhAnhsRepository.gethinhanhcoso();
        }
    }
}
