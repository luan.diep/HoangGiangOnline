﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IChiTietChungTuDatHangService
    {
        void Add(ChiTietChungTuDatHang chiTietChungTuDatHang);
        void Update(ChiTietChungTuDatHang chiTietChungTuDatHang);
        void delete(int id);
        void deletes(string id);
        IEnumerable<ChiTietChungTuDatHang> GetAll();
        IEnumerable<ChiTietChungTuDatHang> GetByMaCTDH(string ma);
        ChiTietChungTuDatHang GetByID(int id);
        ChiTietChungTuDatHang GetByChiTietMaHang(string mahang, string mact);
        IEnumerable<ChiTietChungTuDatHang> GetByMaHang(string ma);
        IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathang(string Id);
        IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathangbyemail(string email);
        ChiTietChungTuDatHang getbyId(string mahang, string mact);
        ChiTietChungTuDatHang getbyIdKhongCoMaGoc(string mahang, string mact);
        void Commit();
        void Save();

    }
    public class ChiTietChungTuDatHangService : IChiTietChungTuDatHangService
    {
        IChiTietChungTuDatHangRepository _chiTietChungTuDatHangRepository;
        IUnitOfWork _unitOfWork;
        public ChiTietChungTuDatHangService(IChiTietChungTuDatHangRepository chiTietChungTuDatHangRepository, IUnitOfWork unitOfWork)
        {
            this._chiTietChungTuDatHangRepository = chiTietChungTuDatHangRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(ChiTietChungTuDatHang chiTietChungTuDatHang)
        {
            _chiTietChungTuDatHangRepository.Add(chiTietChungTuDatHang);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }
		 

        public void delete(int id)
        {
            _chiTietChungTuDatHangRepository.Delete(id);
        }

        public IEnumerable<ChiTietChungTuDatHang> GetAll()
        {
            return _chiTietChungTuDatHangRepository.GetAll();
        }

        public ChiTietChungTuDatHang GetByID(int id)
        {
            return _chiTietChungTuDatHangRepository.GetSingleById(id);
        }

       

        public void Update(ChiTietChungTuDatHang chiTietChungTuDatHang)
        {
            _chiTietChungTuDatHangRepository.Update(chiTietChungTuDatHang);
        }

        public IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathang(string Id)
        {
            return _chiTietChungTuDatHangRepository.getchitietchungtudathang(Id);
        }

        public IEnumerable<ChiTietChungTuDatHang> GetByMaCTDH(string ma)
        {
            return _chiTietChungTuDatHangRepository.GetMulti(x=>x.MaChungTuDatHang==ma);
        }
          public IEnumerable<ChiTietChungTuDatHang> GetByMaHang(string ma)
        {
            return _chiTietChungTuDatHangRepository.GetMulti(x => x.MaHang == ma);
        }

        public void deletes(string id)
        {
            _chiTietChungTuDatHangRepository.Delete(id);
        }

        public IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathangbyemail(string email)
        {
            return _chiTietChungTuDatHangRepository.getchitietchungtudathangbyemail(email);
        }

        public ChiTietChungTuDatHang GetByChiTietMaHang(string mahang, string mact)
        {
            return _chiTietChungTuDatHangRepository.GetSingleByCondition(x=>x.MaHang==mahang &&x.MaChungTuDatHang==mact);
        }

        public ChiTietChungTuDatHang getbyId(string mahang, string mact)
        {
            return _chiTietChungTuDatHangRepository.GetSingleByCondition(x => x.MaHang == mahang && x.MaChungTuDatHang == mact&&x.KhuyenMai!=null);
        }

        public ChiTietChungTuDatHang getbyIdKhongCoMaGoc(string mahang, string mact)
        {
            return _chiTietChungTuDatHangRepository.GetSingleByCondition(x => x.MaHang == mahang && x.MaChungTuDatHang == mact && x.KhuyenMai == null);
        }
    }
}
