﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
  



    public interface INhomTuyenDungService
    {
        void Add(NhomTuyenDung nhomTuyenDung);
        void Update(NhomTuyenDung nhomTuyenDung);
        void delete(int id);
        IEnumerable<NhomTuyenDung> GetAll();
        NhomTuyenDung GetByID(int id);
       
        void Commit();
        void Save();

    }
    public class NhomTuyenDungService : INhomTuyenDungService
    {
        INhomTuyenDungRepository _nhomTuyenDungRepository;
        IUnitOfWork _unitOfWork;
        public NhomTuyenDungService(INhomTuyenDungRepository tuyenDungRepository, IUnitOfWork unitOfWork)
        {
            this._nhomTuyenDungRepository = tuyenDungRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(NhomTuyenDung nhomTuyenDung)
        {
            _nhomTuyenDungRepository.Add(nhomTuyenDung);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }


        public void delete(int id)
        {
            _nhomTuyenDungRepository.Delete(id);
        }

        public IEnumerable<NhomTuyenDung> GetAll()
        {
            return _nhomTuyenDungRepository.GetAll();
        }

        public NhomTuyenDung GetByID(int id)
        {
            return _nhomTuyenDungRepository.GetSingleById(id);
        }



        public void Update(NhomTuyenDung nhomTuyenDung)
        {
            _nhomTuyenDungRepository.Update(nhomTuyenDung);
        }

       
    }
}
