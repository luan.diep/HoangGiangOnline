﻿using System;
using System.Collections.Generic;
using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model.Models;

namespace Platform.Service
{
    public interface IApplicationUserService
    {

        IEnumerable<ApplicationUser_KH> GetUsers(string filter);
        void SaveChanges();
    }

    public class ApplicationUserService : IApplicationUserService
    {
        private IApplicationUserRepository _applicationUserRepository;
        private IUnitOfWork _unitOfWork;

        public ApplicationUserService(IApplicationUserRepository applicationUserRepository,
            IUnitOfWork unitOfWork)
        {
            _applicationUserRepository = applicationUserRepository;
            _unitOfWork = unitOfWork;
        }

       

        public void SaveChanges()
        {
            _unitOfWork.Commit();
        }
        public IEnumerable<ApplicationUser_KH> GetUsers(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                return _applicationUserRepository.GetMulti(x => x.FullName.ToLower().Contains(filter) || x.UserName.ToLower().Contains(filter));
            }
            else
            {
                return _applicationUserRepository.GetAll();
            }
        }
    }
}