﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
  



    public interface IVideoService
    {
        void Add(Video video);
        void Update(Video video);
        void delete(int id);
        IEnumerable<Video> GetAll();
        Video GetByID(int id);

        void Commit();
        void Save();

    }
    public class VideoService : IVideoService
    {
        IVideoRepository _tuyenDungRepository;
        IUnitOfWork _unitOfWork;
        public VideoService(IVideoRepository viDeoRepository, IUnitOfWork unitOfWork)
        {
            this._tuyenDungRepository = viDeoRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(Video video)
        {
            _tuyenDungRepository.Add(video);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }


        public void delete(int id)
        {
            _tuyenDungRepository.Delete(id);
        }

        public IEnumerable<Video> GetAll()
        {
            return _tuyenDungRepository.GetAll();
        }

        public Video GetByID(int id)
        {
            return _tuyenDungRepository.GetSingleById(id);
        }



        public void Update(Video video)
        {
            _tuyenDungRepository.Update(video);
        }


    }
}
