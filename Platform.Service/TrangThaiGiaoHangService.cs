﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ITrangThaiGiaoHangService
    {
        void Add(TrangThaiGiaoHang TrangThaiGiaoHang);
        void Update(TrangThaiGiaoHang TrangThaiGiaoHang);
        void delete(int id);
        IEnumerable<TrangThaiGiaoHang> GetAll();
        TrangThaiGiaoHang GetByID(int id);
        TrangThaiGiaoHang GetByID(String id);
        //getTrangThaiGiaoHang getTenTrangThaiGiaoHang(string msnv);
        void Commit();
        void Save();
        IEnumerable<TrangThaiGiaoHang> LayTheoMaDon(string madon);
        IEnumerable<TimeLine> LayTenTrangThai(string maCT);
    }
    public class TrangThaiGiaoHangService : ITrangThaiGiaoHangService
    {
        ITrangThaiGiaoHangRepository _TrangThaiGiaoHangRepository;
        IUnitOfWork _unitOfWork;
        public TrangThaiGiaoHangService(ITrangThaiGiaoHangRepository TrangThaiGiaoHangRepository, IUnitOfWork unitOfWork)
        {
            this._TrangThaiGiaoHangRepository = TrangThaiGiaoHangRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(TrangThaiGiaoHang TrangThaiGiaoHang)
        {
            _TrangThaiGiaoHangRepository.Add(TrangThaiGiaoHang);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _TrangThaiGiaoHangRepository.Delete(id);
        }

        public IEnumerable<TrangThaiGiaoHang> GetAll()
        {
            return _TrangThaiGiaoHangRepository.GetAll();
        }

        public TrangThaiGiaoHang GetByID(int id)
        {
            return _TrangThaiGiaoHangRepository.GetSingleById(id);
        }


        public void Update(TrangThaiGiaoHang TrangThaiGiaoHang)
        {
            _TrangThaiGiaoHangRepository.Update(TrangThaiGiaoHang);
        }

        public TrangThaiGiaoHang GetByID(string id)
        {
            return _TrangThaiGiaoHangRepository.GetSingleById(id);
        }

        public IEnumerable<TrangThaiGiaoHang> LayTheoMaDon(string madon)
        {
            return _TrangThaiGiaoHangRepository.LayTheoMaDon(madon);
        }

        public IEnumerable<TimeLine> LayTenTrangThai(string maCT)
        {
            return _TrangThaiGiaoHangRepository.LayTenTrangThai(maCT);
        }
    }

}
