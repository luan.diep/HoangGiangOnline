﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
  



    public interface IPhanHoiService
    {
        void Add(PhanHoi phanHoi);
        void Update(PhanHoi phanHoi);
        void delete(int id);
        IEnumerable<PhanHoi> GetAll();
        PhanHoi GetByID(int id);

        void Commit();
        void Save();

    }
    public class PhanHoiService : IPhanHoiService
    {
        IPhanHoiRepository _phanHoiRepository;
        IUnitOfWork _unitOfWork;
        public PhanHoiService(IPhanHoiRepository phanHoiRepository, IUnitOfWork unitOfWork)
        {
            this._phanHoiRepository = phanHoiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(PhanHoi phanHoi)
        {
            _phanHoiRepository.Add(phanHoi);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }


        public void delete(int id)
        {
            _phanHoiRepository.Delete(id);
        }

        public IEnumerable<PhanHoi> GetAll()
        {
            return _phanHoiRepository.GetAll();
        }

        public PhanHoi GetByID(int id)
        {
            return _phanHoiRepository.GetSingleById(id);
        }



        public void Update(PhanHoi phanHoi)
        {
            _phanHoiRepository.Update(phanHoi);
        }


    }
}
