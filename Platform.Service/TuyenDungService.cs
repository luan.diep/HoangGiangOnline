﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
  



    public interface ITuyenDungService
    {
        void Add(TuyenDung tuyenDung);
        void Update(TuyenDung tuyenDung);
        void delete(int id);
        IEnumerable<TuyenDung> GetAll();
        TuyenDung GetByID(int id);
        IEnumerable<TuyenDungCustom> TuyenDungPagination();
        void Commit();
        void Save();

    }
    public class TuyenDungService : ITuyenDungService
    {
        ITuyenDungRepository _tuyenDungRepository;
        IUnitOfWork _unitOfWork;
        public TuyenDungService(ITuyenDungRepository tuyenDungRepository, IUnitOfWork unitOfWork)
        {
            this._tuyenDungRepository = tuyenDungRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(TuyenDung tuyenDung)
        {
            _tuyenDungRepository.Add(tuyenDung);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }


        public void delete(int id)
        {
            _tuyenDungRepository.Delete(id);
        }

        public IEnumerable<TuyenDung> GetAll()
        {
            return _tuyenDungRepository.GetAll();
        }

        public TuyenDung GetByID(int id)
        {
            return _tuyenDungRepository.GetSingleById(id);
        }



        public void Update(TuyenDung tuyenDung)
        {
            _tuyenDungRepository.Update(tuyenDung);
        }

        public IEnumerable<TuyenDungCustom> TuyenDungPagination()
        {
            return _tuyenDungRepository.TuyenDungPagination();
        }
    }
}
