﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IHangHoaService
    {
        void Add(HangHoa hangHoa);
        void Update(HangHoa hangHoa);
        void delete(int id);
        IEnumerable<HangHoa> GetAll();
        HangHoa GetByIDs(int id);
        HangHoa GetByID(string id);
        HangHoa gethinhanh(int mahang);
        HangHoa hinhanh(string mahang);
        HangHoaOnl layhanghoaonlById(string id);
        IEnumerable<HangHoa> layhanghoadathang(string keyword = null);

        IEnumerable<HangHoaSlider> slide_product();
        void Commit();
        void Save();
        string GetMaHangHoaCuoiCung(); 
      
        IEnumerable<HangHoa> GetByName(string id);
        IEnumerable<HangHoaOnl> layhanghoaonl();
        IEnumerable<HangHoaOnl> layhanghoacoso(string filter);

    }
    public class HangHoaService : IHangHoaService
    {
        IHangHoaRepository _hangHoaRepository;
        IUnitOfWork _unitOfWork;
        public HangHoaService(IHangHoaRepository hangHoaRepository, IUnitOfWork unitOfWork)
        {
            this._hangHoaRepository = hangHoaRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(HangHoa hangHoa)
        {
            _hangHoaRepository.Add(hangHoa);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }
		 

        public void delete(int id)
        {
            _hangHoaRepository.Delete(id);
        }

        public IEnumerable<HangHoa> GetAll()
        {
            return _hangHoaRepository.GetAll();
        }

        public HangHoa GetByIDs(int id)
        {
            return _hangHoaRepository.GetSingleById(id);
        }

       

        public void Update(HangHoa hangHoa)
        {
            _hangHoaRepository.Update(hangHoa);
        }

        public HangHoa GetByID(string id)
        {
            return _hangHoaRepository.GetSingleByCondition(x => x.MaHang==id);
        }

      

        public string GetMaHangHoaCuoiCung()
        {
            return _hangHoaRepository.GetMaHangHoaCuoiCung();
        }


        public IEnumerable<HangHoa> layhanghoadathang(string keyword = null)
        {
            var hh = _hangHoaRepository.GetMulti(x => x.DaXoa != true && x.NgungKinhDoanh != true && x.TonKho > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                return hh.Where(x => x.MaHang.Contains(keyword) || x.TenHang.Contains(keyword));
            }
            else
            {
                return hh;
            }

        }
      

      

        public IEnumerable<HangHoa> GetByName(string id)
        {
            return _hangHoaRepository.GetByName(id);
        }

        public HangHoa gethinhanh(int mahang)
        {
            return _hangHoaRepository.GetSingleById(mahang);
        }

        public IEnumerable<HangHoaOnl> layhanghoaonl()
        {
            return _hangHoaRepository.layhanghoaonl();
        }

        public HangHoaOnl layhanghoaonlById(string id)
        {
            return _hangHoaRepository.layhanghoaonlById(id);
        }

        public HangHoa hinhanh(string mahang)
        {
            return _hangHoaRepository.GetSingleByCondition(x=>x.MaHang==mahang);
        }

        public IEnumerable<HangHoaSlider> slide_product()
        {
            return _hangHoaRepository.slide_product();
        }

        public IEnumerable<HangHoaOnl> layhanghoacoso(string fliter)
        {
            return _hangHoaRepository.layhanghoacoso(fliter);
        }
    }
}
