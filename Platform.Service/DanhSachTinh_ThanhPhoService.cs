﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IDanhSachTinh_TPService
    {
        void Add(DanhSachTinh_TP DanhSachTinh_TP);
        void Update(DanhSachTinh_TP DanhSachTinh_TP);
        void delete(int id);
        IEnumerable<DanhSachTinh_TP> GetAll();
        DanhSachTinh_TP GetByID(string id);
        void Commit();
        void Save();

    }
    public class DanhSachTinh_TPService : IDanhSachTinh_TPService
    {
        IDanhSachTinh_TPRepository _DanhSachTinh_TPRepository;
        IUnitOfWork _unitOfWork;
        public DanhSachTinh_TPService(IDanhSachTinh_TPRepository DanhSachTinh_TPRepository, IUnitOfWork unitOfWork)
        {
            this._DanhSachTinh_TPRepository = DanhSachTinh_TPRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(DanhSachTinh_TP DanhSachTinh_TP)
        {
            _DanhSachTinh_TPRepository.Add(DanhSachTinh_TP);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _DanhSachTinh_TPRepository.Delete(id);
        }

        public IEnumerable<DanhSachTinh_TP> GetAll()
        {
            return _DanhSachTinh_TPRepository.GetAll();
        }




        public void Update(DanhSachTinh_TP DanhSachTinh_TP)
        {
            _DanhSachTinh_TPRepository.Update(DanhSachTinh_TP);
        }

        public DanhSachTinh_TP GetByID(string id)
        {
            return _DanhSachTinh_TPRepository.GetSingleById(id);
        }

        
    }
}